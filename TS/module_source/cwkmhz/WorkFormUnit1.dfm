object WorkForm: TWorkForm
  Left = 540
  Top = 0
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  ClientHeight = 589
  ClientWidth = 1031
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object MasterP: TPanel
    Left = 0
    Top = 0
    Width = 1031
    Height = 589
    Align = alClient
    FullRepaint = False
    TabOrder = 0
    object MGrid: TcxGrid
      AlignWithMargins = True
      Left = 4
      Top = 105
      Width = 1023
      Height = 480
      Align = alClient
      TabOrder = 0
      LookAndFeel.NativeStyle = False
      object MTV: TcxGridBandedTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skSum
            Column = MTVColumn5
          end
          item
            Kind = skSum
            Column = MTVColumn4
          end
          item
            OnGetText = MTVTcxGridDataControllerTcxDataSummaryFooterSummaryItems2GetText
            Column = MTVColumn1
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        Styles.Background = cxStyle1
        Styles.ContentOdd = cxStyle1
        Styles.BandHeader = cxStyle2
        Bands = <
          item
            Caption = #31185#30446#27719#24635#34920
          end>
        object MTVColumn1: TcxGridBandedColumn
          Caption = #36134#22871
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object MTVColumn2: TcxGridBandedColumn
          Caption = #31185#30446#32534#30721
          Width = 64
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object MTVColumn3: TcxGridBandedColumn
          Caption = #31185#30446#21517#31216
          Options.Editing = False
          Options.Filtering = False
          Options.ShowEditButtons = isebNever
          Width = 120
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object MTVColumn4: TcxGridBandedColumn
          Caption = #20511#26041#26412#24065#37329#39069'('#20803')'
          DataBinding.ValueType = 'Float'
          Options.Editing = False
          Options.Filtering = False
          Options.ShowEditButtons = isebNever
          Width = 130
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object MTVColumn5: TcxGridBandedColumn
          Caption = #36151#26041#26412#24065#37329#39069'('#20803')'
          DataBinding.ValueType = 'Float'
          Options.Editing = False
          Options.Filtering = False
          Options.FilteringWithFindPanel = False
          Options.ShowEditButtons = isebNever
          Options.Sorting = False
          Width = 130
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
      end
      object MGridLevel1: TcxGridLevel
        GridView = MTV
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1029
      Height = 101
      Align = alTop
      BevelKind = bkTile
      BevelOuter = bvNone
      TabOrder = 1
      object Panel2: TPanel
        Left = 0
        Top = 61
        Width = 1025
        Height = 36
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 600
          Top = 11
          Width = 234
          Height = 12
          Caption = #21333#21035#32534#21495#65306'                   '#21333#21035#21517#31216#65306
        end
        object Label2: TLabel
          Left = 40
          Top = 11
          Width = 360
          Height = 12
          Caption = #21333#25454#32534#21495#65288#21482#36755#39'-'#39#21518#20449#24687#65289#20174#65306'                           '#21040#65306
        end
        object dbbhcb: TComboBox
          Left = 670
          Top = 7
          Width = 100
          Height = 20
          TabOrder = 0
          Text = #35831#36873#25321#21333#21035
          OnChange = dbbhcbChange
        end
        object dbmccb: TComboBox
          Left = 840
          Top = 7
          Width = 149
          Height = 21
          Style = csSimple
          Enabled = False
          TabOrder = 1
          Items.Strings = (
            '02003-'#26611#23663#36741#26009#19968#24211
            '02002-'#26611#23663#36741#26009#20108#24211
            '03004-'#35199#21378#36741#26009#19968#24211
            '04005-'#33829#21475#36741#26009#19968#24211)
        end
        object bDh: TEdit
          Left = 224
          Top = 7
          Width = 121
          Height = 20
          TabOrder = 2
        end
        object edh: TEdit
          Left = 424
          Top = 7
          Width = 121
          Height = 20
          TabOrder = 3
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 1025
        Height = 57
        Align = alTop
        TabOrder = 1
        object RunQuery: TButton
          Left = 656
          Top = 7
          Width = 133
          Height = 44
          Caption = #26597#35810
          Enabled = False
          TabOrder = 0
          OnClick = RunQueryClick
        end
        object ToXLS: TButton
          Left = 816
          Top = 7
          Width = 133
          Height = 44
          Caption = #23548#20986'excel'
          TabOrder = 1
          OnClick = ToxlsClick
        end
        object ztRG: TRadioGroup
          Left = 1
          Top = 1
          Width = 593
          Height = 55
          Align = alLeft
          Caption = #21487#36873#36134#22871
          TabOrder = 2
          OnClick = ztRGClick
        end
        object Button1: TButton
          Left = 968
          Top = 24
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 3
          Visible = False
          OnClick = Button1Click
        end
      end
    end
    object Memo1: TMemo
      Left = 336
      Top = 198
      Width = 456
      Height = 260
      Lines.Strings = (
        'Memo1')
      TabOrder = 2
      Visible = False
    end
  end
  object Cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 80
    Top = 424
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 176
    Top = 398
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 744
    Top = 344
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clMenuBar
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
    end
    object stHigh: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object stLow: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
  end
end

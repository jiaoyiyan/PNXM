﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxStyles, cxClasses,
  tmsAdvGridExcel;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    qxm: TImage;
    Label1: TLabel;
    xmLE: TLabeledEdit;
    beginT: TDateTimePicker;
    endT: TDateTimePicker;
    RunQuery: TButton;
    Toxls: TButton;
    ztxz: TRadioGroup;
    ToXlsMX: TButton;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    PrivilegeCDS: TClientDataSet;
    Memo1: TMemo;
    MGrid: TAdvStringGrid;
    agexp: TAdvGridExcelIO;
    OverNum: TEdit;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure endTExit(Sender: TObject);
    procedure endTClick(Sender: TObject);
    procedure MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string): Boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet; tp: Integer = 1);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure WaitMessage;
    procedure FillGridHeader;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'CBJKCTX_DLL';
  DChar = ','; // 分隔符
  SPCHAR = '-';
  DefaultZT = 'GWGS';

var
  zhanghu: string;
  CurrentZT: string;
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  // 获取插件formCaption
  self.Caption := InputParcel.GetAnsiStringGoods('FormCaption');
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 界面初始化
  // 默认为当天
  // endT.Date := now;
  beginT.Date := now;

  FillGridHeader;
end;

// -----------------------------------------------------------------------------
// 业务代码
Const

  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';

  SSQL = 'SELECT gea02,gen02,occ01,occ02,imm14,gem02,D,T ' + slineBreak +
    'FROM                                         ' + slineBreak +
    '(                                            ' + slineBreak +
    'SELECT imm14,MAX(imm17) T,MAX(imm01) D FROM imm_file ' +
    'WHERE regexp_like(imm01,''^CL01'') ' +
    'AND immconf = ''Y'' AND imm03 = ''Y'' AND %s' + slineBreak +
    'GROUP BY imm14 ' + slineBreak + ' HAVING MAX(imm17) < %s ' + slineBreak +
    ' ) a                          ' + slineBreak +
    'LEFT JOIN gem_file a ON a.gem01 = imm14    ' + slineBreak +
    'LEFT JOIN occ_file b ON b.occ01 = a.ta_gem22 ' + slineBreak +
    'LEFT JOIN gea_file c ON c.gea01 = b.occ20    ' + slineBreak +
    'LEFT JOIN gen_file d ON d.gen01 = b.occ04 ';

  headStr = '序号,分区,业务经理,客户编号,客户简称,项目编号,项目名称,领料单号,扣账日期,未出库天数';
  WeekNum = 4;

var
  xmNF, zqTimeF: string; // 项目,时间的过滤条件
  qyear, qmonth, byear, bmonth: Integer;

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

function TWorkForm.GetFilterS: string;
var
  bt, et: Tdate;
begin
  Result := '';

  if xmLE.Text = '' then
  begin
    xmNF := ' 1=1 ';
  end
  else
  begin
    xmNF := StringReplace(trim(xmLE.Text), DChar, '''' + DChar + '''',
      [rfReplaceAll]);
    xmNF := ' imm14 in (''' + xmNF + ''') ';
  end;

  bt := beginT.Date;

  zqTimeF := ' to_date(''' + FormatDateTime('yyyymmdd', bt) +
    ''',''yyyymmdd'') - ' + trim(OverNum.Text);
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
var
  LJBH: string;
  SQL: string;
  E: string;
begin
  if Not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;
  // 主查询
  MGrid.RowCount := 2;
  screen.Cursor := crSQLWait;
  E := GetFilterS;
  try
    if E <> '' then
    begin
      screen.Cursor := crDefault;
      application.MessageBox(Pchar(E), '提示');
      exit;
    end;
    SQL := Format(SSQL, [xmNF, zqTimeF]);
    // memo1.Text := sql;
    // exit;
    RunCDS(SQL);
    if Cds.RecordCount > 0 then
      FillmGrid(Cds)
    ELSE
    BEGIN
      SHOWMESSAGE('没有查询到符合条件的记录！');
      MGrid.RowCount := 2;
    END;
  finally
    ResizeForm;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(nil);
  with SaveDialog do
  begin
    filter := '*.xls|*.xls';
    if Execute then
    begin
      agexp.XLSExport(SaveDialog.FileName + '.xls');
      SHOWMESSAGE('导入成功！');
    end;
  end;
  SaveDialog.Free;
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet; tp: Integer = 1);
var
  i, j, w: Integer;
begin
  try
    MGrid.BeginUpdate;
    i := 1;
    w := MGrid.ColCount;
    MGrid.RowCount := Q.RecordCount + 1;
    MGrid.FixedRows := 1;
    Q.First;
    while not Q.Eof do
    begin
      MGrid.Cells[0, i] := inttostr(i);
      for j := 1 to w - 2 do
        MGrid.Cells[j, i] := Q.Fields.Fields[j - 1].AsString;
      MGrid.Cells[j,i] := trunc(beginT.DateTime - Q.FieldByName('T').AsDateTime).ToString;
      inc(i);
      Q.Next;
    end;
    MGrid.AutoSizeColumns(false, 2);
  finally
    MGrid.EndUpdate;
  end;
end;

procedure TWorkForm.endTClick(Sender: TObject);
begin
  beginT.Date := endT.Date - 28;
end;

procedure TWorkForm.endTExit(Sender: TObject);
begin
  beginT.Date := endT.Date - 28;
end;

procedure TWorkForm.FillGridHeader;
begin
  MGrid.ColumnHeaders.DelimitedText := headStr;
end;

procedure TWorkForm.MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
  var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if ARow = 0 then
  begin
    HAlign := taCenter;
    VAlign := vtaCenter;
  end;
end;

procedure TWorkForm.MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if ARow > 1 then
  begin
    if (ARow mod 2) = 0 then
      ABrush.Color := MGrid.Color
    else
      ABrush.Color := clBtnFace;
    if (MGrid.Cells[0, ARow] <> '') and (MGrid.Cells[ACol, ARow] = '') then
      ABrush.Color := clYellow;
  end;
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息  同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.

﻿//
// WorkFormUnit.pas -- 工作窗体单元（应用程序员自行修改窗体内容）
// Version 1.00 Ansi Edition
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit;

interface

uses
  winapi.Windows, SysUtils, Classes, Forms, qbclientmodule, ImgList,
  Controls, OleCtrls, SHDocVw, ComCtrls, ExtCtrls,
  StdCtrls, Graphics, Buttons,
  QBParcel, UserConnection, QBJson, jpeg, FileTransfer,
  QBWinMessages, DllSpread, System.ImageList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, dxBarBuiltInMenu, cxPC, cxCustomData,
  cxStyles, cxTL, cxTextEdit, cxTLdxBarBuiltInMenu, cxInplaceContainer, Data.DB,
  Datasnap.DBClient, DbAccessor,iniFiles,qbmisc;

type
  TWorkForm = class(TForm)
    TopPanel: TPanel;
    Bevel1: TBevel;
    SpeedButton1: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton5: TSpeedButton;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    MainPanel: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    BusinessPanel: TPanel;
    BusinessControl: TPageControl;
    MessagePanel: TPanel;
    ContactControl: TPageControl;
    DepartmentPanel: TPanel;
    yewuTab: TTabSheet;
    ImageList1: TImageList;
    Module: TQBClientModule;
    ImageList2: TImageList;
    Memo1: TMemo;
    stInfo: TStatusBar;
    winMessage: TQBWinMessages;
    WorkControl: TcxPageControl;
    Main: TcxTabSheet;
    WebBrowser1: TWebBrowser;
    MainTab: TTabSheet;
    ctl: TcxTreeList;
    dName: TcxTreeListColumn;
    modid: TcxTreeListColumn;
    pid: TcxTreeListColumn;
    lev: TcxTreeListColumn;
    did: TcxTreeListColumn;
    dllname: TcxTreeListColumn;
    checkright: TcxTreeListColumn;
    bright: TcxTreeListColumn;
    ismenu: TcxTreeListColumn;
    isDisplay: TcxTreeListColumn;
    R1: TcxTreeListColumn;
    R2: TcxTreeListColumn;
    R3: TcxTreeListColumn;
    R4: TcxTreeListColumn;
    R5: TcxTreeListColumn;
    R6: TcxTreeListColumn;
    R7: TcxTreeListColumn;
    R8: TcxTreeListColumn;
    R9: TcxTreeListColumn;
    R10: TcxTreeListColumn;
    dba: TDBAccessor;
    cds: TClientDataSet;
    rv: TcxTreeListColumn;
    dbtag: TcxTreeListColumn;
    procedure LogMsg(aMsg: String);
    procedure Shutdown;
    procedure Restart;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedButton5Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure winMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure LoadDLL(Sender: TObject);
    procedure WorkControlCanCloseEx(Sender: TObject; ATabIndex: Integer;
      var ACanClose: Boolean);
    procedure ctlClick(Sender: TObject);
    procedure ctlFocusedNodeChanged(Sender: TcxCustomTreeList; APrevFocusedNode,
      AFocusedNode: TcxTreeListNode);
  private
    FDisplayInfo: string;
    function AddForm(FileName: string): Integer;
    function FindFormID(FileName: string): Integer;
    procedure CloseForm(ID: Integer); overload;
    procedure CloseForm(FileName: string); overload;
    function FindNilID: Integer;
    procedure GetLoginUser(key: AnsiString);
    function RunLoadDll(dllname: string; P: TPanel; DBID: string = 'testdb';
      tag: Integer = 0): Boolean;
    procedure CreateMenuTree(const userID: string);
    procedure FillTree(cds: TClientDataSet);
    procedure SetDisplayInfo(const Value: string);
    procedure OpenRunForm(Title,DLLName,DBTag,DLLid : string);
    procedure RunningDll(Node: TcxTreeListNode);
    procedure GetDefaultDir(key: AnsiString);
    procedure MsgUpdateDll(key: AnsiString);
    { Private declarations }
  public
    { Public declarations }
    //
    // 必须有的四个接口对象...
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
    LocalPath: string;

    // 用来在主窗口底部显示当前操作信息
    property DisplayInfo: string read FDisplayInfo write SetDisplayInfo;
  end;

var
  WorkForm: TWorkForm;

implementation

{$R *.dfm}

// 管理动态DLL窗口功能
type
  TDLLRecord = record
    isFill: Boolean; // 记录是否闲置
    FormCount: Integer; // module中窗口数(为零时表示无窗口）
    FormIndex: Integer;
    dllname: string;
    tag: Integer;
    Module: TQBClientModule;
  end;

const
  DLLPath = 'modules\';
  MaxFID = 999999; // 当数字为999999时，表示没有找到

var
  WorkCount: Integer;
  DLLLoad: array of TDLLRecord;
  s_defaultDir : string; //系统当前目录
  myIni : TiniFile;

procedure TWorkForm.SetDisplayInfo(const Value: string);
begin
  FDisplayInfo := Value;
  stInfo.Panels[3].Text := Value;
end;

// ---------------------------------------------------------------------------
// 动态窗口功能
{$REGION '动态窗口功能'}

function TWorkForm.AddForm(FileName: string): Integer;
// fileName : DLL文件名同一文件名只开一次，不允许一个DLL调用多次
// 返回值： 当前DLLLoad的High个数
var
  i: Integer;
begin
  // 判断是否DLL已调用
  i := FindFormID(FileName);
  if i < MaxFID then
    exit(i); // 已调用，返回调用DLL所ID号直接退出
  i := FindNilID;
  Memo1.Lines.Add('---- Find ID : ' + inttostr(i));
  try
    if i = MaxFID then
    begin
      // 新增DLLLoad记录
      i := WorkCount;
      inc(WorkCount);
      setlength(DLLLoad, WorkCount);
      DLLLoad[i].dllname := DLLPath + FileName;
      DLLLoad[i].Module := TQBClientModule.Create(self);
      DLLLoad[i].Module.UserConnection := UserConn;
      DLLLoad[i].Module.DllFilename := DLLLoad[i].dllname;
      DLLLoad[i].Module.Mount(application, screen);
      DLLLoad[i].FormIndex := DLLLoad[i].Module.FormCreate;
      inc(DLLLoad[i].FormCount);
      DLLLoad[i].isFill := true;
    end
    else
    begin
      // 空记录重复利用
      DLLLoad[i].dllname := DLLPath + FileName;
      DLLLoad[i].Module.DllFilename := DLLLoad[i].dllname;
      DLLLoad[i].Module.Mount(application, screen);
      DLLLoad[i].FormIndex := DLLLoad[i].Module.FormCreate;
      inc(DLLLoad[i].FormCount);
      DLLLoad[i].isFill := true;
      Memo1.Lines.Add('---- user ID : ' + inttostr(i));
    end;
  except
    i := MaxFID; // 表示创建失败
  end;
  result := i;
end;

procedure TWorkForm.CloseForm(ID: Integer);
// 释放相应ID的form,dismount相应module
begin
  // 没有窗口句柄或module为空则退出
  if (ID = MaxFID) or (Not DLLLoad[ID].isFill) or (DLLLoad[ID].tag = 0) then
    exit;
  DLLLoad[ID].Module.FormFree(DLLLoad[ID].FormIndex);
  DLLLoad[ID].dllname := '';
  DLLLoad[ID].FormIndex := -1;
  DLLLoad[ID].FormCount := 0;
  DLLLoad[ID].isFill := false;
  DLLLoad[ID].tag := 0;
  DLLLoad[ID].Module.Dismount;
  // Module不能free,否则内存报错，关闭程序时由系统进行回收
end;

procedure TWorkForm.CloseForm(FileName: string);
// 释放相应DLL的form,dismount相应module
var
  ID: Integer;
begin
  ID := FindFormID(FileName);
  // 没有窗口句柄或module为空则退出
  if (ID = MaxFID) or (Not DLLLoad[ID].isFill) or (DLLLoad[ID].tag = 0) then
    exit;
  DLLLoad[ID].Module.FormFree(DLLLoad[ID].FormIndex);
  DLLLoad[ID].dllname := '';
  DLLLoad[ID].FormIndex := -1;
  DLLLoad[ID].FormCount := 0;
  DLLLoad[ID].isFill := false;
  DLLLoad[ID].tag := 0;
  DLLLoad[ID].Module.Dismount;
end;

function TWorkForm.FindFormID(FileName: string): Integer;
var
  i: Integer;
  isopen: Boolean;
begin
  isopen := false;
  for i := Low(DLLLoad) to High(DLLLoad) do
    if DLLLoad[i].dllname = DLLPath + FileName then
    begin
      isopen := true;
      break;
    end;
  if isopen then
    exit(i)
  else
    exit(MaxFID);
end;

function TWorkForm.FindNilID: Integer;
// 根据 isFill判断是否有空记录
var
  i: Integer;
  isNil: Boolean;
begin
  isNil := false;
  for i := Low(DLLLoad) to High(DLLLoad) do
    if Not DLLLoad[i].isFill then
    begin
      isNil := true;
      break;
    end;
  if isNil then
    exit(i)
  else
    exit(MaxFID);
end;
{$ENDREGION}
// ---------------------------------------------------------------------------
//
// ----------消息响应----------------------------------------------------------
{$REGION '消息响应模块'}

const
  Dll_KEY = 'main_dll';

//发消息升级DLL
procedure TWorkForm.MsgUpdateDll(key : AnsiString);
//key组成：0:DLL_KEY 1：DLL_Name 2:DLLid 3:DBTag
const
  mn = 'QBClient_DLLUpdate'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

  // 获取默认目录
procedure TWorkForm.GetDefaultDir(key: AnsiString);
const
  mn = 'QBClient_defaultDir'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

  // 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: String);
var
  Msgs: TQBWinMessages;
  j: Integer;
  msg: AnsiString;
begin
  msg := AnsiString(aMsg);
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(msg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

//
// 控制终止程序的过程...
procedure TWorkForm.Shutdown;
var
  Msgs: TQBWinMessages;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_Shutdown');
  Msgs.PostUserMessage('QBClient_Shutdown', Integer(application.Handle), 0);
  Msgs.RemoveUserMessage('QBClient_Shutdown');
  FreeAndNil(Msgs);
end;

//
// 控制重启程序的过程...
procedure TWorkForm.Restart;
var
  Msgs: TQBWinMessages;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_Restart');
  Msgs.PostUserMessage('QBClient_Restart', Integer(application.Handle), 0);
  Msgs.RemoveUserMessage('QBClient_Restart');
  FreeAndNil(Msgs);
end;

// 处理接收到的消息
procedure TWorkForm.winMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(PChar(MsgName), PChar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> Dll_KEY) then
        exit; // 不是发给自己的消息，退出
      stInfo.Panels[1].Text := InParcel.GetStringGoods('LoginUser');
      // 获取用户后进行动态菜单加载
      CreateMenuTree(InParcel.GetStringGoods('LoginUser'));
    end;
    exit;
  end;
  // 获取默认目录
  if (StrComp(PChar(MsgName), PChar('Main_defaultDir')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> Dll_KEY) then
        exit; // 不是发给自己的消息，退出
      s_defaultDir := InParcel.GetStringGoods('DefaultDir');
      if not assigned(myIni) then
           myIni := TiniFile.Create(s_defaultDir + 'module.sys');
    end;
    exit;
  end;
  // 返回DLL升级情况消息
  if (StrComp(PChar(MsgName), PChar('Main_DLLUpdate')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> Dll_KEY) then
        exit; // 不是发给自己的消息，退出
      if Not (InParcel.GetBooleanGoods('UpdateOK')) then
      begin
      application.MessageBox('升级DLL失败','提示');
      exit;
      end;
      openRunForm(InParcel.GetStringGoods('Title'),
                  InParcel.GetStringGoods('DLLName'),
                  InParcel.GetStringGoods('DLLDBTag'),
                  InParcel.GetStringGoods('DLLID')
                  );
    end;
    exit;
  end;
end;
{$ENDREGION}
// -----------------------------------------------------------------------------
// --------------动态生成菜单----------------------------------------------------
{$REGION '动态生成菜单模块'}

procedure TWorkForm.CreateMenuTree(const userID: string);
var
  vq: TQBParcel;
  i: Integer;
  s, t: string;
begin
  DisplayInfo := '加载菜单中……';
  // 读取存储过程数据集
  // 初始化TDBAccessor对象...
  dba.UserConnection := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := 'TS';
  vq := TQBParcel.Create;
  vq.PutStringGoods('@UserID', trim(userID));
  if dba.ReadStoredProcDataset('rht_user_M_P', vq, i) then
    if dba.GetStoredProcDataset(0, cds) then
    begin
      FillTree(cds);
    end;
  dba.GetLastError(s, t);
  if s <> '' then
  begin
    s := '加载菜单错误 : ';
    LogMsg(s);
  end;
  DisplayInfo := userID + '菜单加载成功！';
end;

procedure TWorkForm.ctlClick(Sender: TObject);
begin
 if (ctl.Columns[0].Tag = 1) and (ctl.SelectionCount > 0) and (ctl.Selections[0].Texts[5] <> '-') then
 begin
    DisplayInfo := ctl.Selections[0].Texts[0];
    RunningDll(Ctl.FocusedNode);
 end;
end;

procedure TWorkForm.ctlFocusedNodeChanged(Sender: TcxCustomTreeList;
  APrevFocusedNode, AFocusedNode: TcxTreeListNode);
begin
 if assigned(AFocusedNode)  then
 ctl.Columns[0].Tag := 1 else ctl.Columns[0].Tag := 0;
end;

procedure TWorkForm.FillTree(cds: TClientDataSet);
var
  i: Integer;
  cNode, oNode, tNode, sNode, fNode: TcxTreeListNode;
  cLev: Integer;
begin
  ctl.BeginUpdate;
  cLev := 1;
  cds.First;
  while not cds.Eof do
  begin
    cLev := cds.FieldByName('LEV').AsInteger;
    case cLev of
      1:
        begin
          cNode := ctl.Add;
          oNode := cNode;
        end;
      2:
        begin
          if assigned(oNode) then
          begin
            cNode := oNode.AddChild;
            tNode := cNode;
          end
          else
            continue
        end;
      3:
        begin
          if assigned(tNode) then
          begin
            cNode := tNode.AddChild;
            sNode := cNode;
          end
          else
            continue
        end;
      4:
        begin
          if assigned(sNode) then
          begin
            cNode := sNode.AddChild;
            fNode := cNode;
          end
          else
            continue
        end;
      5:
        begin
          if assigned(fNode) then
          begin
            cNode := fNode.AddChild;
          end
          else
            continue
        end;
    end;
    for i := 0 to cds.FieldCount - 1 do
      cNode.Texts[i] := cds.Fields[i].AsString;
      //菜单是否可显示管控
      cNode.Visible := cds.FieldByName('isDisplay').AsBoolean;
    cds.Next;
  end;
  ctl.EndUpdate;
end;
{$ENDREGION}

// ------------------------------------------------------------------------------
//
// 询问是否退出程序...
procedure TWorkForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := (application.MessageBox('是否确认退出濮耐TS系统？', '退出确认',
    mb_okcancel + mb_iconquestion) = IDOK);
end;

//
procedure TWorkForm.FormShow(Sender: TObject);
// 窗口显示时，初始化控件...
begin
  Module.UserConnection := UserConn;
  {$IFDEF RELEASE}WebBrowser1.Navigate('http://www.punai.com');{$ENDIF}
  WorkCount := 0;
  // 注册消息
  winMessage.RegisterUserMessage('Main_LoginUser');
  winMessage.RegisterUserMessage('Main_DLLUpdate');
  winMessage.RegisterUserMessage('Main_defaultDir');
  // 获得LOGINUSER
  GetLoginUser(Dll_KEY);
  //获取当前默认目录
  GetDefaultDir(DLL_KEY);
end;

//
// 测试重启功能...
procedure TWorkForm.SpeedButton1Click(Sender: TObject);
begin
  { if application.MessageBox('老兄，是否真的要重启程序？', '重启确认',
    mb_okcancel + mb_iconquestion) = IDOK then
    Restart; }
  LogMsg('request the loginuser');
  GetLoginUser(Dll_KEY);
end;

//
// 测试模块...
procedure TWorkForm.SpeedButton2Click(Sender: TObject);
var
  fh: Integer;
begin
  Module.DllFilename := DLLPath + 'general.dll';
  Module.Mount(application, screen);
  fh := Module.FormCreate;
  Module.FormShowModal(fh, self);
  Module.FormFree(fh);
  Module.Dismount;
end;

procedure TWorkForm.SpeedButton3Click(Sender: TObject);
begin
  { CloseForm(0);
    Memo1.Lines.Add('delete '); }
end;

procedure TWorkForm.SpeedButton4Click(Sender: TObject);
// var
// i: Integer;
begin
  { 测试用
    i := FindFormID('ClientModuleSample1.dll');
    if i = MaxFID then
    begin
    i := AddForm('ClientModuleSample1.dll');
    if i = MaxFID then
    begin
    application.MessageBox('调用DLL窗口失败', '错误');
    exit;
    end;
    Memo1.Lines.Add('  DLL : ' + 'ClientModuleSample1.dll');
    Memo1.Lines.Add('DLLLoad Count:' + inttostr(high(DLLLoad) + 1) +
    'current is ' + inttostr(i));
    DLLLoad[i].Module.InputParcel.Clear;
    DLLLoad[i].Module.InputParcel.PutAnsiStringGoods('FormCaption', '测试的普通窗体哦');
    DLLLoad[i].Module.InputParcel.PutAnsiStringGoods('DatabaseId', 'testdb');
    DLLLoad[i].Module.InputParcel.PutStringGoods('SqlCommand',
    'select top 10 * from customer order by customerid');
    if Not DLLLoad[i].Module.FormDock(DLLLoad[i].FormIndex, Panel1) then
    application.MessageBox('挂靠窗体失败！', 'Infomation');
    end;
    WorkControl.ActivePageIndex := 1 }
end;

//
// 关闭程序...
procedure TWorkForm.SpeedButton5Click(Sender: TObject);
begin
  close;
end;

//
// 主界面窗口关闭时，终止程序...
procedure TWorkForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  ID: Integer;
begin
  for ID := Low(DLLLoad) to High(DLLLoad) do
  begin
    CloseForm(ID);
  end;
  setlength(DLLLoad, 0);

  //清理iniFile
  if assigned(myIni) then myIni.Free;

  Shutdown;
end;

// -----------------------------------------------------------------------------
procedure TWorkForm.WorkControlCanCloseEx(Sender: TObject; ATabIndex: Integer;
  var ACanClose: Boolean);
// 关闭MainPagecontrol时关闭调用的dll窗口
var
  i: Integer;
begin
  for i := Low(DLLLoad) to High(DLLLoad) do
  begin
    if DLLLoad[i].tag = TcxPageControl(Sender).Pages[ATabIndex].tag then
    begin
      CloseForm(i);
      break;
    end;
  end;
end;

// 动态DLL加载功能函数
function TWorkForm.RunLoadDll(dllname: string; P: TPanel; DBID: string;
  tag: Integer): Boolean;
var
  i: Integer;
begin
  result := false;
  i := FindFormID(dllname);
  if i = MaxFID then
  begin
    i := AddForm(dllname);
    if i = MaxFID then
    begin
      application.MessageBox('调用DLL窗口失败', '错误');
      exit(false);
    end;
    Memo1.Lines.Add('  DLL : ' + dllname);
    Memo1.Lines.Add('DLLLoad Count:' + inttostr(high(DLLLoad) + 1) +
      'current is ' + inttostr(i) + '  TAG is : ' + inttostr(tag));
    DLLLoad[i].tag := tag;
    DLLLoad[i].Module.InputParcel.Clear;
    DLLLoad[i].Module.InputParcel.PutAnsiStringGoods('FormCaption', '测试的普通窗体哦');
    DLLLoad[i].Module.InputParcel.PutAnsiStringGoods('DatabaseId', DBID);
    if Not DLLLoad[i].Module.FormDock(DLLLoad[i].FormIndex, P) then
    begin
      application.MessageBox('挂靠窗体失败！', 'Infomation');
      exit(false);
    end;
    result := true;
  end;
end;

// 加载dll主函数
procedure TWorkForm.LoadDLL(Sender: TObject);
var
  i, tag: Integer;
  t: TcxTabSheet;
  P: TPanel;
  isHave, ok: Boolean;
begin
  tag := TButton(Sender).tag;
  if tag = 0 then
    exit;
  // 已打开窗口直接显示
  isHave := false;
  for i := 1 to WorkControl.PageCount - 1 do
    if tag = WorkControl.Pages[i].tag then
    begin
      isHave := true;
      WorkControl.ActivePageIndex := i;
      break;
    end;
  if isHave then
    exit;
  // 建立新窗口
  ok := false;
  t := nil;
  P := nil;
  try
    t := TcxTabSheet.Create(WorkControl);
    t.PageControl := WorkControl;
    t.Caption := TButton(Sender).Caption;
    t.tag := TButton(Sender).tag;
    t.Visible := false;
    P := TPanel.Create(t);
    P.Parent := t;
    P.Caption := '';
    P.BevelOuter := bvNone;
    P.Align := alClient;
  except
    on E: exception do
    begin
      LogMsg('Main_dll打开DLL出错，出错信息：' + E.message);
      if assigned(P) then
        FreeAndNil(P);
      if assigned(t) then
        FreeAndNil(t);
      application.MessageBox('打开DLL出错，请重启软件！', 'Infomation');
      exit;
    end;
  end;

  case tag of
    2101:
      ok := RunLoadDll('lyshouhuo.dll', P, 'ERP', tag);
    2102:
      ok := RunLoadDll('lylingliao.dll', P, 'ERP', tag);
    2103:
      ok := RunLoadDll('lydbinfo.dll', P, 'ERP', tag);
    2104:
      ok := RunLoadDll('lyjhc.dll', P, 'ERP', tag);
    2105:
      ok := RunLoadDll('ClientModuleSample1.dll', P, 'testdb', tag);
  end;
  if ok then
  begin
    WorkControl.ActivePage := t;
    t.Visible := true;
  end
  else
  begin
    if assigned(P) then
      FreeAndNil(P);
    if assigned(t) then
      FreeAndNil(t);
  end;
end;

// 加载dll主函数
procedure TWorkForm.OpenRunForm(Title,DllName,DBTag,DLLID : string);
var
  i, tag: Integer;
  t: TcxTabSheet;
  P: TPanel;
  isHave, ok: Boolean;
begin
  tag := strtoint(DLLID);
  if tag = 0 then
    exit;
  // 已打开窗口直接显示
  isHave := false;
  for i := 1 to WorkControl.PageCount - 1 do
    if tag = WorkControl.Pages[i].tag then
    begin
      isHave := true;
      WorkControl.ActivePageIndex := i;
      break;
    end;
  if isHave then
    exit;
  // 建立新窗口
  t := nil;
  P := nil;
  try
    t := TcxTabSheet.Create(WorkControl);
    t.PageControl := WorkControl;
    t.Caption := Title;
    t.tag := tag;
    t.Visible := false;
    P := TPanel.Create(t);
    P.Parent := t;
    P.Caption := '';
    P.BevelOuter := bvNone;
    P.Align := alClient;
  except
    on E: exception do
    begin
      LogMsg('Main_dll打开DLL出错，出错信息：' + E.message);
      if assigned(P) then
        FreeAndNil(P);
      if assigned(t) then
        FreeAndNil(t);
      application.MessageBox('打开DLL出错，请重启软件！', 'Infomation');
      exit;
    end;
  end;

  ok := RunLoadDll(dllname, P, dbtag, tag);

  if ok then
  begin
    WorkControl.ActivePage := t;
    t.Visible := true;
  end
  else
  begin
    if assigned(P) then
      FreeAndNil(P);
    if assigned(t) then
      FreeAndNil(t);
  end;
end;

procedure TWorkForm.RunningDll(Node: TcxTreeListNode);
//处理如何打开dll窗口，若需下载DLL，则交由消息机制异步打开
var
isST,isNew           : boolean;
dllname, dbtag,dllID,version,Lversion,Title,s : string;
begin
  Title    := Node.Texts[0];
  dllName  := Node.Texts[5];
  dbtag    := Node.Texts[21];
  dllID    := Node.Texts[4];
  s        := s_defaultDir + 'Modules\' + dllName;
  isST     := FileExists(s);
  version  := qbmisc.getfileversion(s);
  myIni.WriteString('Local',dllID,version);
  Lversion := myIni.ReadString('Remote',dllID,'1.0.0.0');
  isNew    := version >= Lversion;
  if isST and isNew then
  begin
    //直接打开DLL
    openRunForm(Title,dllname,dbtag,dllid);
  end else
  begin
    //发消息升级DLL
    s := DLL_KEY  + ',' + dllName + ',' + dllID + ',' + dbtag + ',' + Title;
    MsgUpdateDll(s);
  end;
end;
end.

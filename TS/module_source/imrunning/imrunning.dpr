﻿//
// imrunning.dpr -- 通用客户端即时运行模块示例工程
//                  Version 2.00 Ansi Edition
//                  Author: Jopher(W.G.Z)
//                  QQ: 779545524
//                  Email: Jopher@189.cn
//                  Homepage: http://www.quickburro.com/
//                  Copyright(C) Jopher Software Studio
//
library imrunning;

uses
   FastMM4,
   winapi.Windows,
   SysUtils,
   Classes,
   UserConnection,
   QBParcel,
   DllSpread,
   TaskCommon,
   QBWinMessages;

{$R *.res}

//
// 执行客户端后台任务的主导出函数，请应用程序员自行修改...
function MainFunction(UserConn: TUserConnection; InParcel: TQBParcel; OutParcel: TQBParcel): boolean; stdcall;
var
   Msgs: TQBWinMessages;
   j,AppHandle: integer;
   tmpstr: ansistring;
begin
   AppHandle:=InParcel.GetIntegerGoods('ApplicationHandle');
//
// 作为示例，这里只向主程序回写一个日志信息...
   Msgs:=TQBWinMessages.Create(nil);
   Msgs.RegisterUserMessage('QBClient_LogMsg');
   tmpstr:='!!! 本日志信息来自即时加载模块！';
   j:=str2mem(tmpstr);
   Msgs.PostUserMessage('QBClient_LogMsg',apphandle,j);
   Msgs.RemoveUserMessage('QBClient_LogMsg');
   FreeAndNil(Msgs);
//
// 返回...
   OutParcel.PutBooleanGoods('ProcessResult',true);
   result:=true;
end;

//
// 主函数...
function LocalProcess(InParcelPtr: integer; var OutParcelPtr: integer): boolean; stdcall;
var
   aUserConn: TUserConnection;
   aInParcel: TQBParcel;
   aOutParcel: TQBParcel;
   j: integer;
begin
   try
      aInParcel:=TQBParcel(InParcelPtr);
   except
      result:=false;
      exit;
   end;
   j:=aInParcel.GetIntegerGoods('UserConnAddr');
   aUserConn:=TUserConnection(Pointer(j));
   aOutParcel:=TQBParcel.Create;
//
// 调用用户编写的任务处理函数...
   InitSocketPool;
   try
      Result:=MainFunction(aUserConn,aInParcel,aOutParcel);
   except
      result:=false;
   end;
   FreeSocketPool;
//
// 返回...
   if result then
      begin
         if aOutParcel.GoodsCount<=0 then
            OutParcelPtr:=0
         else
            begin
               OutParcelPtr:=Parcel2Mem(aOutParcel);
               result:=(OutParcelPtr<>0);
            end;
      end
   else
      OutParcelPtr:=0;
   aOutParcel.free;
end;

//
// 导出...
exports
   LocalProcess;

begin
end.

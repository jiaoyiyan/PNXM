//
// WorkFormUnit.pas -- 工作窗体单元（应用程序员自行修改窗体内容）
//                     Version 1.00 Ansi Edition
//                     Author: Jopher(W.G.Z)
//                     QQ: 779545524
//                     Email: Jopher@189.cn
//                     Homepage: http://www.quickburro.com/
//                     Copyright(C) Jopher Software Studio
//
unit WorkFormUnit;

interface

uses
   winapi.Windows, SysUtils, Forms, DB, DBClient, DbAccessor,
   ExtCtrls, Classes, Controls, Grids, DBGrids,
   UserConnection, QBParcel, StdCtrls;

type
  TWorkForm = class(TForm)
    dba: TDBAccessor;
    DataSource1: TDataSource;
    cds: TClientDataSet;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //
    // 必须有的四个接口对象...
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

var
  WorkForm: TWorkForm;

implementation

{$R *.dfm}

//
// 刷新...
procedure TWorkForm.Button1Click(Sender: TObject);
begin
   dba.readdataset('select * from customer order by customerid',cds);
end;

//
// 退出...
procedure TWorkForm.Button2Click(Sender: TObject);
begin
   cds.Close;
   close;
end;

//
// 窗口打开时，初始化...
procedure TWorkForm.FormShow(Sender: TObject);
begin
   dba.TargetNodeId:=UserConn.UserNodeId;
   dba.UserConnection:=UserConn;
   dba.TargetDatabaseId:='testdb';
   dba.TaskTimeout:=45;
   dba.readdataset('select top 10 * from customer ',cds);
end;

end.

object WorkForm: TWorkForm
  Left = 557
  Top = 210
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #19968#33324#19994#21153#20132#20114#22788#29702' - '#31034#20363#27169#22359
  ClientHeight = 406
  ClientWidth = 779
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 779
    Height = 346
    Align = alClient
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #23435#20307
    TitleFont.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 346
    Width = 779
    Height = 60
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Button1: TButton
      Left = 18
      Top = 18
      Width = 97
      Height = 25
      Caption = #21047#26032'(&R)'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 666
      Top = 18
      Width = 97
      Height = 25
      Caption = #20851#38381'(&X)'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object dba: TDBAccessor
    TaskTimeout = 60
    EnableBCD = False
    IsolationLevel = ilCursorStability
    EnableLoadBalance = False
    Left = 320
    Top = 136
  end
  object DataSource1: TDataSource
    DataSet = cds
    Left = 168
    Top = 136
  end
  object cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 248
    Top = 128
  end
end

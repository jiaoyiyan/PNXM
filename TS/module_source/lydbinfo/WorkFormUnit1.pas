﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, tmsAdvGridExcel;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    Panel3: TPanel;
    Bevel2: TBevel;
    qxm: TImage;
    Label1: TLabel;
    xmLE: TLabeledEdit;
    beginT: TDateTimePicker;
    endT: TDateTimePicker;
    RunQuery: TButton;
    Toxls: TButton;
    ztxz: TRadioGroup;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    ToXlsMX: TButton;
    MGrid: TAdvStringGrid;
    xmF: TPanel;
    Panel5: TPanel;
    Bevel3: TBevel;
    xmsa: TButton;
    xmcl: TButton;
    xmok: TButton;
    xmca: TButton;
    xmcb: TAdvStringGrid;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxxz: TRadioGroup;
    PrivilegeCDS: TClientDataSet;
    Memo1: TMemo;
    Button1: TButton;
    agexp: TAdvGridExcelIO;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure ztxzClick(Sender: TObject);
    procedure qxmClick(Sender: TObject);
    procedure xmclClick(Sender: TObject);
    procedure xmokClick(Sender: TObject);
    procedure xmsaClick(Sender: TObject);
    procedure xmcaClick(Sender: TObject);
    procedure RunQueryClick(Sender: TObject);
    procedure ToxlsClick(Sender: TObject);
    procedure ToXlsMXClick(Sender: TObject);
    procedure MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure qbmonClick(Sender: TObject);
    procedure qbYearExit(Sender: TObject);
    procedure cxxzClick(Sender: TObject);
    procedure MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure Button1Click(Sender: TObject);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function XmCount(const zh: string): Integer;
    procedure XmcbClose;
    procedure XmcbOpen;
    procedure SetTimeRange;
    function XmcbSelectCount: Integer;
    function RunCDS(SQL: string): boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet);
    procedure MGridColTitle(I: Integer);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure MGRid3TypeHeaderSet(const Cds: TClientDataSet);
    procedure MGRid3Fill(const Cds: TClientDataSet);
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'LYdbInfo_DLL';
  SPCHAR = '-';
  DefaultZT = 'GWGS';
  KeyCol = 11; // 在GRID中用于区别汇总与明细的列
  // Grid列表标题
  HZCC = 19; // 领料表标题列数
  DBCC = 42; // 单包信息明细列数
  CWCC = 8; // 财务单包领料基本列数
  BaoP = 3;//每个包号合并单元格后下属的col列数
var
  zhanghu: string;
  CurrentZT: string;
  softWareAuth: boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');
  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');
  //
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 界面初始化
  // 默认为当天
  endT.Date := now;
  beginT.Date := now;

  // 默认期别 年度为本年 月份为本月
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;

  // 期别显示(默认不显示,只有当项目编号为单选时才会出现)
  TimeP.Visible := false;

  // 获取是否可修改项目编号权限,设置xeLE的只读性
  GetPrivilege('ASSERT', '1', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    xmLE.ReadOnly := True
  else
    xmLE.ReadOnly := PrivilegeCDS.FieldByName('R1').AsString = '0';
end;

// -----------------------------------------------------------------------------
// 业务代码
Const
  HZCN = '包号,料件编号,品号,规格,牌号,配方,数量单位,重量单位,新砌数量汇总,新砌重量汇总,维修数量汇总,维修重量汇总,分摊数量汇总,分摊重量汇总,项目编号,项目名称,来源公司,现场仓库,仓库名称';
  DBCN = '项目包号,砌筑日期,完工日期,上线日期,下线日期,料件编号,品名,规格,牌号,配方,数量单位,重量单位,单据日期,新砌单号,数量,重量,'
    + '一次维修单号,数量,重量,二次维修单号,数量,重量,三次维修单号,数量,重量,四次维修单号,数量,重量,五次维修单号,数量,重量,六次及以上单号,数量,重量,'
    + '分摊单号,数量,重量,项目编号,项目名称,' + '来源公司,现场仓库,仓库名称';
  CWCN = '序号,料件编号,品号,规格,牌号,配方,数量单位,来源公司';
  // 查询用户负责的项目编号集
  QueryXmSQL = 'SELECT gem01,gem02 FROM %sgem_file where ta_gem32 = ''%s''';
  // 查询主SQL
  HZSQL = '	SELECT immud02,imn03,ima02,ima021,imaud02,imaud03,ima25,ima907,A_T2,A_T5,B_T2,B_T5,C_T2,C_T5,imm14,gem02,imn06,imn15,imd02 FROM 	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,immud02,imn06,imn15,imn03,A_T2,A_T5,B_T2,B_T5,C_T2,C_T5 FROM 	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT DISTINCT imm14,immud02,DECODE(imn06,''GWGS'',''高温公司'',''GNGS'',''功能公司'',''YKGS'',''营口公司'',''-'') imn06,imn15,imn03,immud03,Timn42,Timn45 FROM	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,SUBSTR(immud02,0,INSTR(immud02,''-'')-1) immud02,SUBSTR(imn06,0,INSTR(imn06,''-'') -1) imn06,imn15,imn03,immud03,	 '
    + #13#10 +
    '	  SUM(imn42)OVER(PARTITION BY imm14,SUBSTR(immud02,0,INSTR(immud02,''-'')-1),SUBSTR(imn06,0,INSTR(imn06,''-'') -1),imn03,immud03) AS Timn42,	 '
    + #13#10 +
    '	  SUM(imn45)OVER(PARTITION BY imm14,SUBSTR(immud02,0,INSTR(immud02,''-'')-1),SUBSTR(imn06,0,INSTR(imn06,''-'') -1),imn03,immud03) AS Timn45 	 '
    + #13#10 + '	     FROM %simn_file,%0:simm_file 	 ' + #13#10 +
    '	      WHERE  imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL %s	 '
    + #13#10 + '	) aa)	 ' + #13#10 + '	PIVOT	 ' + #13#10 +
    '	( MAX(Timn42) T2,MAX(Timn45) T5 FOR immud03 IN (''1'' AS A,''2'' AS B,''3'' AS C)	 '
    + #13#10 +
    '	)) xx,%0:sima_file,%0:sgem_file,%0:simd_file WHERE xx.imn03 = ima01 AND xx.imm14 = gem01 AND xx.imn15 = imd01 	 '
    + #13#10 + '	    ORDER BY imm14,immud02,imn06,imn15,imn03	 ';

  DBSQL = '	SELECT immud02,tc_pjy04,tc_pjy04,tc_pjy06,tc_pjy07,imn03,ima02,ima021,imaud02,imaud03,ima25,ima907,imm17,'
    + #13#10 +
    '	A_M,A_T2,A_T5,B1_M,B1_T2,B1_T5, B2_M,B2_T2,B2_T5,B3_M,B3_T2,B3_T5,B4_M,B4_T2,B4_T5,B5_M,B5_T2,B5_T5,B6_M,B6_T2,B6_T5,C_M,C_T2,C_T5,imm14,gem02,imn06,imn15,imd02 FROM 	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,immud02,imn06,imn15,imn03,imm17,A_M,A_T2,A_T5,B1_M,B1_T2,B1_T5, B2_M,B2_T2,B2_T5,B3_M,B3_T2,B3_T5,B4_M,B4_T2,B4_T5,B5_M,B5_T2,B5_T5,B6_M,B6_T2,B6_T5,C_M,C_T2,C_T5 FROM 	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,immud02,DECODE(imn06,''GWGS'',''高温公司'',''GNGS'',''功能公司'',''YKGS'',''营口公司'',''-'') imn06,imn15,imn03,imm17,imm01,immud03,immud10,imn42,imn45 FROM	 '
    + #13#10 + '	(	 ' + #13#10 +
    '	SELECT imm14,immud02,SUBSTR(imn06,0,INSTR(imn06,''-'') -1) imn06,imn15,imn03,imm17,imm01,immud03,nvl(immud10,1) immud10,imn42,imn45 	 '
    + #13#10 + '	     FROM %simn_file,%0:simm_file 	 ' + #13#10 +
    '	      WHERE  imn01 = imm01 AND imm03 = ''Y''  AND immconf = ''Y'' AND regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL  %s	 '
    + #13#10 + '	) aa)	 ' + #13#10 + '	PIVOT	 ' + #13#10 +
    '	( max(imm01) M,SUM(imn42) T2,SUM(imn45) T5 FOR (immud03,immud10) IN ((''1'',1) AS A,(''2'',1) AS B1,(''2'',2) AS B2,(''2'',3) AS B3,(''2'',4) AS B4,(''2'',5) AS B5,(''2'',6) AS B6,(''3'',1) AS C)	 '
    + #13#10 + '	)	 ' + #13#10 +
    '	) xx,%0:stc_pjy_file,%0:sima_file,%0:sgem_file,%0:simd_file WHERE xx.immud02 = tc_pjy01 AND xx.imm14 = tc_pjy03 AND xx.imn03 = ima01 AND xx.imm14 = gem01 AND xx.imn15 = imd01 	 '
    + #13#10 + '	    ORDER BY imm14,immud02,imn06,imn15,imm17,imn03	 ';
  cwsql = '  SELECT imn03,immud02,ima02,ima021,imaud02,imaud03,ima25,ima907,' +
    ' A_T2,A_T5,B_T2,B_T5,C_T2,C_T5,imm14,imn06 FROM ' + slinebreak + '  (     '
    + slinebreak + '  SELECT imm14,immud02,imn06,imn15,imn03,' +
    '  A_T2,A_T5,B_T2,B_T5,C_T2,C_T5 FROM         ' + slinebreak +
    '  (                                          ' + slinebreak +
    '  SELECT DISTINCT imm14,immud02,             ' +
    '  DECODE(imn06,''GWGS'',''高温公司'',''GNGS'',''功能公司'',' +
    '  ''YKGS'',''营口公司'',''-'') imn06,imn15,imn03,immud03,' +
    '  Timn42,Timn45 FROM                         ' + slinebreak +
    '  (                                          ' + slinebreak +
    '  SELECT imm14,immud02,SUBSTR(imn06,0,INSTR(imn06,''-'') -1) imn06,' +
    ' imn15,imn03,immud03,                        ' + slinebreak +
    '    SUM(imn42)OVER(PARTITION BY imm14,immud02,' +
    ' SUBSTR(imn06,0,INSTR(imn06,''-'') -1),imn03,immud03) AS Timn42,' +
    slinebreak + '    SUM(imn45)OVER(PARTITION BY imm14,immud02,' +
    ' SUBSTR(imn06,0,INSTR(imn06,''-'') -1),imn03,immud03) AS Timn45 ' +
    slinebreak + '       FROM %simn_file,%0:simm_file           ' + slinebreak +
    '        WHERE  imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND ' +
    ' regexp_like(imm01,''CL01'') AND immud03 IS NOT NULL  %s      ' +
    slinebreak +
    '  ) aa)                                                       ' +
    slinebreak +
    '  PIVOT                                                       ' +
    slinebreak + '  ( MAX(Timn42) T2,MAX(Timn45) T5 FOR ' +
    ' immud03 IN (''1'' AS A,''2'' AS B,''3'' AS C) ' + slinebreak +
    '  )) xx,%0:sima_file WHERE xx.imn03 = ima01    ' + slinebreak +
    '      ORDER BY imm14,imn03,immud02,imn06';

var
  baomList: string; // 财务报表中包表列表
  BaoRowNum : integer; //财务报表中的列数

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.XmCount(const zh: string): Integer;
// 返回账号所负责的项目数目
var
  I: Integer;
  SQL: string;
begin
  if zh = '' then
    exit(0);
  // 查询返回项目数
  if CurrentZT = '' then
    CurrentZT := DefaultZT;

  SQL := Format(QueryXmSQL, [CurrentZT + '.', zh]);
  if Not RunCDS(SQL) then
    exit(0);

  if Cds.RecordCount = 0 then
    exit(0);

  dba.ReadDataset(SQL, Cds);
  if Cds.RecordCount = 0 then
    exit(0);

  xmcb.FixedCols := 0;
  xmcb.ColWidths[0] := 20;
  xmcb.Options := xmcb.Options + [goRowSelect, goEditing];
  xmcb.ShowSelection := false;

  // 在GRID中显示项目
  xmcb.RowCount := Cds.RecordCount + 1;
  Cds.First;
  I := 1;
  while not Cds.Eof do
  begin
    xmcb.AddCheckBox(0, I, false, false);
    xmcb.Cells[1, I] := Cds.Fields.Fields[0].AsString;
    xmcb.Cells[2, I] := Cds.Fields.Fields[1].AsString;
    inc(I);
    Cds.Next;
  end;
  Result := Cds.RecordCount;
end;

procedure TWorkForm.xmokClick(Sender: TObject);
// 确定返回的选中项目号写入项目编号中。若全不选中，则弹出出错框。若全选，则筛选条件不包括项目编号条件
var
  I: Integer;
  State: boolean;
  Value: TstringList;
begin
  Value := TstringList.Create;

  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, I, State);
    if State then
      Value.Append(xmcb.Cells[1, I]);
  end;
  if Value.Count = 0 then
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
    begin
      Value.Free;
      exit;
    end;

  xmLE.Text := Value.DelimitedText; // 返回项目统计选择项
  Value.Free;
  XmcbClose;
end;

procedure TWorkForm.xmsaClick(Sender: TObject);
var
  I: Integer;
begin
  // 设置所有项目编号全选中
  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, I, True);
    xmcb.RowColor[I] := xmcb.SelectionColor;
  end;
end;

procedure TWorkForm.ztxzClick(Sender: TObject);
var
  s: string;
begin
  // 切换当前选择项，获取当前账套
  case ztxz.ItemIndex of
    1:
      CurrentZT := 'XCXD' // 现场测试账套
  else
    CurrentZT := DefaultZT;
  end;
  // 获取新的项目编号
  xmF.Visible := false;
  TimeP.Visible := false;
  s := xmcb.ColumnHeaders.DelimitedText;
  xmcb.ClearAll;
  xmcb.ColumnHeaders.DelimitedText := s;
  if XmCount(zhanghu) = 0 then
    application.MessageBox(Pchar('当前账套下没有账户：' + zhanghu + ' 所负责的项目!'), '提示');

  // 界面初始化
  xmLE.Text := '';

  s := MGrid.ColumnHeaders.DelimitedText;
  MGrid.ClearAll;
  MGrid.ColumnHeaders.DelimitedText := s;

  self.Caption := '收货查询 易拓账号: ' + zhanghu + '  当前账套： ' + ztxz.Items
    [ztxz.ItemIndex];
end;

procedure TWorkForm.XmcbOpen;
// 打开项目编号选择
begin
  MasterP.Enabled := false;
  xmF.Visible := True;
end;

procedure TWorkForm.SetTimeRange;
// 根据不同条件,对收货筛选时间进行自动化处理
var
  y, m, d: word;
  SQL: string;
begin
  // 期别的影响
  if Not TimeP.Visible then
    exit;

  SQL := 'SELECT  NVL(ta_gem33,1) dd FROM ' + CurrentZT +
    '.gem_file WHERE gem01 = ''' + trim(xmLE.Text) + '''';
  if Not RunCDS(SQL) then
    exit;
  Cds.First;
  if Cds.RecordCount > 1 then
    exit; // 查询的结果大于1条,不可能,直接退出
  y := strtoint(qbYear.Text);
  m := qbmon.ItemIndex + 1;
  d := Cds.FieldByName('dd').AsInteger;
  if d = 1 then
  begin
    beginT.DateTime := EncodeDate(y, m, d);
    endT.DateTime := IncMonth(EncodeDate(y, m, d), 1) - 1;
  end
  else
  begin
    beginT.DateTime := incMonth(EncodeDate(y, m, d), -1) + 1;
    endT.DateTime := EncodeDate(y, m, d);
  end;
end;

procedure TWorkForm.XmcbClose;
// 关闭项目编号选择
begin
  xmF.Visible := false;
  MasterP.Enabled := True;
  // 当选中项为1时显示期间选择
  TimeP.Visible := XmcbSelectCount = 1;
  if TimeP.Visible then
    SetTimeRange;
  ResizeForm;
end;

function TWorkForm.XmcbSelectCount: Integer;
// 返回项目编号选择数目
var
  I: Integer;
  State: boolean;
begin
  Result := 0;
  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.GetCheckBoxState(0, I, State);
    if State then
      inc(Result);
  end;
end;

function TWorkForm.RunCDS(SQL: string): boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.qbmonClick(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qbYearExit(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qxmClick(Sender: TObject);
begin
  // 返回项目编号选择,选择前后MasterP大小发生变化不知为何，重新定义其大小
  XmcbOpen;
  ResizeForm;
end;

procedure TWorkForm.xmcaClick(Sender: TObject);
begin
  // 直接退出，设置xmf不可见。保持原筛选条件
  if XmcbSelectCount = 0 then
  begin
    if MessageBox(self.Handle, '没有选中任何项目，是否要关闭？', '提示',
      MB_ICONINFORMATION + MB_OkCancel) = idCancel then
      exit;
  end;
  XmcbClose;
end;

procedure TWorkForm.xmclClick(Sender: TObject);
var
  I: Integer;
begin
  // 设置所有项目编号全不选中
  for I := 1 to xmcb.RowCount - 1 do
  begin
    xmcb.SetCheckBoxState(0, I, false);
    xmcb.RowColor[I] := xmcb.Color;
  end;
end;

function TWorkForm.GetFilterS: string;
// 返回动动态筛选条件
// 若项目编号选择为空，则带出所有项目编号信息
var
  FS: TstringList;
  I: Integer;
  bt, et: Tdate;
begin
  Result := '';
  FS := TstringList.Create;

  if xmLE.Text = '' then
  // 若项目编号选择为空，则带出所有项目编号信息
  begin
    for I := 1 to xmcb.RowCount - 1 do
      FS.Append(xmcb.Cells[1, I]);
    xmLE.Text := FS.DelimitedText;
  end
  else
    FS.DelimitedText := xmLE.Text;
  for I := 0 to FS.Count - 1 do
    FS[I] := '''' + FS[I] + '''';

  Result := ' AND imm14 IN (' + FS.DelimitedText + ') ';
  FS.Free;

  bt := beginT.Date;
  et := endT.Date;
  if et < bt then
  begin
    Result := 'Error : 结束日期不可小于开始时间！';
    exit;
  end;

  Result := Result + ' AND imm17 >= to_date(''' + FormatDateTime('yyyymmdd', bt)
    + ''',''yyyymmdd'') AND ' + ' imm17 <= to_date(''' +
    FormatDateTime('yyyymmdd', et) + ''',''yyyymmdd'')';
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
// 查寻
var
  filter, SQL: string;
begin
  if Not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;

  // 主查询
  screen.Cursor := crSQLWait;
  filter := GetFilterS;

  if filter[1] = 'E' then // 返回值以E开头，表示值有问题，直接退出
  begin
    application.MessageBox(Pchar(filter), '错误');
    exit;
  end;

  try
    case cxxz.ItemIndex of
      0:
        SQL := Format(HZSQL, [CurrentZT + '.', filter]);
      1:
        SQL := Format(DBSQL, [CurrentZT + '.', filter]);
      2:
        SQL := Format(cwsql, [CurrentZT + '.', filter]);
    end;

    // Memo1.Text := SQL;
    // exit;

    RunCDS(SQL);

    if Cds.RecordCount = 0 then
      application.MessageBox('没有查询到相关记录，请修改查询条件后继续！', '提示')
    else
      FillmGrid(Cds);
    ResizeForm;
  finally
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToxlsClick(Sender: TObject);
// 将领料汇总值报表导入excel
const
  XMNO = 14; // 项目编号所在的列数
var
  str: string;
  I, j, m, y: Integer;
  excelapp, sheet: Variant;
  xmName, colName, Bcaption, XMRQ: string; // 项目编号 / 列标题 / 尾部标识行内容 /筛选时间
  FSQL, SQL: string;
  FS: TstringList;
begin
  screen.Cursor := crAppStart;

  // 1.取期别为beginT的期别,获取项目包结存信息
  m := MonthOf(beginT.Date);
  y := YearOf(beginT.Date);
  FS := TstringList.Create;
  if xmLE.Text = '' then
  // 若项目编号选择为空，则带出所有项目编号信息
  begin
    for I := 1 to xmcb.RowCount - 1 do
      FS.Append(xmcb.Cells[1, I]);
    xmLE.Text := FS.DelimitedText;
  end
  else
    FS.DelimitedText := xmLE.Text;
  for I := 0 to FS.Count - 1 do
    FS[I] := '''' + FS[I] + '''';
  FSQL := Format(' tc_pjz10 IN (%s) AND tc_pjz02 = %d AND tc_pjz03 = %d',
    [FS.DelimitedText, y, m]);
  FS.Free;

  SQL := 'SELECT DISTINCT tc_pjz10,tc_pjz02,tc_pjz03,nvl(tc_pjz19,0) tc_pjz19,'
    + 'nvl(tc_pjz20,0) tc_pjz20,nvl(tc_pjz21,0) tc_pjz21,nvl(tc_pjz22,0) tc_pjz22 '
    + ' FROM tc_pjz_file WHERE ' + FSQL;
  RunCDS(SQL);

  str := '';

  // 时间范围
  XMRQ := str + '筛选时间从： ' + char(9) + FormatDateTime('yyyy/mm/dd', beginT.Date)
    + char(9) + '  到： ' + char(9) + FormatDateTime('yyyy/mm/dd', endT.Date);

  colName := '';
  Bcaption := '';
  for I := 0 to MGrid.ColCount - 1 do
  // 显示列标题
  begin
    colName := colName + MGrid.ColumnHeaders[I] + char(9);
    Bcaption := Bcaption + '================' + char(9);
  end;

  xmName := '';
  // 显示内容
  for I := 1 to MGrid.RowCount - 1 do
  begin
    if xmName <> MGrid.Cells[XMNO, I] then
    // 项目变化,新增单头
    begin
      // 项目筛选记录
      if xmName = '' then
        str := '项目编号 ： ' + char(9) + MGrid.Cells[XMNO, I] + #13 + XMRQ
      else
        str := str + Bcaption + #13 + #13 + '项目编号 ： ' + char(9) + MGrid.Cells
          [XMNO, I] + #13 + XMRQ;
      xmName := MGrid.Cells[XMNO, I];

      if Not Cds.Locate('tc_pjz10', MGrid.Cells[XMNO, I], []) then
        Continue;
      str := str + #13 + '上月结存包个数: ' + char(9) + Cds.FieldByName('tc_pjz19')
        .AsString + char(9) + '本月做包个数: ' + char(9) + Cds.FieldByName('tc_pjz20')
        .AsString + char(9) + '本月下线包个数: ' + char(9) +
        Cds.FieldByName('tc_pjz21').AsString + char(9) + '本月结存包个数: ' + char(9) +
        Cds.FieldByName('tc_pjz22').AsString + char(9) + #13 + #13 +
        colName + #13;
    end;
    for j := 0 to MGrid.ColCount - 1 do
    begin
      str := str + MGrid.Cells[j, I] + char(9);
    end;
    str := str + #13;
    application.ProcessMessages;
  end;
  try
    try
      clipboard.Clear;
      clipboard.Open;
      clipboard.AsText := str;
      clipboard.close;
      excelapp := createoleobject('excel.application');
      excelapp.workbooks.add(1);
      sheet := excelapp.workbooks[1].worksheets[1];
      sheet.name := 'sheet1';
      sheet.paste;
      clipboard.Clear;
      excelapp.Visible := True;
    except
      application.MessageBox('Excel未安装', '提示');
    end;
  finally
    clipboard.Clear;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.ToXlsMXClick(Sender: TObject);
// 将明细表值导入excel
const
  XMNO = 37; // 项目编号所在的列数
var
  str: string;
  I, j: Integer;
  excelapp, sheet: Variant;
  xmName, colName, Bcaption, XMRQ: string; // 项目编号 / 列标题 / 尾部标识行内容 /筛选时间
begin
  screen.Cursor := crAppStart;

  str := '';

  // 时间范围
  XMRQ := str + '筛选时间从： ' + char(9) + FormatDateTime('yyyy/mm/dd', beginT.Date)
    + char(9) + '  到： ' + char(9) + FormatDateTime('yyyy/mm/dd', endT.Date);

  colName := '';
  Bcaption := '';
  for I := 0 to MGrid.ColCount - 1 do
  // 显示列标题
  begin
    colName := colName + MGrid.ColumnHeaders[I] + char(9);
    Bcaption := Bcaption + '================' + char(9);
  end;

  xmName := '';
  // 显示内容
  for I := 1 to MGrid.RowCount - 1 do
  begin
    if xmName <> MGrid.Cells[XMNO, I] then
    // 项目变化,新增单头
    begin
      // 项目筛选记录
      if xmName = '' then
        str := '项目编号 ： ' + char(9) + MGrid.Cells[XMNO, I] + #13 + XMRQ
      else
        str := str + Bcaption + #13 + #13 + '项目编号 ： ' + char(9) + MGrid.Cells
          [XMNO, I] + #13 + XMRQ;
      str := str + #13 + colName + #13;
      xmName := MGrid.Cells[XMNO, I];
    end;
    for j := 0 to MGrid.ColCount - 1 do
    begin
      str := str + MGrid.Cells[j, I] + char(9);
    end;
    str := str + #13;
    application.ProcessMessages;
  end;
  try
    try
      clipboard.Clear;
      clipboard.Open;
      clipboard.AsText := str;
      clipboard.close;
      excelapp := createoleobject('excel.application');
      excelapp.workbooks.add(1);
      sheet := excelapp.workbooks[1].worksheets[1];
      sheet.name := 'sheet1';
      sheet.paste;
      clipboard.Clear;
      excelapp.Visible := True;
      MGrid.ContractAll;
      screen.Cursor := crDefault;
    except
      application.MessageBox('Excel未安装', '提示');
    end;
  finally
    clipboard.Clear;
    screen.Cursor := crDefault;
  end;
end;

procedure TWorkForm.MGridGetAlignment(Sender: TObject; ARow, ACol: Integer;
  var HAlign: TAlignment; var VAlign: TVAlignment);
var
  I: Integer;
begin
  case cxxz.ItemIndex of
    0 .. 1:
      I := 1;
    2:
      I := 2;
  end;
  if ARow < I then
  begin
    HAlign := taCenter;
    VAlign := vtaCenter;
  end;
  if I = 2 then
    if (ARow >= 2) and (ACol >= CWCC) then
      HAlign := taRightJustify;
end;

procedure TWorkForm.MGridGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
// 主GRID中汇总行字显示为红色，奇偶行区分开
begin
  if MGrid.Cells[KeyCol, ARow] = SPCHAR then
    AFont.Color := clRed;

  if (ARow mod 2) = 0 then
    ABrush.Color := MGrid.Color
  else
    ABrush.Color := clBtnFace;
end;

procedure TWorkForm.MGridColTitle(I: Integer);
// 根据报表类型刷新主GRID的标题
var
  j: Integer;
  n: string;
begin
  j := 0;
  n := '暂无';
  if I > 1 then
    exit;
  case I of
    0:
      begin
        j := HZCC;
        n := HZCN;
      end;
    1:
      begin
        j := DBCC;
        n := DBCN;
      end;
  end;

  MGrid.ClearAll;
  MGrid.FixedRows := 1;
  MGrid.ColCount := j;
  MGrid.ColumnHeaders.DelimitedText := n;
end;

procedure TWorkForm.MGRid3TypeHeaderSet(const Cds: TClientDataSet);
// 财务查询所使用的GRID表头设计
const
  phs: array [1 .. 3] of string = ('新砌', '维修', '分摊');
var
  BaoList: TstringList;
  HeadStr: TstringList;
  LJList : TstringList;
  BaoNum: Integer;
  ColNum: Integer;
  I, t, n: Integer;
begin
  if Cds.RecordCount = 0 then
    exit;
  MGrid.ClearAll;
  MGrid.FixedRows := 2;

  // 获取包个数
  BaoList := TstringList.Create;
  BaoList.Sorted := True;
  BaoList.Duplicates := dupIgnore;
  HeadStr := TstringList.Create;
  HeadStr.DelimitedText := CWCN;
  //获取料件不重复个数，用于计算报表Row行值
  LJList := TstringList.Create;
  LJList.Sorted := true;
  LJList.Duplicates := dupIgnore;

  Cds.First;
  while Not Cds.Eof do
  begin
    BaoList.add(Cds.FieldByName('immud02').AsString);
    LJList.Add(Cds.FieldByName('imn03').AsString);
    Cds.Next;
  end;
  BaoNum    := BaoList.Count;
  BaoRowNum := LJList.Count;
  baomList := BaoList.DelimitedText;
  t := 0;
  n := 0;
  // 设置Grid列
  ColNum := CWCC + BaoNum * BaoP + 1; // 3是三种单别；1是合计项
  MGrid.ColCount := ColNum;

  // 合并表头列
  for I := 0 to ColNum - 1 do
  begin
    // 通用列的合并处理
    if I < CWCC then
    begin
      MGrid.MergeCells(I, 0, 1, 2);
      MGrid.Cells[I, 0] := HeadStr[I];
    end;
    // 包号动态生成列
    if (I >= CWCC) and (I <= ColNum - 2) then
    begin
      if t = 0 then
      begin
        MGrid.MergeCells(I, 0, 3, 1);
        MGrid.Cells[I, 0] := BaoList[n];
        inc(n);
      end;
      MGrid.Cells[I, 1] := phs[t + 1];
      inc(t);
      if t = 3 then
        t := 0;
    end;

    // 合计列处理
    if I = ColNum - 1 then
    begin
      MGrid.MergeCells(I, 0, 1, 2);
      MGrid.Cells[I, 0] := '合计';
    end;
  end;
  LJList.Free;
  HeadStr.Free;
  BaoList.Free;
end;

procedure TWorkForm.MGRid3Fill(const Cds: TClientDataSet);
// 财务单包领料信息报表填充
var
  I, j, L, RowNum: Integer;
  ljmc, mc, baohao, bh: string;
  bList: TstringList;
begin
  //行数 ＝ 料件唯一个数 + 2列值
  MGrid.RowCount := BaoRowNum + 2;
  RowNum := 1;
  I := 0;
  ljmc := '';
  bList := TstringList.Create;
  bList.DelimitedText := baomList;
  Cds.First;
  while not Cds.Eof do
  begin
    mc := Cds.FieldByName('imn03').AsString;
    if ljmc <> mc then
    begin
      ljmc := mc;
      inc(RowNum);
      // 序号,料件编号,品号,规格,牌号,配方,数量单位,来源公司
      MGrid.Cells[0, RowNum] := inttostr(RowNum - 1);
      MGrid.Cells[1, RowNum] := mc;
      MGrid.Cells[2, RowNum] := Cds.FieldByName('ima02').AsString;
      MGrid.Cells[3, RowNum] := Cds.FieldByName('ima021').AsString;
      MGrid.Cells[4, RowNum] := Cds.FieldByName('imaud02').AsString;
      MGrid.Cells[5, RowNum] := Cds.FieldByName('imaud03').AsString;
      MGrid.Cells[6, RowNum] := Cds.FieldByName('ima25').AsString;
      MGrid.Cells[7, RowNum] := Cds.FieldByName('imn06').AsString;
    end;
    //三种单别的显示
    baohao := Cds.FieldByName('immud02').AsString;
    L := bList.IndexOf(baohao);
    L := CWCC + L * BaoP;
    MGrid.Cells[L, RowNum] := Cds.FieldByName('A_T2').AsString;
    MGrid.Cells[L + 1, RowNum] := Cds.FieldByName('B_T2').AsString;
    MGrid.Cells[L + 2, RowNum] := Cds.FieldByName('C_T2').AsString;
    Cds.Next;
  end;
  //合计列显示 ：保留三位小数
  for I := 2 to MGrid.RowCount - 1 do
    MGrid.Cells[MGrid.ColCount - 1, I] :=
      FormatFloat('0.000', MGrid.RowSum(I, CWCC, MGrid.ColCount - 2));
  bList.Free;
end;

procedure TWorkForm.Button1Click(Sender: TObject);
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(nil);
  with SaveDialog do
  begin
    filter := '*.xls|*.xls';
    if Execute then
    begin
    agexp.XLSExport(saveDialog.FileName + '.xls');
      showmessage('导入成功！');
    end;
  end;
  SaveDialog.Free;
end;

procedure TWorkForm.cxxzClick(Sender: TObject);
begin
  MGridColTitle(cxxz.ItemIndex);
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet);
var
  I, j: Integer;
  s: string;
begin
  if Q.RecordCount = 0 then
    exit;

  MGrid.BeginUpdate;

  if cxxz.ItemIndex = 2 then
  begin
    MGRid3TypeHeaderSet(Q);
    // exit;
    MGRid3Fill(Q);
    // MGrid.Row := 2;
    // MGrid.Col := 1;
  end
  else
  begin
    MGridColTitle(cxxz.ItemIndex);
    with MGrid do
    begin
      MGrid.RowCount := Q.RecordCount + 1;
      I := 1;
      Q.First;
      while not Q.Eof do
      begin
        for j := 0 to Q.FieldCount - 1 do
        begin
          MGrid.Cells[j, I] := Q.Fields.Fields[j].AsString;
        end;
        inc(I);
        Q.Next;
      end;
      Row := 1;
      Col := 0;
    end;
  end;
  MGrid.AutoSizeColumns(false, 5);
  MGrid.EndUpdate;
end;
// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}

// 消息响应事件
// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
      XmCount(zhanghu);
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.

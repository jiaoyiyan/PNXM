//
// WorkFormUnit.pas -- 工作窗体单元（应用程序员自行修改窗体内容）
//                     Version 1.00 Ansi Edition
//                     Author: Jopher(W.G.Z)
//                     QQ: 779545524
//                     Email: Jopher@189.cn
//                     Homepage: http://www.quickburro.com/
//                     Copyright(C) Jopher Software Studio
//
unit WorkFormUnit;

interface

uses
   winapi.Windows, SysUtils, Forms, Buttons, Controls, jpeg, ExtCtrls, Classes, StdCtrls,
   QBParcel, UserConnection, QBJson, QBCommon;

type
  TWorkForm = class(TForm)
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Bevel3: TBevel;
    Label6: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label11: TLabel;
    Label12: TLabel;
    Edit5: TEdit;
    Image1: TImage;
    Bevel1: TBevel;
    procedure FormShow(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //
    // 必须有的四个接口对象...
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
    //
    // 其他自定义全局变量...
    ConfigFileName: string;
    s_key: AnsiString;
    s_userid: AnsiString;
    s_RootNodeId: AnsiString;
    s_RootNodeAddress: AnsiString;
    s_NodeAddressingPort: integer;
    s_RootNodeTaskPort: integer;
    s_UserNodeId: AnsiString;
    s_UserNodeAddress: AnsiString;
    s_UserNodeMsgPort: integer;
    s_UserNodeTaskPort: integer;
    s_ModuleConfigFile: AnsiString;
  end;

var
  WorkForm: TWorkForm;

const
   keyKey='n:5/0-kj8ga&_0a=~h';

implementation

{$R *.dfm}

procedure TWorkForm.BitBtn1Click(Sender: TObject);
var
   p1,p3,p4: integer;
   json: TQBJson;
begin
   if (trim(edit1.Text)='')
      or (trim(edit2.Text)='')
      or (trim(edit3.Text)='')
      or (trim(edit4.Text)='')
      or (trim(edit5.Text)='')
      or (trim(edit6.Text)='')
      or (trim(edit8.Text)='')
      or (trim(edit9.Text)='') then
      begin
         application.MessageBox('参数输入不完整，请重新输入！','操作提示',mb_ok+mb_iconinformation);
         exit;
      end;
   try
      p1:=strtoint(edit4.text);
   except
      p1:=-1;
   end;
   if (p1<=0) or (p1>65535) then
      begin
         application.MessageBox('节点寻址端口无效！(有效范围: 1-65535)','操作提示',mb_ok+mb_iconinformation);
         exit;
      end;
   try
      p3:=strtoint(edit8.text);
   except
      p3:=-1;
   end;
   if (p3<=0) or (p3>65535) then
      begin
         application.MessageBox('节点消息端口无效！(有效范围: 1-65535)','操作提示',mb_ok+mb_iconinformation);
         exit;
      end;
   try
      p4:=strtoint(edit9.text);
   except
      p4:=-1;
   end;
   if (p4<=0) or (p4>65535) then
      begin
         application.MessageBox('节点任务端口无效！(有效范围: 1-65535)','操作提示',mb_ok+mb_iconinformation);
         exit;
      end;
   if (StrIComp(PChar(trim(edit2.Text)),PChar(trim(edit6.text)))<>0) and (pos('#',edit6.Text)<=1) then
      begin
         application.MessageBox('对不起，用户所在节点代码不符合命名规则，请重新输入！','操作提示',mb_ok+mb_iconinformation);
         exit;
      end;
//
// 保存参数...
   s_key:=AnsiString(trim(edit1.Text));
   s_key:=qbcommon.Encrypt(s_key,keykey);
   s_RootNodeId:=AnsiString(trim(edit2.Text));
   s_RootNodeAddress:=AnsiString(trim(edit3.Text));
   s_NodeAddressingPort:=p1;
   s_UserNodeId:=AnsiString(trim(edit6.Text));
   s_UserNodeAddress:=AnsiString(trim(edit7.Text));
   s_UserNodeMsgPort:=p3;
   s_UserNodeTaskPort:=p4;
   s_ModuleConfigFile:=AnsiString(trim(edit5.Text));
   //
   json:=TQBJson.Create;
   json.Put('TransferKey',s_key);
   json.Put('RootNodeId',s_RootNodeId);
   json.Put('RootNodeAddress',s_RootNodeAddress);
   json.Put('NodeAddressingPort',s_NodeAddressingPort);
   json.Put('UserNodeId',s_UserNodeId);
   json.Put('UserNodeAddress',s_UserNodeAddress);
   json.Put('UserNodeMsgPort',s_UserNodeMsgPort);
   json.Put('UserNodeTaskPort',s_UserNodeTaskPort);
   json.Put('ModuleConfigFile',s_ModuleConfigFile);
   json.SaveToFile(ConfigFileName,3);
   FreeAndNil(json);
//
   OutputParcel.PutBooleanGoods('ProcessResult',true);
   close;
end;

procedure TWorkForm.BitBtn2Click(Sender: TObject);
begin
   OutputParcel.PutBooleanGoods('ProcessResult',false);
   close;
end;

procedure TWorkForm.FormShow(Sender: TObject);
var
   json: TQBJson;
begin
   ConfigFileName:=InputParcel.GetStringGoods('ConfigFileName');
//
// 从配置文件读取联网参数...
   if fileexists(ConfigFileName) then
      begin
         json:=TQBJson.create(ConfigFileName,false);
         try
           s_Key:=Json.GetString('TransferKey');
           s_key:=qbcommon.Decrypt(s_key,keykey);
           s_RootNodeId:=json.GetString('RootNodeId');
           s_RootNodeAddress:=json.GetString('RootNodeAddress');
           s_NodeAddressingPort:=json.GetInt('NodeAddressingPort');
           s_UserNodeId:=json.GetString('UserNodeId');
           s_UserNodeAddress:=json.GetString('UserNodeAddress');
           s_UserNodeMsgPort:=json.GetInt('UserNodeMsgPort');
           s_UserNodeTaskPort:=json.GetInt('UserNodeTaskPort');
           s_ModuleConfigFile:=json.GetString('ModuleConfigFile');
         except
           s_Key:='j76s!9l1w~8_9sa00g*+zx';
           s_RootNodeId:='china';
           s_RootNodeAddress:='127.0.0.1';
           s_NodeAddressingPort:=5880;
           s_UserNodeId:='china';
           s_UserNodeAddress:='127.0.0.1';
           s_UserNodeMsgPort:=5882;
           s_UserNodeTaskPort:=5881;
           s_ModuleConfigFile:=AnsiString(extractfilename(ConfigFileName));
           delete(s_ModuleConfigFile,length(s_ModuleConfigFile)-3,4);
           s_ModuleConfigFile:='upgradefiles\'+s_ModuleConfigFile+'.mcf';
         end;
         FreeAndNil(Json);
      end
   else
      begin
         s_Key:='j76s!9l1w~8_9sa00g*+zx';
         s_RootNodeId:='china';
         s_RootNodeAddress:='127.0.0.1';
         s_NodeAddressingPort:=5880;
         s_UserNodeId:='china';
         s_UserNodeAddress:='127.0.0.1';
         s_UserNodeMsgPort:=5882;
         s_UserNodeTaskPort:=5881;
         s_ModuleConfigFile:=AnsiString(extractfilename(ConfigFileName));
         delete(s_ModuleConfigFile,length(s_ModuleConfigFile)-3,4);
         s_ModuleConfigFile:='upgradefiles\'+s_ModuleConfigFile+'.mcf';
      end;
//
// 显示...
   edit1.Text:=string(s_key);
   edit2.Text:=string(s_RootNodeId);
   edit3.Text:=string(s_RootNodeAddress);
   edit4.Text:=inttostr(s_NodeAddressingPort);
   edit6.Text:=string(s_UserNodeId);
   edit7.Text:=string(s_UserNodeAddress);
   edit8.Text:=inttostr(s_UserNodeMsgPort);
   edit9.Text:=inttostr(s_UserNodeTaskPort);
   edit5.Text:=string(s_ModuleConfigFile);
end;

end.

﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel, AdvUtil, AdvObj,
  Vcl.clipbrd, system.UITypes,
  BaseGrid, AdvGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxTextEdit, cxTLdxBarBuiltInMenu, cxClasses, cxInplaceContainer, cxContainer,
  cxEdit, Vcl.DBCtrls, cxDropDownEdit, cxDBEdit, cxButtonEdit, cxMaskEdit,
  cxCalendar, cxCurrencyEdit, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxLabel, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridCustomView,
  cxGrid, dxDateTimeWheelPicker, Entity_TQP_FILE, Entity_TQQ_FILE,
  cxGridCustomPopupMenu, cxGridPopupMenu, Vcl.Menus, cxCalc, cxMemo, cxRichEdit;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    DELS: TcxStyle;
    AddS: TcxStyle;
    ModS: TcxStyle;
    DefaultS: TcxStyle;
    PrivilegeCDS: TClientDataSet;
    pnl_db: TPanel;
    dbgrd1: TDBGrid;
    pnl_kh: TPanel;
    pnl4: TPanel;
    qkhbh: TLabeledEdit;
    qkhgj: TLabeledEdit;
    qkhjc: TLabeledEdit;
    qkhqc: TLabeledEdit;
    pnl5: TPanel;
    btn1: TButton;
    btn3: TButton;
    dbgrd2: TDBGrid;
    pnl_find: TPanel;
    pnl7: TPanel;
    qhtbh: TLabeledEdit;
    qhtkq: TLabeledEdit;
    pnl8: TPanel;
    btn2: TButton;
    btn4: TButton;
    ds1: TDataSource;
    Tuq: TClientDataSet;
    ds2: TDataSource;
    MainPage: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    MasterP: TPanel;
    pnl1: TPanel;
    addbody: TButton;
    btnfind: TButton;
    btnOK: TButton;
    btnmodi: TButton;
    btnbadd: TButton;
    btnOpenTQP: TButton;
    lbl8: TLabel;
    htbh: TLabeledEdit;
    jhzg: TLabeledEdit;
    htzj: TLabeledEdit;
    htyl: TLabeledEdit;
    htzt: TLabeledEdit;
    Panel1: TPanel;
    btn_audio: TButton;
    flag_OK: TButton;
    flag_zf: TButton;
    Label1: TLabel;
    pik_create: TDateTimePicker;
    ll_name: TLabel;
    cxG: TcxGrid;
    CGB: TcxGridTableView;
    cxGLevel1: TcxGridLevel;
    CGBColumn1: TcxGridColumn;
    CGBColumn2: TcxGridColumn;
    CGBColumn3: TcxGridColumn;
    CGBColumn4: TcxGridColumn;
    CGBColumn5: TcxGridColumn;
    CGBColumn6: TcxGridColumn;
    CGBColumn7: TcxGridColumn;
    CGBColumn8: TcxGridColumn;
    CGBColumn9: TcxGridColumn;
    CGBColumn10: TcxGridColumn;
    CGBColumn11: TcxGridColumn;
    CGBColumn12: TcxGridColumn;
    CGBColumn13: TcxGridColumn;
    CGBColumn14: TcxGridColumn;
    CGBColumn15: TcxGridColumn;
    CGBColumn16: TcxGridColumn;
    Memo1: TMemo;
    khbh: TcxButtonEdit;
    CDSTMP: TClientDataSet;
    btnallBody: TButton;
    btnDel: TButton;
    Lhygs: TLabeledEdit;
    lkhjl: TLabeledEdit;
    lkhjc: TLabeledEdit;
    lkhqc: TLabeledEdit;
    cxStyle4: TcxStyle;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure btnOpenTQPClick(Sender: TObject);
    procedure btnfindClick(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure dbgrd2DblClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btn1_tqp08PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure btnmodiClick(Sender: TObject);
    procedure btnbaddClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure addbodyClick(Sender: TObject);
    procedure CGBDataControllerNewRecord(ADataController
      : TcxCustomDataController; ARecordIndex: Integer);
    procedure htbhChange(Sender: TObject);
    procedure htztChange(Sender: TObject);
    procedure htzjChange(Sender: TObject);
    procedure htylChange(Sender: TObject);
    procedure dbgrd1DblClick(Sender: TObject);
    procedure khbhPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure btnallBodyClick(Sender: TObject);
    procedure khbhPropertiesChange(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure CGBColumn5PropertiesEditValueChanged(Sender: TObject);
    procedure CGBColumn6PropertiesEditValueChanged(Sender: TObject);
    procedure CGBColumn9PropertiesEditValueChanged(Sender: TObject);
    procedure CGBColumn11PropertiesEditValueChanged(Sender: TObject);
    procedure CGBColumn12PropertiesEditValueChanged(Sender: TObject);
    procedure btn_audioClick(Sender: TObject);
    procedure CGBColumn8PropertiesEditValueChanged(Sender: TObject);
    procedure pik_createChange(Sender: TObject);
    procedure flag_OKClick(Sender: TObject);
    procedure flag_zfClick(Sender: TObject);
    procedure CGBColumn15PropertiesEditValueChanged(Sender: TObject);
  private
    procedure GetLoginUser(Key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    function RunCDS(SQL: string; DS: TClientDataSet): Boolean;
    procedure GetAuth(Key: AnsiString);
    procedure ResizeForm;
    procedure WaitMessage;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure fillKhInfo;
    procedure Inithead(H: TTQP_FILE; ID: string = '');
    procedure FillHead(H: TTQP_FILE);
    procedure DisplayStatus(T: Char); // 显示合同当前状态
    procedure FillBody(BList: TTQQ_FILEList); overload;
    procedure FillBody(B: TTQQ_FILE); overload;
    procedure InitBody(htID: string; B: TTQQ_FILE); overload;
    procedure InitBody(htID: string); overload;
    procedure addBodyItem(htID: string);
    procedure FillRecord(B: TTQQ_FILE; I: Integer = 0);
    procedure DisplayJHZG(const ID: string);
    procedure FillHeadBody(ID: string);
    procedure SwitchEditStatus(K: Boolean);
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

const
  DLL_KEY = 'HTHT_DLL';

  // 业务代码
const
  FSQL = 'SELECT occ01,occ02,oca02,occ18,gen02 FROM occ_file  ' + sLineBreak +
    'LEFT JOIN oca_file ON oca01 = occ03 ' + sLineBreak +
    'LEFT JOIN gen_file ON gen01 = occ04 WHERE ROWNUM < 100 ' +
    sLineBreak + ' %s';
  MSQL = 'SELECT tqp01,tqpud01,tqp02,tqp06,tqp07,tqp03,tqpud07,tqp21,tqp04,tqp08,'
    + sLineBreak +
    'tqpud02,tqporiu,tqpdate,tqpplant,tqplegal,tqpacti,tqporig FROM tqp_file WHERE ROWNUM < 100  %s  ORDER BY tqp01 DESC';

 BeizhuLen = 80; //备注长度
var
  FlagOP: Boolean; // 是否可操作
  FlagQuery: Boolean; // 是否可全部查询
  FlagPrint: Boolean; // 是否可打印

  HtHead: TTQP_FILE;
  HtHeadArr: TTQP_FILEList;
  HtBody: TTQQ_FILE;
  HtBodyArr: TTQQ_FILEList;
  zhanghu: string;
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  // ----------------------------------------------------------------------------
  //

procedure TWorkForm.WaitMessage;
begin
  while True do
  begin
    application.ProcessMessages; // 读取消息队列
    if not MessageFlag then
      Break;
  end;
end;

procedure TWorkForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Assigned(HtHead) then
    FreeAndNil(HtHead);
  if Assigned(HtHeadArr) then
    FreeAndNil(HtHeadArr);
  if Assigned(HtBody) then
    FreeAndNil(HtBody);
  if Assigned(HtBodyArr) then
    FreeAndNil(HtBodyArr);
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := 'ERP_ZZHT';

  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 权限的设置
  // 获取是否查询所有的合同
  GetPrivilege('ASSERT', '6', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    FlagQuery := false
  else
    FlagQuery := not(PrivilegeCDS.FieldByName('R1').AsString = '0');
  // 获取是否可操作合同
  GetPrivilege('ASSERT', '7', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    FlagOP := false
  else
    FlagOP := not(PrivilegeCDS.FieldByName('R1').AsString = '0');
  // 获取是否可打印合同
  GetPrivilege('ASSERT', '8', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    FlagPrint := false
  else
    FlagPrint := not(PrivilegeCDS.FieldByName('R1').AsString = '0');

  pnl1.Enabled := false;
  pik_create.Date := Date;
  btnOpenTQP.Visible := FlagOP;

  // 初始化实体类
  HtHead := TTQP_FILE.Create;
  HtHeadArr := TTQP_FILEList.Create;
  HtBody := TTQQ_FILE.Create;
  HtBodyArr := TTQQ_FILEList.Create;
end;

// -----------------------------------------------------------------------------
procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

function TWorkForm.RunCDS(SQL: string; DS: TClientDataSet): Boolean;
var
  s, T: string;
begin
  Result := false;
  // DS.close;
  if not dba.ReadDataset(SQL, DS) then
  begin
    application.MessageBox('读数据集失败！', 'Infomation');
    dba.GetLastError(s, T);
    LogMsg(pchar(DLL_KEY + s + ':' + T));
  end
  else
  begin
    DS.Active := True;
    Result := True;
  end;
end;

procedure TWorkForm.btn3Click(Sender: TObject);
begin
  pnl_kh.Visible := false;
  MasterP.Enabled := True;
end;

procedure TWorkForm.dbgrd1DblClick(Sender: TObject);
begin
  // 双击清单页签，来到合同查看页
  khbh.Tag := 1;
  MainPage.ActivePageIndex := 0;
  SwitchEditStatus(false);
  FillHeadBody(Cds.FieldByName('tqp01').AsString);
end;

procedure TWorkForm.dbgrd2DblClick(Sender: TObject);
begin
  // 将当前值插入到字段值中
  khbh.Tag := 0;
  if Assigned(HtHead) then
    HtHead.f_TQP08 := Tuq.FieldByName('occ01').Text;
  khbh.Text := Tuq.FieldByName('occ01').Text;
  fillKhInfo;
  pnl_kh.Visible := false;
  dbgrd2.Enabled := false;
  MasterP.Enabled := True;
end;

procedure TWorkForm.btn1_tqp08PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  if Cds.State in [dsinsert, dsEdit] then
  begin
    // open the form to select the customer
    MasterP.Enabled := false;
    pnl_kh.Visible := True;
    dbgrd2.Enabled := True;

    ds2.DataSet := nil;
    qkhbh.Text := '';
    qkhjc.Text := '';
    qkhqc.Text := '';
    qkhqc.Text := '';
  end;
end;

procedure TWorkForm.btn1Click(Sender: TObject);
var
  s, SQL: string;
begin
  // 用来查询搜索的客户信息
  s := ' AND 1 = 1 ';
  if qkhbh.Text <> '' then
    s := s + ' AND occ01 like ''%' + qkhbh.Text + '%''';
  if qkhjc.Text <> '' then
    s := s + ' AND occ02 like ''%' + qkhjc.Text + '%''';
  if qkhqc.Text <> '' then
    s := s + ' AND occ18 like ''%' + qkhqc.Text + '%''';
  if qkhgj.Text <> '' then
    s := s + ' AND geb02 like ''%' + qkhgj.Text + '%''';

  SQL := Format(FSQL, [s]);
  Memo1.Text := SQL;
  RunCDS(SQL, Tuq);

  ds2.DataSet := Tuq;
end;

procedure TWorkForm.btn2Click(Sender: TObject);
var
  s, SQL: string;
begin
  s := ' AND 1 = 1 ';
  if qhtbh.Text <> '' then
    s := ' AND  tqp02 LIKE ''%' + qhtbh.Text + '%''';
  if qhtkq.Text <> '' then
    s := ' AND  tqp08 LIKE ''%' + qhtkq.Text + '%''';
  if not FlagQuery then // 没有权限查看能所合同
    s := ' AND tqporiu = ''' + zhanghu + '''';

  Cds.DisableControls;
  SQL := Format(MSQL, [s]);
  // Memo1.Text := sql;
  RunCDS(SQL, Cds);
  Cds.EnableControls;
  pnl_find.Visible := false;
  MasterP.Enabled := True;
  ResizeForm;
  MainPage.ActivePageIndex := 1;
end;

procedure TWorkForm.btn4Click(Sender: TObject);
begin
  pnl_find.Visible := false;
  MasterP.Enabled := True;
end;

procedure TWorkForm.btnallBodyClick(Sender: TObject);
begin
  if MessageDlg('您确定要删除(注意：本操作不可恢复)当前合同的所有单身记录吗？', mtConfirmation,
    [mbYes, mbNo], 0) = mrNo then
  begin
    exit;
  end;
  // 单身信息清空
  HtBodyArr.Clear;
  CGB.DataController.RecordCount := 0;
end;

procedure TWorkForm.btnbaddClick(Sender: TObject);
var
  s, T: string;
  I: Integer;
begin
  CGB.DataController.RecordCount := 0;
  FillHeadBody('');
  // 单头信息导入数据库
  s := 'INSERT INTO TQP_FILE (tqp01,tqp02,tqp03,tqp08,tqp04,tqp06,tqp20,tqpud07,tqpud08,tqporiu,TQPORIG,TQPPLANT,TQPLEGAL) '
    + 'values(''%s'',''%s'',''%s'',''%s'',''%s'',to_date(''%s'',''yyyy/mm/dd''),''%s'',%s,%s,''%s'',''%s'',''ZZHT'',''ZZHT'')';
  s := Format(s, [HtHead.f_TQP01, HtHead.f_TQP02, HtHead.f_TQP03,
    HtHead.f_TQP08, HtHead.f_TQP04, formatdatetime('yyyy/mm/dd',
    HtHead.f_TQP06), HtHead.f_TQP20, floattostr(HtHead.f_TQPUD07),
    floattostr(HtHead.f_TQPUD08), HtHead.f_TQPORIU, HtHead.f_TQPORIG]);

  // Memo1.Text := s;
  // exit;
  if not dba.ExecuteCommand(s, I) then
  begin
    dba.GetLastError(s, T);
    LogMsg('合同单头上传出错： ' + s + '  ' + T);
  end;
end;

procedure TWorkForm.btnDelClick(Sender: TObject);
var
  s: string;
  I: Integer;
begin
  if MessageDlg('您确定要删除(注意：本操作不可恢复)当前合同记录吗?', mtConfirmation, [mbYes, mbNo], 0)
    = mrNo then
  begin
    exit;
  end;
  if not Assigned(HtHead) then
  begin
    ShowMessage('当前没有合同要删除，请确认后继续');
    exit;
  end;
  // 合同删除,并直接进行合同清单页
  s := Format('delete tqp_file where tqp01 = ''%s''', [HtHead.f_TQP01]);
  // 删除原有合同单头信息
  if not dba.ExecuteCommand(s, I) then
    exit;
  // 更新单身。更新成功后，将新增合同按钮
  s := Format('delete tqq_file where tqq01 = ''%s''', [HtHead.f_TQP01]);
  // 删除原有合同单身信息
  if not dba.ExecuteCommand(s, I) then
    exit;
  HtBodyArr.Clear;
  CGB.DataController.RecordCount := 0;
  FreeAndNil(HtHead);
end;

procedure TWorkForm.addbodyClick(Sender: TObject);
begin
  addBodyItem(HtHead.f_TQP01);
  CGB.Controller.FocusedRowIndex := CGB.DataController.RecordCount - 1;
  CGB.Controller.FocusedColumnIndex := 1;
end;

procedure TWorkForm.btnfindClick(Sender: TObject);
begin
  if Cds.ChangeCount > 0 then
  begin
    application.MessageBox('尚有记录修改未提交，请提交后继续！', 'Infomation');
    exit;
  end
  else
  begin
    pnl_find.Visible := True;
    qhtbh.Text := '';
    qhtkq.Text := '';
    MasterP.Enabled := false;
    ResizeForm;
  end;
end;

procedure TWorkForm.btnmodiClick(Sender: TObject);
var
  I, J: Integer;
begin
  if MessageDlg('您确定要删除(注意：本操作不可恢复)当前选择的单身记录吗?', mtConfirmation, [mbYes, mbNo],
    0) = mrNo then
  begin
    exit;
  end;
  // 删除选中单身记录
  I := CGB.Controller.SelectedRows[0].RecordIndex;
  // 删除列表信息
  for J := 0 to HtBodyArr.Count - 1 do
  begin
    if TTQQ_FILE(HtBodyArr.Items[J]).f_TQQ02 = I then
    begin
      HtBodyArr.Delete(J);
      Break;
    end;
  end;
  // 删除GRID信息
  CGB.DataController.DeleteRecord(I);
end;

procedure TWorkForm.btnOKClick(Sender: TObject);
var
  s, T: string;
  I: Integer;
begin
  if not Assigned(HtHead) then
  begin
    ShowMessage('没有有效的合同要保存，请确认后继续');
    exit;
  end;
  // 单头信息导入数据库
  s := Format('delete tqp_file where tqp01 = ''%s''', [HtHead.f_TQP01]);
  // 删除原有合同单头信息
  if not dba.ExecuteCommand(s, I) then
    exit;
  s := 'INSERT INTO TQP_FILE (tqp01,tqp02,tqp03,tqp08,tqp04,tqp06,tqp20,tqpud07,tqpud08,tqporiu,TQPORIG,TQPACTI,TQPPLANT,TQPLEGAL) '
    + 'values(''%s'',''%s'',''%s'',''%s'',''%s'',to_date(''%s'',''yyyy/mm/dd''),''%s'',%s,%s,''%s'',''%s'',''Y'',''ZZHT'',''ZZHT'')';
  s := Format(s, [HtHead.f_TQP01, HtHead.f_TQP02, HtHead.f_TQP03,
    HtHead.f_TQP08, HtHead.f_TQP04, formatdatetime('yyyy/mm/dd',
    HtHead.f_TQP06), HtHead.f_TQP20, floattostr(HtHead.f_TQPUD07),
    floattostr(HtHead.f_TQPUD08), HtHead.f_TQPORIU, HtHead.f_TQPORIG]);

  // Memo1.Text := s;
  // exit;
  if not dba.ExecuteCommand(s, I) then
  begin
    dba.GetLastError(s, T);
    LogMsg('合同单头上传出错： ' + s + '  ' + T);
  end;

  // 更新单身。更新成功后，将新增合同按钮
  s := Format('delete tqq_file where tqq01 = ''%s''', [HtHead.f_TQP01]);
  // 删除原有合同单身信息
  if not dba.ExecuteCommand(s, I) then
    exit;

  s := '';
  for I := 0 to CGB.DataController.RecordCount - 1 do
  begin
    s := s + sLineBreak +
      'into tqq_file(TQQ01,TQQ02,TQQUD02,TQQ03,TQQUD03,TQQ04,TQQUD07,TQQ05,TQQUD08,TQQ06,TQQUD09,TQQUD01,TQQPLANT,TQQLEGAL,TQQUD13) values'
      + Format('(''%s'',%s,''%s'',''%s'',''%s'',''%s'',%s,''%s'',%s,''%s'',%s,''%s'',''%s'',''%s'',to_date(''%s'',''yyyy/mm/dd''))',
      [CGB.DataController.Values[I, 0], IntToStr(CGB.DataController.Values[I, 1]
      ), CGB.DataController.Values[I, 2], CGB.DataController.Values[I, 3],
      CGB.DataController.Values[I, 4], CGB.DataController.Values[I, 5],
      floattostr(CGB.DataController.Values[I, 6]), CGB.DataController.Values[I,
      8], floattostr(CGB.DataController.Values[I, 9]),
      CGB.DataController.Values[I, 10], floattostr(CGB.DataController.Values[I,
      11]), CGB.DataController.Values[I, 14], 'ZZHT', 'ZZHT',
      formatdatetime('yyyy/mm/dd', CGB.DataController.Values[I, 7])]);
  end;

  s := Format('insert all %s select 1 from dual', [s]);
  // Memo1.Text := s;
  if dba.ExecuteCommand(s, I) then
    ShowMessage('合同信息已上传成功')
  else
  begin
    dba.GetLastError(s, T);
    LogMsg('update error info ' + s + '  ' + T);
  end;
end;

procedure TWorkForm.btnOpenTQPClick(Sender: TObject);
begin
  if not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 5 > 0) then
      exit;
  end;
  if btnOpenTQP.Tag = 1 then
    btnOpenTQP.Tag := 0
  else
    btnOpenTQP.Tag := 1;
  SwitchEditStatus(btnOpenTQP.Tag = 1);
end;

procedure TWorkForm.btn_audioClick(Sender: TObject);
begin
  // 审核过程
  if not Assigned(HtHead) then
    exit;
  HtHead.f_TQP04 := 'Y';
  DisplayStatus('Y');
end;

procedure TWorkForm.flag_OKClick(Sender: TObject);
begin
  // 结案过程
  if not Assigned(HtHead) then
    exit;
  HtHead.f_TQP04 := 'C';
  DisplayStatus('C');
end;

procedure TWorkForm.flag_zfClick(Sender: TObject);
begin
  // 作废状态
  if not Assigned(HtHead) then
    exit;
  HtHead.f_TQP04 := 'X';
  DisplayStatus('X');
end;

procedure TWorkForm.CGBColumn11PropertiesEditValueChanged(Sender: TObject);
var
  I: Integer;
  Edit: TcxCustomEdit;
  Value: Variant;
begin
  Edit := Sender as TcxCustomEdit;
  Value := Edit.EditValue;
  // 运费变化引起变化
  I := CGB.Controller.SelectedRows[0].RecordIndex;
  CGB.DataController.Values[I, 12] := Value + CGB.DataController.Values[I, 11];
  // 出厂成本价
  CGB.DataController.Values[I, 13] := CGB.DataController.Values[I, 8] -
    CGB.DataController.Values[I, 12]; // 盈利值
end;

procedure TWorkForm.CGBColumn12PropertiesEditValueChanged(Sender: TObject);
var
  I: Integer;
  Edit: TcxCustomEdit;
  Value: Variant;
begin
  Edit := Sender as TcxCustomEdit;
  Value := Edit.EditValue;
  // 成本价变化引起变化
  I := CGB.Controller.SelectedRows[0].RecordIndex;
  CGB.DataController.Values[I, 12] := CGB.DataController.Values[I, 10] + Value;
  // 出厂成本价
  CGB.DataController.Values[I, 13] := CGB.DataController.Values[I, 8] -
    CGB.DataController.Values[I, 12]; // 盈利值
end;

procedure TWorkForm.CGBColumn15PropertiesEditValueChanged(Sender: TObject);
var
  I: Integer;
  Edit: TcxCustomEdit;
  Value: Variant;
begin
  Edit := Sender as TcxCustomEdit;
  Value := Edit.EditValue;
  // 不能超过80个汉字
  if Length(Value) > BeizhuLen then
  begin
    ShowMessage('备注信息超长，将自动截取！');
    I := CGB.DataController.GetSelectedRowIndex(0);
    CGB.DataController.Values[I, 14] := copy(Value,1,BeizhuLen);
  end;
end;

procedure TWorkForm.CGBColumn5PropertiesEditValueChanged(Sender: TObject);
var
  I: Integer;
  htzjf: Double;
  Edit: TcxCustomEdit;
  Value: Variant;
begin
  Edit := Sender as TcxCustomEdit;
  Value := Edit.EditValue;
  // 数量变化引起变化
  I := CGB.DataController.GetSelectedRowIndex(0);
  htzjf := CGB.DataController.Values[I, 9]; // 原合同总价
  CGB.DataController.Values[I, 6] := Value * CGB.DataController.Values[I, 5];
  // 总重
  CGB.DataController.Values[I, 9] := Value * CGB.DataController.Values[I, 8];
  // 合同总价
  htzjf := HtHead.f_TQPUD07 - htzjf + CGB.DataController.Values[I, 9];
  htzj.Text := FormatCurr('0.00', htzjf);
end;

procedure TWorkForm.CGBColumn6PropertiesEditValueChanged(Sender: TObject);
var
  I: Integer;
  Edit: TcxCustomEdit;
  Value: Variant;
begin
  Edit := Sender as TcxCustomEdit;
  Value := Edit.EditValue;
  // 单重变化引起变化
  I := CGB.DataController.GetSelectedRowIndex(0);
  CGB.DataController.Values[I, 6] := CGB.DataController.Values[I, 4] * Value;
  // 总重
end;

procedure TWorkForm.CGBColumn8PropertiesEditValueChanged(Sender: TObject);
var
  I: Integer;
  Edit: TcxCustomEdit;
  Value: Variant;
begin
  Edit := Sender as TcxCustomEdit;
  Value := Edit.EditValue;
  // 判断约定交货日是否不小于签合同时间
  if Value < HtHead.f_TQP06 then
  begin
    ShowMessage('约定交货日不可小于合同签订日！');
    I := CGB.DataController.GetSelectedRowIndex(0);
    CGB.DataController.Values[I, 7] := HtHead.f_TQP06;
  end;
end;

procedure TWorkForm.CGBColumn9PropertiesEditValueChanged(Sender: TObject);
var
  I: Integer;
  htzjf: Double;
  Edit: TcxCustomEdit;
  Value: Variant;
begin
  Edit := Sender as TcxCustomEdit;
  Value := Edit.EditValue;
  // 合同单价变化引起变化
  I := CGB.DataController.GetSelectedRowIndex(0);
  htzjf := CGB.DataController.Values[I, 9]; // 原合同总价

  CGB.DataController.Values[I, 9] := CGB.DataController.Values[I, 4] * Value;
  // 合同总价
  CGB.DataController.Values[I, 13] := Value - CGB.DataController.Values[I, 12];
  // 盈利值

  htzjf := HtHead.f_TQPUD07 - htzjf + CGB.DataController.Values[I, 9];
  htzj.Text := FormatCurr('0.00', htzjf);
end;

procedure TWorkForm.CGBDataControllerNewRecord(ADataController
  : TcxCustomDataController; ARecordIndex: Integer);
// 在新增记录前，对新记录进行初始化
// htyl.Text := IntToStr(aRecordIndex);
// CGB.DataController.Values[ARecordIndex,1] := ARecordIndex + 1;
var
  B: TTQQ_FILE;
begin
  if HtHead.f_TQP01 = '' then
  begin
    application.MessageBox('没有有效的合同号，请点击<新增合同>后继续', 'Information');
    exit;
  end;
  B := TTQQ_FILE.Create;
  InitBody(HtHead.f_TQP01, B);
  HtBodyArr.add(B);
  CGB.DataController.Values[ARecordIndex, 0] := B.f_TQQ01;
  CGB.DataController.Values[ARecordIndex, 1] := B.f_TQQ02;
  CGB.DataController.Values[ARecordIndex, 2] := B.f_TQQUD02;
  CGB.DataController.Values[ARecordIndex, 3] := B.f_TQQ03;
  CGB.DataController.Values[ARecordIndex, 4] := StrToFloatDef(B.f_TQQUD03, 0);
  CGB.DataController.Values[ARecordIndex, 5] := StrToFloatDef(B.f_TQQ04, 0);
  CGB.DataController.Values[ARecordIndex, 6] := B.f_TQQUD07;
  CGB.DataController.Values[ARecordIndex, 7] := B.f_TQQUD13;
  CGB.DataController.Values[ARecordIndex, 8] := StrToFloatDef(B.f_TQQ05, 0);
  CGB.DataController.Values[ARecordIndex, 9] := B.f_TQQUD08;
  CGB.DataController.Values[ARecordIndex, 10] := StrToFloatDef(B.f_TQQ06, 0);
  CGB.DataController.Values[ARecordIndex, 11] := B.f_TQQUD09;
  CGB.DataController.Values[ARecordIndex, 12] := StrToFloatDef(B.f_TQQ06, 0) +
    B.f_TQQUD09;
  CGB.DataController.Values[ARecordIndex, 13] := StrToFloatDef(B.f_TQQ05, 0) -
    StrToFloatDef(B.f_TQQ06, 0) - B.f_TQQUD09;
  CGB.DataController.Values[ARecordIndex, 14] := B.f_TQQUD01;
  CGB.DataController.Values[ARecordIndex, 15] := '';
  CGB.Controller.FocusedColumnIndex := 1;
  B.Free;
end;

// ------------------------------------------------------------------------------------
procedure TWorkForm.Inithead(H: TTQP_FILE; ID: string = '');
var
  NID: string;
  s: string;
begin
  // 若ID为空时，表示新建一个合同号，不为空，填充单头类
  if ID = '' then
  begin
    if not dba.GenerateId('TQP_FILE', 'tqp01', '', 'YYMMDDXXXX', '', NID) then
    begin
      application.MessageBox('生成合同失败，请重试', 'Information');
      exit;
    end;
    H.f_TQP01 := NID;
    H.f_TQP02 := '';
    H.f_TQP03 := '000';
    H.f_TQP04 := 'O';
    H.f_TQP06 := Date;
    H.f_TQP08 := '';
    H.f_TQPORIU := zhanghu;
    H.f_TQP20 := '';
    H.f_TQPUD07 := 0;
    H.f_TQPUD08 := 0;
    H.f_TQPPLANT := 'ZZHT';
    H.f_TQPLEGAL := 'ZZHT';
    H.f_TQPACTI := 'Y';
    H.f_TQPORIG := '015201';
  end
  else
  begin
    s := 'SELECT * FROM TQP_FILE WHERE TQP01 = ''' + ID + '''';
    RunCDS(s, CDSTMP);
    HtHead := TTQP_FILE.ReadFromCds(CDSTMP);
  end;

end;

procedure TWorkForm.khbhPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  MasterP.Enabled := false;
  pnl_kh.Visible := True;
  dbgrd2.Enabled := True;

  ds2.DataSet := nil;
  qkhbh.Text := '';
  qkhjc.Text := '';
  qkhqc.Text := '';
  qkhqc.Text := '';
end;

procedure TWorkForm.khbhPropertiesChange(Sender: TObject);
var
  SQL: string;
begin
  { TODO : 客户编号变化时，填充客户信息 }
  // 只有当从清单中获取信息时，才填充
  if khbh.Tag = 0 then
    exit;
  SQL := Format(FSQL, [' AND OCC01 = ''' + khbh.Text + '''']);
  RunCDS(SQL, Tuq);
  // memo1.Text := sql + '///'+;
  fillKhInfo;
end;

procedure TWorkForm.fillKhInfo;
begin
  lkhjc.Text := Tuq.FieldByName('occ02').Text;
  lkhqc.Text := Tuq.FieldByName('occ18').Text;
  Lhygs.Text := Tuq.FieldByName('oca02').Text;
  lkhjl.Text := Tuq.FieldByName('gen02').Text;
end;

procedure TWorkForm.FillHead(H: TTQP_FILE);
begin
  htbh.Text := H.f_TQP02;
  khbh.Text := H.f_TQP08;
  pik_create.Date := H.f_TQP06;
  jhzg.Text := H.f_TQPORIU;
  DisplayStatus(H.f_TQP04[1]);
  DisplayJHZG(H.f_TQPORIU);
  htzj.Text := FormatCurr('0.00', H.f_TQPUD07);
  htyl.Text := FormatCurr('0.00', H.f_TQPUD08);
end;

procedure TWorkForm.addBodyItem(htID: string);
var
  B: TTQQ_FILE;
begin
  if htID = '' then
  begin
    application.MessageBox('没有有效的合同号，请点击<新增合同>后继续', 'Information');
    exit;
  end;
  B := TTQQ_FILE.Create;
  InitBody(htID, B);
  HtBodyArr.add(B);
  FillBody(B);
  B.Free;
end;

procedure TWorkForm.InitBody(htID: string);
var
  s: string;
begin
  // 根据合同号，初始化整个单身
  s := 'SELECT * FROM TQQ_FILE WHERE TQQ01 = ''' + htID + '''';
  // LogMsg('查询单身SQL：' + s);
  RunCDS(s, CDSTMP);
  HtBodyArr := TTQQ_FILEList.ReadFromCds(CDSTMP);
  // LogMsg('查询函数获得条数：' + HtBodyArr.Count.ToString);
end;

procedure TWorkForm.InitBody(htID: string; B: TTQQ_FILE);
var
  ID: Integer;
begin
  // 新增单身的默认认值
  if htID = '' then
  begin
    application.MessageBox('没有有效的合同号，请点击<新增合同>后继续', 'Information');
    exit;
  end;
  ID := HtBodyArr.Count + 1;
  B.f_TQQ01 := htID;
  B.f_TQQ02 := ID;
  B.f_TQQUD02 := '';
  B.f_TQQ03 := '';
  B.f_TQQUD03 := '0';
  B.f_TQQ04 := '0';
  B.f_TQQUD07 := 0;
  B.f_TQQUD13 := Date;
  B.f_TQQ05 := '0';
  B.f_TQQUD08 := 0;
  B.f_TQQ06 := '0';
  B.f_TQQUD01 := '';
  B.f_TQQPLANT := 'ZZHT';
  B.f_TQQLEGAL := 'ZZHT';
end;

procedure TWorkForm.FillRecord(B: TTQQ_FILE; I: Integer = 0);
var
  ID, J: Integer;
begin
  // I为0时，表示新增记录。当I > 0时，若在当前GRID中没有找到项次的就新增，找到就替换
  ID := 0;
  for J := 0 to CGB.DataController.RecordCount - 1 do
  begin
    if I = CGB.DataController.Values[J, 1] then
    begin
      ID := J;
      Break;
    end;
  end;
  if ID = 0 then
    ID := CGB.DataController.AppendRecord;

  CGB.DataController.Values[ID, 0] := B.f_TQQ01;
  CGB.DataController.Values[ID, 1] := B.f_TQQ02;
  CGB.DataController.Values[ID, 2] := B.f_TQQUD02;
  CGB.DataController.Values[ID, 3] := B.f_TQQ03;
  CGB.DataController.Values[ID, 4] := StrToFloatDef(B.f_TQQUD03, 0);
  CGB.DataController.Values[ID, 5] := StrToFloatDef(B.f_TQQ04, 0);
  CGB.DataController.Values[ID, 6] := B.f_TQQUD07;
  CGB.DataController.Values[ID, 7] := B.f_TQQUD13;
  CGB.DataController.Values[ID, 8] := StrToFloatDef(B.f_TQQ05, 0);
  CGB.DataController.Values[ID, 9] := B.f_TQQUD08;
  CGB.DataController.Values[ID, 10] := StrToFloatDef(B.f_TQQ06, 0);
  CGB.DataController.Values[ID, 11] := B.f_TQQUD09;
  CGB.DataController.Values[ID, 12] := StrToFloatDef(B.f_TQQ06, 0) +
    B.f_TQQUD09;
  CGB.DataController.Values[ID, 13] := StrToFloatDef(B.f_TQQ05, 0) -
    StrToFloatDef(B.f_TQQ06, 0) - B.f_TQQUD09;
  CGB.DataController.Values[ID, 14] := B.f_TQQUD01;
  CGB.DataController.Values[ID, 15] := '';
end;

procedure TWorkForm.FillBody(B: TTQQ_FILE);
begin
  FillRecord(B);
end;

procedure TWorkForm.FillBody(BList: TTQQ_FILEList);
var
  I: Integer;
begin
  // 将单身显示在grid中
  if BList.Count = 0 then
    exit;
  CGB.BeginUpdate();
  CGB.DataController.RecordCount := 0;
  for I := 0 to BList.Count - 1 do
  begin
    // LogMsg(TTQQ_FILE(BList.Items[I]).f_TQQ01 + '-' + TTQQ_FILE(BList.Items[I])
    // .f_TQQ02.ToString);
    FillRecord(TTQQ_FILE(BList.Items[I]), TTQQ_FILE(BList.Items[I]).f_TQQ02);
  end;
  CGB.EndUpdate;
end;

procedure TWorkForm.FillHeadBody(ID: string);
begin
  // 根据合同ID填充单头与单身

  // 单头初始化
  if Assigned(HtHead) then
    FreeAndNil(HtHead);
  if not Assigned(HtHead) then
    HtHead := TTQP_FILE.Create;
  Inithead(HtHead, ID);
  FillHead(HtHead);
  // 单身初始化
  if Assigned(HtBodyArr) then
    HtBodyArr.Clear;
  if not Assigned(HtBodyArr) then
    HtBodyArr := TTQQ_FILEList.Create;

  InitBody(ID);
  // LogMsg('查询单身条数为：' + HtBodyArr.Count.ToString);
  FillBody(HtBodyArr);
end;

procedure TWorkForm.SwitchEditStatus(K: Boolean);
begin
  pnl1.Enabled := K;
  pnl_db.Enabled := K;
  cxG.Enabled := K;
  if K then
    btnOpenTQP.Caption := '切换为只读模式'
  else

    btnOpenTQP.Caption := '切换为编辑模式';

end;

procedure TWorkForm.DisplayStatus(T: Char);
var
  s: string;
begin
  case T of
    'O':
      s := '开立';
    'Y':
      s := '审核';
    'C':
      s := '结案';
    'X':
      s := '作废'
  else
    s := '开立';
  end;
  htzt.Text := s;
end;

procedure TWorkForm.DisplayJHZG(const ID: string);
var
  s, T, UName: string;
begin
  if not dba.ReadSimpleResult('SELECT gen02 FROM gen_file WHERE gen01 = ''' + ID
    + '''', UName) then
  begin
    UName := '';
    dba.GetLastError(s, T);
    LogMsg('获取用户姓名出错，代码' + s + ':' + T);
  end;
  // LogMsg('name ID = ' + ID);
  ll_name.Caption := UName;
end;
// ---------------------处理响应事件---------------------------------------------

procedure TWorkForm.htbhChange(Sender: TObject);
begin
  // 合同编号
  if not Assigned(HtHead) then
    exit;
  HtHead.f_TQP02 := Trim(htbh.Text);
end;

procedure TWorkForm.pik_createChange(Sender: TObject);
begin
  // 合同签订日
  if not Assigned(HtHead) then
    exit;
  HtHead.f_TQP06 := pik_create.Date;
end;

procedure TWorkForm.htylChange(Sender: TObject);
begin
  // 合同盈利额
  if not Assigned(HtHead) then
    exit;
  HtHead.f_TQPUD08 := StrToFloatDef(Trim(htyl.Text), 0);
end;

procedure TWorkForm.htzjChange(Sender: TObject);
begin
  // 合同总价
  if not Assigned(HtHead) then
    exit;
  HtHead.f_TQPUD07 := StrToFloatDef(Trim(htzj.Text), 0);
end;

procedure TWorkForm.htztChange(Sender: TObject);
begin
  // 合同状态
  if not Assigned(HtHead) then
    exit;
  if htzt.Text = '开立' then
    HtHead.f_TQP04 := 'O';
  if htzt.Text = '审核' then
    HtHead.f_TQP04 := 'Y';
  if htzt.Text = '结案' then
    HtHead.f_TQP04 := 'C';
  if htzt.Text = '作废' then
    HtHead.f_TQP04 := 'X';
end;

// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  J: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  J := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), J);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(Key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  J: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  J := str2mem(Key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), J);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  J: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  J := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), J);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(Key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  J: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  J := str2mem(Key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), J);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息
  if (StrComp(pchar(MsgName), pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(pchar(MsgName), pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(pchar(MsgName), pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.

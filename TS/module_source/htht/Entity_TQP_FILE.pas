//
// ------------------------------------------------------------------------------------
//
// Entity_TQP_FILE.pas -- 数据表"TQP_FILE"对应的实体类单元（UniDAC引擎）for VCL
//
//                          本单元由QuickBurro V5.0.4.0 实体类生成器（GetEntity）所生成。
//
//                          生成时间: 2019-03-18 14:13:55
//
//                          版权所有(C) 樵夫软件工作室（Jopher Software Studio）, 2006-2017 
//
//
//// ------------------------------------------------------------------------------------
//

unit Entity_TQP_FILE;

interface

uses
   {$IF CompilerVersion>=23.0}
   System.SysUtils, 
   System.Classes, 
   Data.db, 
   Datasnap.DBClient,
   {$ELSE}
   SysUtils, 
   Classes, 
   db, 
   DBClient,
   {$IFEND}
   {$IFDEF UNICODE}
   {$IF CompilerVersion>=23.0}System.AnsiStrings{$ELSE}AnsiStrings{$IFEND},
   {$ENDIF}
   QBParcel,
   RemoteUniDac;

type
  TTQP_FILE = class(TObject)

  Private
    ff_TQP01: String;
    ff_TQP02: String;
    ff_TQP03: String;
    ff_TQP04: String;
    ff_TQP05: String;
    ff_TQP06: TDateTime;
    ff_TQP07: TDateTime;
    ff_TQP08: String;
    ff_TQP09: String;
    ff_TQP10: String;
    ff_TQP11: Double;
    ff_TQP12: String;
    ff_TQP13: String;
    ff_TQP14: TDateTime;
    ff_TQP15: Integer;
    ff_TQP16: Integer;
    ff_TQP17: String;
    ff_TQP18: Double;
    ff_TQP19: String;
    ff_TQP20: String;
    ff_TQP21: String;
    ff_TQP22: String;
    ff_TQP23: Double;
    ff_TQP24: Integer;
    ff_TQP25: String;
    ff_TQPACTI: String;
    ff_TQPUSER: String;
    ff_TQPGRUP: String;
    ff_TQPMODU: String;
    ff_TQPDATE: TDateTime;
    ff_TQPUD01: String;
    ff_TQPUD02: String;
    ff_TQPUD03: String;
    ff_TQPUD04: String;
    ff_TQPUD05: String;
    ff_TQPUD06: String;
    ff_TQPUD07: Double;
    ff_TQPUD08: Double;
    ff_TQPUD09: Double;
    ff_TQPUD10: Int64;
    ff_TQPUD11: Int64;
    ff_TQPUD12: Int64;
    ff_TQPUD13: TDateTime;
    ff_TQPUD14: TDateTime;
    ff_TQPUD15: TDateTime;
    ff_TQPPLANT: String;
    ff_TQPLEGAL: String;
    ff_TQPORIU: String;
    ff_TQPORIG: String;

    flag_TQP01: boolean;
    flag_TQP02: boolean;
    flag_TQP03: boolean;
    flag_TQP04: boolean;
    flag_TQP05: boolean;
    flag_TQP06: boolean;
    flag_TQP07: boolean;
    flag_TQP08: boolean;
    flag_TQP09: boolean;
    flag_TQP10: boolean;
    flag_TQP11: boolean;
    flag_TQP12: boolean;
    flag_TQP13: boolean;
    flag_TQP14: boolean;
    flag_TQP15: boolean;
    flag_TQP16: boolean;
    flag_TQP17: boolean;
    flag_TQP18: boolean;
    flag_TQP19: boolean;
    flag_TQP20: boolean;
    flag_TQP21: boolean;
    flag_TQP22: boolean;
    flag_TQP23: boolean;
    flag_TQP24: boolean;
    flag_TQP25: boolean;
    flag_TQPACTI: boolean;
    flag_TQPUSER: boolean;
    flag_TQPGRUP: boolean;
    flag_TQPMODU: boolean;
    flag_TQPDATE: boolean;
    flag_TQPUD01: boolean;
    flag_TQPUD02: boolean;
    flag_TQPUD03: boolean;
    flag_TQPUD04: boolean;
    flag_TQPUD05: boolean;
    flag_TQPUD06: boolean;
    flag_TQPUD07: boolean;
    flag_TQPUD08: boolean;
    flag_TQPUD09: boolean;
    flag_TQPUD10: boolean;
    flag_TQPUD11: boolean;
    flag_TQPUD12: boolean;
    flag_TQPUD13: boolean;
    flag_TQPUD14: boolean;
    flag_TQPUD15: boolean;
    flag_TQPPLANT: boolean;
    flag_TQPLEGAL: boolean;
    flag_TQPORIU: boolean;
    flag_TQPORIG: boolean;

    procedure Set_TQP01(_TQP01: String);
    procedure Set_TQP02(_TQP02: String);
    procedure Set_TQP03(_TQP03: String);
    procedure Set_TQP04(_TQP04: String);
    procedure Set_TQP05(_TQP05: String);
    procedure Set_TQP06(_TQP06: TDateTime);
    procedure Set_TQP07(_TQP07: TDateTime);
    procedure Set_TQP08(_TQP08: String);
    procedure Set_TQP09(_TQP09: String);
    procedure Set_TQP10(_TQP10: String);
    procedure Set_TQP11(_TQP11: Double);
    procedure Set_TQP12(_TQP12: String);
    procedure Set_TQP13(_TQP13: String);
    procedure Set_TQP14(_TQP14: TDateTime);
    procedure Set_TQP15(_TQP15: Integer);
    procedure Set_TQP16(_TQP16: Integer);
    procedure Set_TQP17(_TQP17: String);
    procedure Set_TQP18(_TQP18: Double);
    procedure Set_TQP19(_TQP19: String);
    procedure Set_TQP20(_TQP20: String);
    procedure Set_TQP21(_TQP21: String);
    procedure Set_TQP22(_TQP22: String);
    procedure Set_TQP23(_TQP23: Double);
    procedure Set_TQP24(_TQP24: Integer);
    procedure Set_TQP25(_TQP25: String);
    procedure Set_TQPACTI(_TQPACTI: String);
    procedure Set_TQPUSER(_TQPUSER: String);
    procedure Set_TQPGRUP(_TQPGRUP: String);
    procedure Set_TQPMODU(_TQPMODU: String);
    procedure Set_TQPDATE(_TQPDATE: TDateTime);
    procedure Set_TQPUD01(_TQPUD01: String);
    procedure Set_TQPUD02(_TQPUD02: String);
    procedure Set_TQPUD03(_TQPUD03: String);
    procedure Set_TQPUD04(_TQPUD04: String);
    procedure Set_TQPUD05(_TQPUD05: String);
    procedure Set_TQPUD06(_TQPUD06: String);
    procedure Set_TQPUD07(_TQPUD07: Double);
    procedure Set_TQPUD08(_TQPUD08: Double);
    procedure Set_TQPUD09(_TQPUD09: Double);
    procedure Set_TQPUD10(_TQPUD10: Int64);
    procedure Set_TQPUD11(_TQPUD11: Int64);
    procedure Set_TQPUD12(_TQPUD12: Int64);
    procedure Set_TQPUD13(_TQPUD13: TDateTime);
    procedure Set_TQPUD14(_TQPUD14: TDateTime);
    procedure Set_TQPUD15(_TQPUD15: TDateTime);
    procedure Set_TQPPLANT(_TQPPLANT: String);
    procedure Set_TQPLEGAL(_TQPLEGAL: String);
    procedure Set_TQPORIU(_TQPORIU: String);
    procedure Set_TQPORIG(_TQPORIG: String);

  public
    Property f_TQP01: String read ff_TQP01 write Set_TQP01;
    Property TQP01_Updated: boolean read flag_TQP01 write flag_TQP01;
    Property f_TQP02: String read ff_TQP02 write Set_TQP02;
    Property TQP02_Updated: boolean read flag_TQP02 write flag_TQP02;
    Property f_TQP03: String read ff_TQP03 write Set_TQP03;
    Property TQP03_Updated: boolean read flag_TQP03 write flag_TQP03;
    Property f_TQP04: String read ff_TQP04 write Set_TQP04;
    Property TQP04_Updated: boolean read flag_TQP04 write flag_TQP04;
    Property f_TQP05: String read ff_TQP05 write Set_TQP05;
    Property TQP05_Updated: boolean read flag_TQP05 write flag_TQP05;
    Property f_TQP06: TDateTime read ff_TQP06 write Set_TQP06;
    Property TQP06_Updated: boolean read flag_TQP06 write flag_TQP06;
    Property f_TQP07: TDateTime read ff_TQP07 write Set_TQP07;
    Property TQP07_Updated: boolean read flag_TQP07 write flag_TQP07;
    Property f_TQP08: String read ff_TQP08 write Set_TQP08;
    Property TQP08_Updated: boolean read flag_TQP08 write flag_TQP08;
    Property f_TQP09: String read ff_TQP09 write Set_TQP09;
    Property TQP09_Updated: boolean read flag_TQP09 write flag_TQP09;
    Property f_TQP10: String read ff_TQP10 write Set_TQP10;
    Property TQP10_Updated: boolean read flag_TQP10 write flag_TQP10;
    Property f_TQP11: Double read ff_TQP11 write Set_TQP11;
    Property TQP11_Updated: boolean read flag_TQP11 write flag_TQP11;
    Property f_TQP12: String read ff_TQP12 write Set_TQP12;
    Property TQP12_Updated: boolean read flag_TQP12 write flag_TQP12;
    Property f_TQP13: String read ff_TQP13 write Set_TQP13;
    Property TQP13_Updated: boolean read flag_TQP13 write flag_TQP13;
    Property f_TQP14: TDateTime read ff_TQP14 write Set_TQP14;
    Property TQP14_Updated: boolean read flag_TQP14 write flag_TQP14;
    Property f_TQP15: Integer read ff_TQP15 write Set_TQP15;
    Property TQP15_Updated: boolean read flag_TQP15 write flag_TQP15;
    Property f_TQP16: Integer read ff_TQP16 write Set_TQP16;
    Property TQP16_Updated: boolean read flag_TQP16 write flag_TQP16;
    Property f_TQP17: String read ff_TQP17 write Set_TQP17;
    Property TQP17_Updated: boolean read flag_TQP17 write flag_TQP17;
    Property f_TQP18: Double read ff_TQP18 write Set_TQP18;
    Property TQP18_Updated: boolean read flag_TQP18 write flag_TQP18;
    Property f_TQP19: String read ff_TQP19 write Set_TQP19;
    Property TQP19_Updated: boolean read flag_TQP19 write flag_TQP19;
    Property f_TQP20: String read ff_TQP20 write Set_TQP20;
    Property TQP20_Updated: boolean read flag_TQP20 write flag_TQP20;
    Property f_TQP21: String read ff_TQP21 write Set_TQP21;
    Property TQP21_Updated: boolean read flag_TQP21 write flag_TQP21;
    Property f_TQP22: String read ff_TQP22 write Set_TQP22;
    Property TQP22_Updated: boolean read flag_TQP22 write flag_TQP22;
    Property f_TQP23: Double read ff_TQP23 write Set_TQP23;
    Property TQP23_Updated: boolean read flag_TQP23 write flag_TQP23;
    Property f_TQP24: Integer read ff_TQP24 write Set_TQP24;
    Property TQP24_Updated: boolean read flag_TQP24 write flag_TQP24;
    Property f_TQP25: String read ff_TQP25 write Set_TQP25;
    Property TQP25_Updated: boolean read flag_TQP25 write flag_TQP25;
    Property f_TQPACTI: String read ff_TQPACTI write Set_TQPACTI;
    Property TQPACTI_Updated: boolean read flag_TQPACTI write flag_TQPACTI;
    Property f_TQPUSER: String read ff_TQPUSER write Set_TQPUSER;
    Property TQPUSER_Updated: boolean read flag_TQPUSER write flag_TQPUSER;
    Property f_TQPGRUP: String read ff_TQPGRUP write Set_TQPGRUP;
    Property TQPGRUP_Updated: boolean read flag_TQPGRUP write flag_TQPGRUP;
    Property f_TQPMODU: String read ff_TQPMODU write Set_TQPMODU;
    Property TQPMODU_Updated: boolean read flag_TQPMODU write flag_TQPMODU;
    Property f_TQPDATE: TDateTime read ff_TQPDATE write Set_TQPDATE;
    Property TQPDATE_Updated: boolean read flag_TQPDATE write flag_TQPDATE;
    Property f_TQPUD01: String read ff_TQPUD01 write Set_TQPUD01;
    Property TQPUD01_Updated: boolean read flag_TQPUD01 write flag_TQPUD01;
    Property f_TQPUD02: String read ff_TQPUD02 write Set_TQPUD02;
    Property TQPUD02_Updated: boolean read flag_TQPUD02 write flag_TQPUD02;
    Property f_TQPUD03: String read ff_TQPUD03 write Set_TQPUD03;
    Property TQPUD03_Updated: boolean read flag_TQPUD03 write flag_TQPUD03;
    Property f_TQPUD04: String read ff_TQPUD04 write Set_TQPUD04;
    Property TQPUD04_Updated: boolean read flag_TQPUD04 write flag_TQPUD04;
    Property f_TQPUD05: String read ff_TQPUD05 write Set_TQPUD05;
    Property TQPUD05_Updated: boolean read flag_TQPUD05 write flag_TQPUD05;
    Property f_TQPUD06: String read ff_TQPUD06 write Set_TQPUD06;
    Property TQPUD06_Updated: boolean read flag_TQPUD06 write flag_TQPUD06;
    Property f_TQPUD07: Double read ff_TQPUD07 write Set_TQPUD07;
    Property TQPUD07_Updated: boolean read flag_TQPUD07 write flag_TQPUD07;
    Property f_TQPUD08: Double read ff_TQPUD08 write Set_TQPUD08;
    Property TQPUD08_Updated: boolean read flag_TQPUD08 write flag_TQPUD08;
    Property f_TQPUD09: Double read ff_TQPUD09 write Set_TQPUD09;
    Property TQPUD09_Updated: boolean read flag_TQPUD09 write flag_TQPUD09;
    Property f_TQPUD10: Int64 read ff_TQPUD10 write Set_TQPUD10;
    Property TQPUD10_Updated: boolean read flag_TQPUD10 write flag_TQPUD10;
    Property f_TQPUD11: Int64 read ff_TQPUD11 write Set_TQPUD11;
    Property TQPUD11_Updated: boolean read flag_TQPUD11 write flag_TQPUD11;
    Property f_TQPUD12: Int64 read ff_TQPUD12 write Set_TQPUD12;
    Property TQPUD12_Updated: boolean read flag_TQPUD12 write flag_TQPUD12;
    Property f_TQPUD13: TDateTime read ff_TQPUD13 write Set_TQPUD13;
    Property TQPUD13_Updated: boolean read flag_TQPUD13 write flag_TQPUD13;
    Property f_TQPUD14: TDateTime read ff_TQPUD14 write Set_TQPUD14;
    Property TQPUD14_Updated: boolean read flag_TQPUD14 write flag_TQPUD14;
    Property f_TQPUD15: TDateTime read ff_TQPUD15 write Set_TQPUD15;
    Property TQPUD15_Updated: boolean read flag_TQPUD15 write flag_TQPUD15;
    Property f_TQPPLANT: String read ff_TQPPLANT write Set_TQPPLANT;
    Property TQPPLANT_Updated: boolean read flag_TQPPLANT write flag_TQPPLANT;
    Property f_TQPLEGAL: String read ff_TQPLEGAL write Set_TQPLEGAL;
    Property TQPLEGAL_Updated: boolean read flag_TQPLEGAL write flag_TQPLEGAL;
    Property f_TQPORIU: String read ff_TQPORIU write Set_TQPORIU;
    Property TQPORIU_Updated: boolean read flag_TQPORIU write flag_TQPORIU;
    Property f_TQPORIG: String read ff_TQPORIG write Set_TQPORIG;
    Property TQPORIG_Updated: boolean read flag_TQPORIG write flag_TQPORIG;

    constructor Create;
    destructor Destroy; override;
    class function CreateCds: TClientDataset;
    class function ReadFromCds(aCds: TDataset): TTQP_FILE;
    function SaveToCds(Cds: TDataset): Boolean;
    class function ReadFromDB(DBA: TRemoteUnidac; Condition: String): TTQP_FILE;
    function DeleteFromDB(DBA: TRemoteUnidac; Condition: String): boolean;
    function InsertToDB(DBA: TRemoteUnidac): boolean;
    function UpdateToDB(DBA: TRemoteUnidac; Condition: String): boolean;
    function UpdateToDB2(DBA: TRemoteUnidac; Condition: String): boolean;
    function PropIsNull(PropName: string): boolean;
    function SetPropNull(PropName: string): boolean;
    function CloneTo(var NewObj: TTQP_FILE): boolean;

  end;

  TTQP_FILEList = class(TObject)
  Private
     fCount: integer;
     fList: array of TTQP_FILE;

  public    
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure add(Obj: TTQP_FILE);
    procedure Delete(Index: Integer);
    function Get(Index: Integer): Pointer;
    procedure Put(Index: Integer; Item: Pointer);
    class function ReadFromDB(DBA: TRemoteUnidac; Condition: String = ''; 
      OrderBy: String = ''; SelectOption: string=''): TTQP_FILEList;
    class function ReadFromCDS(Cds: TDataset): TTQP_FILEList;
    function SaveToCds(Cds: TDataset): boolean;
    function InsertToDB(DBA: TRemoteUnidac): boolean;
    function UpdateToDB(DBA: TRemoteUnidac; KeyFieldList: string; Condition: String): boolean;
    function DeleteFromDB(DBA: TRemoteUnidac; Condition: String): boolean;
    function SaveToFile(FileName: String; DataFormat: TDataPacketFormat): boolean;
    function LoadFromFile(FileName: String): boolean;
    function SaveToStream(aStream: TMemoryStream; DataFormat: TDataPacketFormat): boolean;
    function LoadFromStream(aStream: TMemoryStream): boolean;
    function SaveToParcel(aParcel: TQBParcel; GoodsName: AnsiString): boolean;
    function LoadFromParcel(aParcel: TQBParcel; GoodsName: AnsiString): boolean;
    function CloneTo(NewList: TTQP_FILEList): boolean;
    property Count: integer read fCount;
    property Items[Index: Integer]: Pointer read Get write Put;
    
  end;

implementation

{ TTQP_FILE }

//
// 属性设置的一系列内部过程...
procedure TTQP_FILE.Set_TQP01(_TQP01: String);
begin
   ff_TQP01:=_TQP01;
   flag_TQP01:=true;
end;

procedure TTQP_FILE.Set_TQP02(_TQP02: String);
begin
   ff_TQP02:=_TQP02;
   flag_TQP02:=true;
end;

procedure TTQP_FILE.Set_TQP03(_TQP03: String);
begin
   ff_TQP03:=_TQP03;
   flag_TQP03:=true;
end;

procedure TTQP_FILE.Set_TQP04(_TQP04: String);
begin
   ff_TQP04:=_TQP04;
   flag_TQP04:=true;
end;

procedure TTQP_FILE.Set_TQP05(_TQP05: String);
begin
   ff_TQP05:=_TQP05;
   flag_TQP05:=true;
end;

procedure TTQP_FILE.Set_TQP06(_TQP06: TDateTime);
begin
   ff_TQP06:=_TQP06;
   flag_TQP06:=true;
end;

procedure TTQP_FILE.Set_TQP07(_TQP07: TDateTime);
begin
   ff_TQP07:=_TQP07;
   flag_TQP07:=true;
end;

procedure TTQP_FILE.Set_TQP08(_TQP08: String);
begin
   ff_TQP08:=_TQP08;
   flag_TQP08:=true;
end;

procedure TTQP_FILE.Set_TQP09(_TQP09: String);
begin
   ff_TQP09:=_TQP09;
   flag_TQP09:=true;
end;

procedure TTQP_FILE.Set_TQP10(_TQP10: String);
begin
   ff_TQP10:=_TQP10;
   flag_TQP10:=true;
end;

procedure TTQP_FILE.Set_TQP11(_TQP11: Double);
begin
   ff_TQP11:=_TQP11;
   flag_TQP11:=true;
end;

procedure TTQP_FILE.Set_TQP12(_TQP12: String);
begin
   ff_TQP12:=_TQP12;
   flag_TQP12:=true;
end;

procedure TTQP_FILE.Set_TQP13(_TQP13: String);
begin
   ff_TQP13:=_TQP13;
   flag_TQP13:=true;
end;

procedure TTQP_FILE.Set_TQP14(_TQP14: TDateTime);
begin
   ff_TQP14:=_TQP14;
   flag_TQP14:=true;
end;

procedure TTQP_FILE.Set_TQP15(_TQP15: Integer);
begin
   ff_TQP15:=_TQP15;
   flag_TQP15:=true;
end;

procedure TTQP_FILE.Set_TQP16(_TQP16: Integer);
begin
   ff_TQP16:=_TQP16;
   flag_TQP16:=true;
end;

procedure TTQP_FILE.Set_TQP17(_TQP17: String);
begin
   ff_TQP17:=_TQP17;
   flag_TQP17:=true;
end;

procedure TTQP_FILE.Set_TQP18(_TQP18: Double);
begin
   ff_TQP18:=_TQP18;
   flag_TQP18:=true;
end;

procedure TTQP_FILE.Set_TQP19(_TQP19: String);
begin
   ff_TQP19:=_TQP19;
   flag_TQP19:=true;
end;

procedure TTQP_FILE.Set_TQP20(_TQP20: String);
begin
   ff_TQP20:=_TQP20;
   flag_TQP20:=true;
end;

procedure TTQP_FILE.Set_TQP21(_TQP21: String);
begin
   ff_TQP21:=_TQP21;
   flag_TQP21:=true;
end;

procedure TTQP_FILE.Set_TQP22(_TQP22: String);
begin
   ff_TQP22:=_TQP22;
   flag_TQP22:=true;
end;

procedure TTQP_FILE.Set_TQP23(_TQP23: Double);
begin
   ff_TQP23:=_TQP23;
   flag_TQP23:=true;
end;

procedure TTQP_FILE.Set_TQP24(_TQP24: Integer);
begin
   ff_TQP24:=_TQP24;
   flag_TQP24:=true;
end;

procedure TTQP_FILE.Set_TQP25(_TQP25: String);
begin
   ff_TQP25:=_TQP25;
   flag_TQP25:=true;
end;

procedure TTQP_FILE.Set_TQPACTI(_TQPACTI: String);
begin
   ff_TQPACTI:=_TQPACTI;
   flag_TQPACTI:=true;
end;

procedure TTQP_FILE.Set_TQPUSER(_TQPUSER: String);
begin
   ff_TQPUSER:=_TQPUSER;
   flag_TQPUSER:=true;
end;

procedure TTQP_FILE.Set_TQPGRUP(_TQPGRUP: String);
begin
   ff_TQPGRUP:=_TQPGRUP;
   flag_TQPGRUP:=true;
end;

procedure TTQP_FILE.Set_TQPMODU(_TQPMODU: String);
begin
   ff_TQPMODU:=_TQPMODU;
   flag_TQPMODU:=true;
end;

procedure TTQP_FILE.Set_TQPDATE(_TQPDATE: TDateTime);
begin
   ff_TQPDATE:=_TQPDATE;
   flag_TQPDATE:=true;
end;

procedure TTQP_FILE.Set_TQPUD01(_TQPUD01: String);
begin
   ff_TQPUD01:=_TQPUD01;
   flag_TQPUD01:=true;
end;

procedure TTQP_FILE.Set_TQPUD02(_TQPUD02: String);
begin
   ff_TQPUD02:=_TQPUD02;
   flag_TQPUD02:=true;
end;

procedure TTQP_FILE.Set_TQPUD03(_TQPUD03: String);
begin
   ff_TQPUD03:=_TQPUD03;
   flag_TQPUD03:=true;
end;

procedure TTQP_FILE.Set_TQPUD04(_TQPUD04: String);
begin
   ff_TQPUD04:=_TQPUD04;
   flag_TQPUD04:=true;
end;

procedure TTQP_FILE.Set_TQPUD05(_TQPUD05: String);
begin
   ff_TQPUD05:=_TQPUD05;
   flag_TQPUD05:=true;
end;

procedure TTQP_FILE.Set_TQPUD06(_TQPUD06: String);
begin
   ff_TQPUD06:=_TQPUD06;
   flag_TQPUD06:=true;
end;

procedure TTQP_FILE.Set_TQPUD07(_TQPUD07: Double);
begin
   ff_TQPUD07:=_TQPUD07;
   flag_TQPUD07:=true;
end;

procedure TTQP_FILE.Set_TQPUD08(_TQPUD08: Double);
begin
   ff_TQPUD08:=_TQPUD08;
   flag_TQPUD08:=true;
end;

procedure TTQP_FILE.Set_TQPUD09(_TQPUD09: Double);
begin
   ff_TQPUD09:=_TQPUD09;
   flag_TQPUD09:=true;
end;

procedure TTQP_FILE.Set_TQPUD10(_TQPUD10: Int64);
begin
   ff_TQPUD10:=_TQPUD10;
   flag_TQPUD10:=true;
end;

procedure TTQP_FILE.Set_TQPUD11(_TQPUD11: Int64);
begin
   ff_TQPUD11:=_TQPUD11;
   flag_TQPUD11:=true;
end;

procedure TTQP_FILE.Set_TQPUD12(_TQPUD12: Int64);
begin
   ff_TQPUD12:=_TQPUD12;
   flag_TQPUD12:=true;
end;

procedure TTQP_FILE.Set_TQPUD13(_TQPUD13: TDateTime);
begin
   ff_TQPUD13:=_TQPUD13;
   flag_TQPUD13:=true;
end;

procedure TTQP_FILE.Set_TQPUD14(_TQPUD14: TDateTime);
begin
   ff_TQPUD14:=_TQPUD14;
   flag_TQPUD14:=true;
end;

procedure TTQP_FILE.Set_TQPUD15(_TQPUD15: TDateTime);
begin
   ff_TQPUD15:=_TQPUD15;
   flag_TQPUD15:=true;
end;

procedure TTQP_FILE.Set_TQPPLANT(_TQPPLANT: String);
begin
   ff_TQPPLANT:=_TQPPLANT;
   flag_TQPPLANT:=true;
end;

procedure TTQP_FILE.Set_TQPLEGAL(_TQPLEGAL: String);
begin
   ff_TQPLEGAL:=_TQPLEGAL;
   flag_TQPLEGAL:=true;
end;

procedure TTQP_FILE.Set_TQPORIU(_TQPORIU: String);
begin
   ff_TQPORIU:=_TQPORIU;
   flag_TQPORIU:=true;
end;

procedure TTQP_FILE.Set_TQPORIG(_TQPORIG: String);
begin
   ff_TQPORIG:=_TQPORIG;
   flag_TQPORIG:=true;
end;


//
// 创建Cds容器，用于记录数据的本地暂存...
class function TTQP_FILE.CreateCds: TClientDataset;
begin
  Result:=TClientDataset.Create(nil);
  with Result do
     begin
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP01';
              DataType:=ftString;
              Size:=40;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP02';
              DataType:=ftString;
              Size:=160;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP03';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP04';
              DataType:=ftString;
              Size:=2;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP05';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP06';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP07';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP08';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP09';
              DataType:=ftString;
              Size:=60;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP10';
              DataType:=ftString;
              Size:=2;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP11';
              DataType:=ftFloat;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP12';
              DataType:=ftString;
              Size:=2;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP13';
              DataType:=ftString;
              Size:=510;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP14';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP15';
              DataType:=ftInteger;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP16';
              DataType:=ftInteger;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP17';
              DataType:=ftString;
              Size:=60;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP18';
              DataType:=ftFloat;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP19';
              DataType:=ftString;
              Size:=2;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP20';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP21';
              DataType:=ftString;
              Size:=8;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP22';
              DataType:=ftString;
              Size:=8;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP23';
              DataType:=ftFloat;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP24';
              DataType:=ftInteger;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQP25';
              DataType:=ftString;
              Size:=2;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPACTI';
              DataType:=ftString;
              Size:=2;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUSER';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPGRUP';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPMODU';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPDATE';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD01';
              DataType:=ftString;
              Size:=510;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD02';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD03';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD04';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD05';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD06';
              DataType:=ftString;
              Size:=80;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD07';
              DataType:=ftFloat;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD08';
              DataType:=ftFloat;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD09';
              DataType:=ftFloat;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD10';
              DataType:=ftLargeint;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD11';
              DataType:=ftLargeint;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD12';
              DataType:=ftLargeint;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD13';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD14';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPUD15';
              DataType:=ftDateTime;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPPLANT';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPLEGAL';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPORIU';
              DataType:=ftString;
              Size:=20;
           end;
        with FieldDefs.AddFieldDef do
           begin
              Name:='TQPORIG';
              DataType:=ftString;
              Size:=20;
           end;
        CreateDataSet;
     end;
  Result.Open;
end;

//
// 创建对象实例...
constructor TTQP_FILE.Create;
begin
  inherited;
  ff_TQP01:='';
  flag_TQP01:=false;
  ff_TQP02:='';
  flag_TQP02:=false;
  ff_TQP03:='';
  flag_TQP03:=false;
  ff_TQP04:='';
  flag_TQP04:=false;
  ff_TQP05:='';
  flag_TQP05:=false;
  ff_TQP06:=0;
  flag_TQP06:=false;
  ff_TQP07:=0;
  flag_TQP07:=false;
  ff_TQP08:='';
  flag_TQP08:=false;
  ff_TQP09:='';
  flag_TQP09:=false;
  ff_TQP10:='';
  flag_TQP10:=false;
  ff_TQP11:=0.0;
  flag_TQP11:=false;
  ff_TQP12:='';
  flag_TQP12:=false;
  ff_TQP13:='';
  flag_TQP13:=false;
  ff_TQP14:=0;
  flag_TQP14:=false;
  ff_TQP15:=0;
  flag_TQP15:=false;
  ff_TQP16:=0;
  flag_TQP16:=false;
  ff_TQP17:='';
  flag_TQP17:=false;
  ff_TQP18:=0.0;
  flag_TQP18:=false;
  ff_TQP19:='';
  flag_TQP19:=false;
  ff_TQP20:='';
  flag_TQP20:=false;
  ff_TQP21:='';
  flag_TQP21:=false;
  ff_TQP22:='';
  flag_TQP22:=false;
  ff_TQP23:=0.0;
  flag_TQP23:=false;
  ff_TQP24:=0;
  flag_TQP24:=false;
  ff_TQP25:='';
  flag_TQP25:=false;
  ff_TQPACTI:='';
  flag_TQPACTI:=false;
  ff_TQPUSER:='';
  flag_TQPUSER:=false;
  ff_TQPGRUP:='';
  flag_TQPGRUP:=false;
  ff_TQPMODU:='';
  flag_TQPMODU:=false;
  ff_TQPDATE:=0;
  flag_TQPDATE:=false;
  ff_TQPUD01:='';
  flag_TQPUD01:=false;
  ff_TQPUD02:='';
  flag_TQPUD02:=false;
  ff_TQPUD03:='';
  flag_TQPUD03:=false;
  ff_TQPUD04:='';
  flag_TQPUD04:=false;
  ff_TQPUD05:='';
  flag_TQPUD05:=false;
  ff_TQPUD06:='';
  flag_TQPUD06:=false;
  ff_TQPUD07:=0.0;
  flag_TQPUD07:=false;
  ff_TQPUD08:=0.0;
  flag_TQPUD08:=false;
  ff_TQPUD09:=0.0;
  flag_TQPUD09:=false;
  ff_TQPUD10:=0;
  flag_TQPUD10:=false;
  ff_TQPUD11:=0;
  flag_TQPUD11:=false;
  ff_TQPUD12:=0;
  flag_TQPUD12:=false;
  ff_TQPUD13:=0;
  flag_TQPUD13:=false;
  ff_TQPUD14:=0;
  flag_TQPUD14:=false;
  ff_TQPUD15:=0;
  flag_TQPUD15:=false;
  ff_TQPPLANT:='';
  flag_TQPPLANT:=false;
  ff_TQPLEGAL:='';
  flag_TQPLEGAL:=false;
  ff_TQPORIU:='';
  flag_TQPORIU:=false;
  ff_TQPORIG:='';
  flag_TQPORIG:=false;
end;

//
// 释放对象实例...
destructor TTQP_FILE.Destroy;
begin
  inherited;
end;

//
// 将Cds中的当前记录转换为一个对象实例...
class function TTQP_FILE.ReadFromCds(aCds: TDataset): TTQP_FILE;
begin
  if (not aCds.active) or (aCds.RecordCount<=0) then
    begin
      Result:=nil;
      exit;
    end;
  Result := TTQP_FILE.Create;
  with Result do
    begin
      ff_TQP01 := Trim(aCds.FieldByName('TQP01').AsString);
      flag_TQP01:=(not aCds.FieldByName('TQP01').IsNull);
      ff_TQP02 := Trim(aCds.FieldByName('TQP02').AsString);
      flag_TQP02:=(not aCds.FieldByName('TQP02').IsNull);
      ff_TQP03 := Trim(aCds.FieldByName('TQP03').AsString);
      flag_TQP03:=(not aCds.FieldByName('TQP03').IsNull);
      ff_TQP04 := Trim(aCds.FieldByName('TQP04').AsString);
      flag_TQP04:=(not aCds.FieldByName('TQP04').IsNull);
      ff_TQP05 := Trim(aCds.FieldByName('TQP05').AsString);
      flag_TQP05:=(not aCds.FieldByName('TQP05').IsNull);
      ff_TQP06 := aCds.FieldByName('TQP06').AsDateTime;
      flag_TQP06:=(not aCds.FieldByName('TQP06').IsNull);
      ff_TQP07 := aCds.FieldByName('TQP07').AsDateTime;
      flag_TQP07:=(not aCds.FieldByName('TQP07').IsNull);
      ff_TQP08 := Trim(aCds.FieldByName('TQP08').AsString);
      flag_TQP08:=(not aCds.FieldByName('TQP08').IsNull);
      ff_TQP09 := Trim(aCds.FieldByName('TQP09').AsString);
      flag_TQP09:=(not aCds.FieldByName('TQP09').IsNull);
      ff_TQP10 := Trim(aCds.FieldByName('TQP10').AsString);
      flag_TQP10:=(not aCds.FieldByName('TQP10').IsNull);
      ff_TQP11 := aCds.FieldByName('TQP11').AsFloat;
      flag_TQP11:=(not aCds.FieldByName('TQP11').IsNull);
      ff_TQP12 := Trim(aCds.FieldByName('TQP12').AsString);
      flag_TQP12:=(not aCds.FieldByName('TQP12').IsNull);
      ff_TQP13 := Trim(aCds.FieldByName('TQP13').AsString);
      flag_TQP13:=(not aCds.FieldByName('TQP13').IsNull);
      ff_TQP14 := aCds.FieldByName('TQP14').AsDateTime;
      flag_TQP14:=(not aCds.FieldByName('TQP14').IsNull);
      ff_TQP15 := aCds.FieldByName('TQP15').AsInteger;
      flag_TQP15:=(not aCds.FieldByName('TQP15').IsNull);
      ff_TQP16 := aCds.FieldByName('TQP16').AsInteger;
      flag_TQP16:=(not aCds.FieldByName('TQP16').IsNull);
      ff_TQP17 := Trim(aCds.FieldByName('TQP17').AsString);
      flag_TQP17:=(not aCds.FieldByName('TQP17').IsNull);
      ff_TQP18 := aCds.FieldByName('TQP18').AsFloat;
      flag_TQP18:=(not aCds.FieldByName('TQP18').IsNull);
      ff_TQP19 := Trim(aCds.FieldByName('TQP19').AsString);
      flag_TQP19:=(not aCds.FieldByName('TQP19').IsNull);
      ff_TQP20 := Trim(aCds.FieldByName('TQP20').AsString);
      flag_TQP20:=(not aCds.FieldByName('TQP20').IsNull);
      ff_TQP21 := Trim(aCds.FieldByName('TQP21').AsString);
      flag_TQP21:=(not aCds.FieldByName('TQP21').IsNull);
      ff_TQP22 := Trim(aCds.FieldByName('TQP22').AsString);
      flag_TQP22:=(not aCds.FieldByName('TQP22').IsNull);
      ff_TQP23 := aCds.FieldByName('TQP23').AsFloat;
      flag_TQP23:=(not aCds.FieldByName('TQP23').IsNull);
      ff_TQP24 := aCds.FieldByName('TQP24').AsInteger;
      flag_TQP24:=(not aCds.FieldByName('TQP24').IsNull);
      ff_TQP25 := Trim(aCds.FieldByName('TQP25').AsString);
      flag_TQP25:=(not aCds.FieldByName('TQP25').IsNull);
      ff_TQPACTI := Trim(aCds.FieldByName('TQPACTI').AsString);
      flag_TQPACTI:=(not aCds.FieldByName('TQPACTI').IsNull);
      ff_TQPUSER := Trim(aCds.FieldByName('TQPUSER').AsString);
      flag_TQPUSER:=(not aCds.FieldByName('TQPUSER').IsNull);
      ff_TQPGRUP := Trim(aCds.FieldByName('TQPGRUP').AsString);
      flag_TQPGRUP:=(not aCds.FieldByName('TQPGRUP').IsNull);
      ff_TQPMODU := Trim(aCds.FieldByName('TQPMODU').AsString);
      flag_TQPMODU:=(not aCds.FieldByName('TQPMODU').IsNull);
      ff_TQPDATE := aCds.FieldByName('TQPDATE').AsDateTime;
      flag_TQPDATE:=(not aCds.FieldByName('TQPDATE').IsNull);
      ff_TQPUD01 := Trim(aCds.FieldByName('TQPUD01').AsString);
      flag_TQPUD01:=(not aCds.FieldByName('TQPUD01').IsNull);
      ff_TQPUD02 := Trim(aCds.FieldByName('TQPUD02').AsString);
      flag_TQPUD02:=(not aCds.FieldByName('TQPUD02').IsNull);
      ff_TQPUD03 := Trim(aCds.FieldByName('TQPUD03').AsString);
      flag_TQPUD03:=(not aCds.FieldByName('TQPUD03').IsNull);
      ff_TQPUD04 := Trim(aCds.FieldByName('TQPUD04').AsString);
      flag_TQPUD04:=(not aCds.FieldByName('TQPUD04').IsNull);
      ff_TQPUD05 := Trim(aCds.FieldByName('TQPUD05').AsString);
      flag_TQPUD05:=(not aCds.FieldByName('TQPUD05').IsNull);
      ff_TQPUD06 := Trim(aCds.FieldByName('TQPUD06').AsString);
      flag_TQPUD06:=(not aCds.FieldByName('TQPUD06').IsNull);
      ff_TQPUD07 := aCds.FieldByName('TQPUD07').AsFloat;
      flag_TQPUD07:=(not aCds.FieldByName('TQPUD07').IsNull);
      ff_TQPUD08 := aCds.FieldByName('TQPUD08').AsFloat;
      flag_TQPUD08:=(not aCds.FieldByName('TQPUD08').IsNull);
      ff_TQPUD09 := aCds.FieldByName('TQPUD09').AsFloat;
      flag_TQPUD09:=(not aCds.FieldByName('TQPUD09').IsNull);
      ff_TQPUD10 := aCds.FieldByName('TQPUD10').AsInteger;
      flag_TQPUD10:=(not aCds.FieldByName('TQPUD10').IsNull);
      ff_TQPUD11 := aCds.FieldByName('TQPUD11').AsInteger;
      flag_TQPUD11:=(not aCds.FieldByName('TQPUD11').IsNull);
      ff_TQPUD12 := aCds.FieldByName('TQPUD12').AsInteger;
      flag_TQPUD12:=(not aCds.FieldByName('TQPUD12').IsNull);
      ff_TQPUD13 := aCds.FieldByName('TQPUD13').AsDateTime;
      flag_TQPUD13:=(not aCds.FieldByName('TQPUD13').IsNull);
      ff_TQPUD14 := aCds.FieldByName('TQPUD14').AsDateTime;
      flag_TQPUD14:=(not aCds.FieldByName('TQPUD14').IsNull);
      ff_TQPUD15 := aCds.FieldByName('TQPUD15').AsDateTime;
      flag_TQPUD15:=(not aCds.FieldByName('TQPUD15').IsNull);
      ff_TQPPLANT := Trim(aCds.FieldByName('TQPPLANT').AsString);
      flag_TQPPLANT:=(not aCds.FieldByName('TQPPLANT').IsNull);
      ff_TQPLEGAL := Trim(aCds.FieldByName('TQPLEGAL').AsString);
      flag_TQPLEGAL:=(not aCds.FieldByName('TQPLEGAL').IsNull);
      ff_TQPORIU := Trim(aCds.FieldByName('TQPORIU').AsString);
      flag_TQPORIU:=(not aCds.FieldByName('TQPORIU').IsNull);
      ff_TQPORIG := Trim(aCds.FieldByName('TQPORIG').AsString);
      flag_TQPORIG:=(not aCds.FieldByName('TQPORIG').IsNull);
    end;
end;

//
// 将对象转换到Cds的当前记录...
function TTQP_FILE.SaveToCds(Cds: TDataset): Boolean;
begin
   try
     Cds.edit;
     if flag_TQP01 then
        Cds.FieldValues['TQP01']:=ff_TQP01;
     if flag_TQP02 then
        Cds.FieldValues['TQP02']:=ff_TQP02;
     if flag_TQP03 then
        Cds.FieldValues['TQP03']:=ff_TQP03;
     if flag_TQP04 then
        Cds.FieldValues['TQP04']:=ff_TQP04;
     if flag_TQP05 then
        Cds.FieldValues['TQP05']:=ff_TQP05;
     if flag_TQP06 then
        Cds.FieldValues['TQP06']:=ff_TQP06;
     if flag_TQP07 then
        Cds.FieldValues['TQP07']:=ff_TQP07;
     if flag_TQP08 then
        Cds.FieldValues['TQP08']:=ff_TQP08;
     if flag_TQP09 then
        Cds.FieldValues['TQP09']:=ff_TQP09;
     if flag_TQP10 then
        Cds.FieldValues['TQP10']:=ff_TQP10;
     if flag_TQP11 then
        Cds.FieldValues['TQP11']:=ff_TQP11;
     if flag_TQP12 then
        Cds.FieldValues['TQP12']:=ff_TQP12;
     if flag_TQP13 then
        Cds.FieldValues['TQP13']:=ff_TQP13;
     if flag_TQP14 then
        Cds.FieldValues['TQP14']:=ff_TQP14;
     if flag_TQP15 then
        Cds.FieldValues['TQP15']:=ff_TQP15;
     if flag_TQP16 then
        Cds.FieldValues['TQP16']:=ff_TQP16;
     if flag_TQP17 then
        Cds.FieldValues['TQP17']:=ff_TQP17;
     if flag_TQP18 then
        Cds.FieldValues['TQP18']:=ff_TQP18;
     if flag_TQP19 then
        Cds.FieldValues['TQP19']:=ff_TQP19;
     if flag_TQP20 then
        Cds.FieldValues['TQP20']:=ff_TQP20;
     if flag_TQP21 then
        Cds.FieldValues['TQP21']:=ff_TQP21;
     if flag_TQP22 then
        Cds.FieldValues['TQP22']:=ff_TQP22;
     if flag_TQP23 then
        Cds.FieldValues['TQP23']:=ff_TQP23;
     if flag_TQP24 then
        Cds.FieldValues['TQP24']:=ff_TQP24;
     if flag_TQP25 then
        Cds.FieldValues['TQP25']:=ff_TQP25;
     if flag_TQPACTI then
        Cds.FieldValues['TQPACTI']:=ff_TQPACTI;
     if flag_TQPUSER then
        Cds.FieldValues['TQPUSER']:=ff_TQPUSER;
     if flag_TQPGRUP then
        Cds.FieldValues['TQPGRUP']:=ff_TQPGRUP;
     if flag_TQPMODU then
        Cds.FieldValues['TQPMODU']:=ff_TQPMODU;
     if flag_TQPDATE then
        Cds.FieldValues['TQPDATE']:=ff_TQPDATE;
     if flag_TQPUD01 then
        Cds.FieldValues['TQPUD01']:=ff_TQPUD01;
     if flag_TQPUD02 then
        Cds.FieldValues['TQPUD02']:=ff_TQPUD02;
     if flag_TQPUD03 then
        Cds.FieldValues['TQPUD03']:=ff_TQPUD03;
     if flag_TQPUD04 then
        Cds.FieldValues['TQPUD04']:=ff_TQPUD04;
     if flag_TQPUD05 then
        Cds.FieldValues['TQPUD05']:=ff_TQPUD05;
     if flag_TQPUD06 then
        Cds.FieldValues['TQPUD06']:=ff_TQPUD06;
     if flag_TQPUD07 then
        Cds.FieldValues['TQPUD07']:=ff_TQPUD07;
     if flag_TQPUD08 then
        Cds.FieldValues['TQPUD08']:=ff_TQPUD08;
     if flag_TQPUD09 then
        Cds.FieldValues['TQPUD09']:=ff_TQPUD09;
     if flag_TQPUD10 then
        Cds.FieldValues['TQPUD10']:=ff_TQPUD10;
     if flag_TQPUD11 then
        Cds.FieldValues['TQPUD11']:=ff_TQPUD11;
     if flag_TQPUD12 then
        Cds.FieldValues['TQPUD12']:=ff_TQPUD12;
     if flag_TQPUD13 then
        Cds.FieldValues['TQPUD13']:=ff_TQPUD13;
     if flag_TQPUD14 then
        Cds.FieldValues['TQPUD14']:=ff_TQPUD14;
     if flag_TQPUD15 then
        Cds.FieldValues['TQPUD15']:=ff_TQPUD15;
     if flag_TQPPLANT then
        Cds.FieldValues['TQPPLANT']:=ff_TQPPLANT;
     if flag_TQPLEGAL then
        Cds.FieldValues['TQPLEGAL']:=ff_TQPLEGAL;
     if flag_TQPORIU then
        Cds.FieldValues['TQPORIU']:=ff_TQPORIU;
     if flag_TQPORIG then
        Cds.FieldValues['TQPORIG']:=ff_TQPORIG;
     Cds.post;
     result:=true;
   except
     result:=false;
   end;
end;

//
// 从远程数据库中读一个记录，并转换成对象实例...
class function TTQP_FILE.ReadFromDB(DBA: TRemoteUnidac; Condition: String): TTQP_FILE;
var
  SqlCommand: String;
  Cds: TClientDataset;
begin
  Result := nil;
  Cds:=TClientDataset.Create(nil);
  if Condition='' then
     SqlCommand:='SELECT * FROM TQP_FILE'
  else
     SqlCommand := 'SELECT * FROM TQP_FILE WHERE '+condition;
  if not DBA.ReadDataset(SqlCommand,Cds) then
     begin
       Cds.Free;
       exit;
     end;
  Result:=ReadFromCds(Cds);
  Cds.Free;
end;

//
// 将对象实例的属性写入到远程数据库记录...
function TTQP_FILE.InsertToDB(DBA: TRemoteUnidac): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQP_FILE.CreateCds;
  try
     Cds.Append;
     if flag_TQP01 then
        Cds.FieldValues['TQP01']:=ff_TQP01;
     if flag_TQP02 then
        Cds.FieldValues['TQP02']:=ff_TQP02;
     if flag_TQP03 then
        Cds.FieldValues['TQP03']:=ff_TQP03;
     if flag_TQP04 then
        Cds.FieldValues['TQP04']:=ff_TQP04;
     if flag_TQP05 then
        Cds.FieldValues['TQP05']:=ff_TQP05;
     if flag_TQP06 then
        Cds.FieldValues['TQP06']:=ff_TQP06;
     if flag_TQP07 then
        Cds.FieldValues['TQP07']:=ff_TQP07;
     if flag_TQP08 then
        Cds.FieldValues['TQP08']:=ff_TQP08;
     if flag_TQP09 then
        Cds.FieldValues['TQP09']:=ff_TQP09;
     if flag_TQP10 then
        Cds.FieldValues['TQP10']:=ff_TQP10;
     if flag_TQP11 then
        Cds.FieldValues['TQP11']:=ff_TQP11;
     if flag_TQP12 then
        Cds.FieldValues['TQP12']:=ff_TQP12;
     if flag_TQP13 then
        Cds.FieldValues['TQP13']:=ff_TQP13;
     if flag_TQP14 then
        Cds.FieldValues['TQP14']:=ff_TQP14;
     if flag_TQP15 then
        Cds.FieldValues['TQP15']:=ff_TQP15;
     if flag_TQP16 then
        Cds.FieldValues['TQP16']:=ff_TQP16;
     if flag_TQP17 then
        Cds.FieldValues['TQP17']:=ff_TQP17;
     if flag_TQP18 then
        Cds.FieldValues['TQP18']:=ff_TQP18;
     if flag_TQP19 then
        Cds.FieldValues['TQP19']:=ff_TQP19;
     if flag_TQP20 then
        Cds.FieldValues['TQP20']:=ff_TQP20;
     if flag_TQP21 then
        Cds.FieldValues['TQP21']:=ff_TQP21;
     if flag_TQP22 then
        Cds.FieldValues['TQP22']:=ff_TQP22;
     if flag_TQP23 then
        Cds.FieldValues['TQP23']:=ff_TQP23;
     if flag_TQP24 then
        Cds.FieldValues['TQP24']:=ff_TQP24;
     if flag_TQP25 then
        Cds.FieldValues['TQP25']:=ff_TQP25;
     if flag_TQPACTI then
        Cds.FieldValues['TQPACTI']:=ff_TQPACTI;
     if flag_TQPUSER then
        Cds.FieldValues['TQPUSER']:=ff_TQPUSER;
     if flag_TQPGRUP then
        Cds.FieldValues['TQPGRUP']:=ff_TQPGRUP;
     if flag_TQPMODU then
        Cds.FieldValues['TQPMODU']:=ff_TQPMODU;
     if flag_TQPDATE then
        Cds.FieldValues['TQPDATE']:=ff_TQPDATE;
     if flag_TQPUD01 then
        Cds.FieldValues['TQPUD01']:=ff_TQPUD01;
     if flag_TQPUD02 then
        Cds.FieldValues['TQPUD02']:=ff_TQPUD02;
     if flag_TQPUD03 then
        Cds.FieldValues['TQPUD03']:=ff_TQPUD03;
     if flag_TQPUD04 then
        Cds.FieldValues['TQPUD04']:=ff_TQPUD04;
     if flag_TQPUD05 then
        Cds.FieldValues['TQPUD05']:=ff_TQPUD05;
     if flag_TQPUD06 then
        Cds.FieldValues['TQPUD06']:=ff_TQPUD06;
     if flag_TQPUD07 then
        Cds.FieldValues['TQPUD07']:=ff_TQPUD07;
     if flag_TQPUD08 then
        Cds.FieldValues['TQPUD08']:=ff_TQPUD08;
     if flag_TQPUD09 then
        Cds.FieldValues['TQPUD09']:=ff_TQPUD09;
     if flag_TQPUD10 then
        Cds.FieldValues['TQPUD10']:=ff_TQPUD10;
     if flag_TQPUD11 then
        Cds.FieldValues['TQPUD11']:=ff_TQPUD11;
     if flag_TQPUD12 then
        Cds.FieldValues['TQPUD12']:=ff_TQPUD12;
     if flag_TQPUD13 then
        Cds.FieldValues['TQPUD13']:=ff_TQPUD13;
     if flag_TQPUD14 then
        Cds.FieldValues['TQPUD14']:=ff_TQPUD14;
     if flag_TQPUD15 then
        Cds.FieldValues['TQPUD15']:=ff_TQPUD15;
     if flag_TQPPLANT then
        Cds.FieldValues['TQPPLANT']:=ff_TQPPLANT;
     if flag_TQPLEGAL then
        Cds.FieldValues['TQPLEGAL']:=ff_TQPLEGAL;
     if flag_TQPORIU then
        Cds.FieldValues['TQPORIU']:=ff_TQPORIU;
     if flag_TQPORIG then
        Cds.FieldValues['TQPORIG']:=ff_TQPORIG;
     Cds.Post;
  except
     Cds.Free;
     Result:=false;
     exit;
  end;
  Result:=DBA.AppendRecord('TQP_FILE',Cds);
  Cds.Free;
end;

//
// 将对象实例的属性更新到数据库记录（更新全部字段）...
function TTQP_FILE.UpdateToDB(DBA: TRemoteUnidac; Condition: String): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQP_FILE.CreateCds;
  try
     Cds.Append;
     if flag_TQP01 then
        Cds.FieldValues['TQP01']:=ff_TQP01;
     if flag_TQP02 then
        Cds.FieldValues['TQP02']:=ff_TQP02;
     if flag_TQP03 then
        Cds.FieldValues['TQP03']:=ff_TQP03;
     if flag_TQP04 then
        Cds.FieldValues['TQP04']:=ff_TQP04;
     if flag_TQP05 then
        Cds.FieldValues['TQP05']:=ff_TQP05;
     if flag_TQP06 then
        Cds.FieldValues['TQP06']:=ff_TQP06;
     if flag_TQP07 then
        Cds.FieldValues['TQP07']:=ff_TQP07;
     if flag_TQP08 then
        Cds.FieldValues['TQP08']:=ff_TQP08;
     if flag_TQP09 then
        Cds.FieldValues['TQP09']:=ff_TQP09;
     if flag_TQP10 then
        Cds.FieldValues['TQP10']:=ff_TQP10;
     if flag_TQP11 then
        Cds.FieldValues['TQP11']:=ff_TQP11;
     if flag_TQP12 then
        Cds.FieldValues['TQP12']:=ff_TQP12;
     if flag_TQP13 then
        Cds.FieldValues['TQP13']:=ff_TQP13;
     if flag_TQP14 then
        Cds.FieldValues['TQP14']:=ff_TQP14;
     if flag_TQP15 then
        Cds.FieldValues['TQP15']:=ff_TQP15;
     if flag_TQP16 then
        Cds.FieldValues['TQP16']:=ff_TQP16;
     if flag_TQP17 then
        Cds.FieldValues['TQP17']:=ff_TQP17;
     if flag_TQP18 then
        Cds.FieldValues['TQP18']:=ff_TQP18;
     if flag_TQP19 then
        Cds.FieldValues['TQP19']:=ff_TQP19;
     if flag_TQP20 then
        Cds.FieldValues['TQP20']:=ff_TQP20;
     if flag_TQP21 then
        Cds.FieldValues['TQP21']:=ff_TQP21;
     if flag_TQP22 then
        Cds.FieldValues['TQP22']:=ff_TQP22;
     if flag_TQP23 then
        Cds.FieldValues['TQP23']:=ff_TQP23;
     if flag_TQP24 then
        Cds.FieldValues['TQP24']:=ff_TQP24;
     if flag_TQP25 then
        Cds.FieldValues['TQP25']:=ff_TQP25;
     if flag_TQPACTI then
        Cds.FieldValues['TQPACTI']:=ff_TQPACTI;
     if flag_TQPUSER then
        Cds.FieldValues['TQPUSER']:=ff_TQPUSER;
     if flag_TQPGRUP then
        Cds.FieldValues['TQPGRUP']:=ff_TQPGRUP;
     if flag_TQPMODU then
        Cds.FieldValues['TQPMODU']:=ff_TQPMODU;
     if flag_TQPDATE then
        Cds.FieldValues['TQPDATE']:=ff_TQPDATE;
     if flag_TQPUD01 then
        Cds.FieldValues['TQPUD01']:=ff_TQPUD01;
     if flag_TQPUD02 then
        Cds.FieldValues['TQPUD02']:=ff_TQPUD02;
     if flag_TQPUD03 then
        Cds.FieldValues['TQPUD03']:=ff_TQPUD03;
     if flag_TQPUD04 then
        Cds.FieldValues['TQPUD04']:=ff_TQPUD04;
     if flag_TQPUD05 then
        Cds.FieldValues['TQPUD05']:=ff_TQPUD05;
     if flag_TQPUD06 then
        Cds.FieldValues['TQPUD06']:=ff_TQPUD06;
     if flag_TQPUD07 then
        Cds.FieldValues['TQPUD07']:=ff_TQPUD07;
     if flag_TQPUD08 then
        Cds.FieldValues['TQPUD08']:=ff_TQPUD08;
     if flag_TQPUD09 then
        Cds.FieldValues['TQPUD09']:=ff_TQPUD09;
     if flag_TQPUD10 then
        Cds.FieldValues['TQPUD10']:=ff_TQPUD10;
     if flag_TQPUD11 then
        Cds.FieldValues['TQPUD11']:=ff_TQPUD11;
     if flag_TQPUD12 then
        Cds.FieldValues['TQPUD12']:=ff_TQPUD12;
     if flag_TQPUD13 then
        Cds.FieldValues['TQPUD13']:=ff_TQPUD13;
     if flag_TQPUD14 then
        Cds.FieldValues['TQPUD14']:=ff_TQPUD14;
     if flag_TQPUD15 then
        Cds.FieldValues['TQPUD15']:=ff_TQPUD15;
     if flag_TQPPLANT then
        Cds.FieldValues['TQPPLANT']:=ff_TQPPLANT;
     if flag_TQPLEGAL then
        Cds.FieldValues['TQPLEGAL']:=ff_TQPLEGAL;
     if flag_TQPORIU then
        Cds.FieldValues['TQPORIU']:=ff_TQPORIU;
     if flag_TQPORIG then
        Cds.FieldValues['TQPORIG']:=ff_TQPORIG;
     Cds.Post;
  except
     Cds.Free;
     Result:=false;
     exit;
  end;
  Result:=DBA.UpdateRecord('TQP_FILE',Condition,Cds);
  Cds.Free;
end;

//
// 将对象实例的属性更新到数据库记录（更新修改了的字段）...
function TTQP_FILE.UpdateToDB2(DBA: TRemoteUnidac; Condition: String): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQP_FILE.CreateCds;
  try
     Cds.Append;
     if flag_TQP01 then
        Cds.FieldValues['TQP01']:=ff_TQP01;
     if flag_TQP02 then
        Cds.FieldValues['TQP02']:=ff_TQP02;
     if flag_TQP03 then
        Cds.FieldValues['TQP03']:=ff_TQP03;
     if flag_TQP04 then
        Cds.FieldValues['TQP04']:=ff_TQP04;
     if flag_TQP05 then
        Cds.FieldValues['TQP05']:=ff_TQP05;
     if flag_TQP06 then
        Cds.FieldValues['TQP06']:=ff_TQP06;
     if flag_TQP07 then
        Cds.FieldValues['TQP07']:=ff_TQP07;
     if flag_TQP08 then
        Cds.FieldValues['TQP08']:=ff_TQP08;
     if flag_TQP09 then
        Cds.FieldValues['TQP09']:=ff_TQP09;
     if flag_TQP10 then
        Cds.FieldValues['TQP10']:=ff_TQP10;
     if flag_TQP11 then
        Cds.FieldValues['TQP11']:=ff_TQP11;
     if flag_TQP12 then
        Cds.FieldValues['TQP12']:=ff_TQP12;
     if flag_TQP13 then
        Cds.FieldValues['TQP13']:=ff_TQP13;
     if flag_TQP14 then
        Cds.FieldValues['TQP14']:=ff_TQP14;
     if flag_TQP15 then
        Cds.FieldValues['TQP15']:=ff_TQP15;
     if flag_TQP16 then
        Cds.FieldValues['TQP16']:=ff_TQP16;
     if flag_TQP17 then
        Cds.FieldValues['TQP17']:=ff_TQP17;
     if flag_TQP18 then
        Cds.FieldValues['TQP18']:=ff_TQP18;
     if flag_TQP19 then
        Cds.FieldValues['TQP19']:=ff_TQP19;
     if flag_TQP20 then
        Cds.FieldValues['TQP20']:=ff_TQP20;
     if flag_TQP21 then
        Cds.FieldValues['TQP21']:=ff_TQP21;
     if flag_TQP22 then
        Cds.FieldValues['TQP22']:=ff_TQP22;
     if flag_TQP23 then
        Cds.FieldValues['TQP23']:=ff_TQP23;
     if flag_TQP24 then
        Cds.FieldValues['TQP24']:=ff_TQP24;
     if flag_TQP25 then
        Cds.FieldValues['TQP25']:=ff_TQP25;
     if flag_TQPACTI then
        Cds.FieldValues['TQPACTI']:=ff_TQPACTI;
     if flag_TQPUSER then
        Cds.FieldValues['TQPUSER']:=ff_TQPUSER;
     if flag_TQPGRUP then
        Cds.FieldValues['TQPGRUP']:=ff_TQPGRUP;
     if flag_TQPMODU then
        Cds.FieldValues['TQPMODU']:=ff_TQPMODU;
     if flag_TQPDATE then
        Cds.FieldValues['TQPDATE']:=ff_TQPDATE;
     if flag_TQPUD01 then
        Cds.FieldValues['TQPUD01']:=ff_TQPUD01;
     if flag_TQPUD02 then
        Cds.FieldValues['TQPUD02']:=ff_TQPUD02;
     if flag_TQPUD03 then
        Cds.FieldValues['TQPUD03']:=ff_TQPUD03;
     if flag_TQPUD04 then
        Cds.FieldValues['TQPUD04']:=ff_TQPUD04;
     if flag_TQPUD05 then
        Cds.FieldValues['TQPUD05']:=ff_TQPUD05;
     if flag_TQPUD06 then
        Cds.FieldValues['TQPUD06']:=ff_TQPUD06;
     if flag_TQPUD07 then
        Cds.FieldValues['TQPUD07']:=ff_TQPUD07;
     if flag_TQPUD08 then
        Cds.FieldValues['TQPUD08']:=ff_TQPUD08;
     if flag_TQPUD09 then
        Cds.FieldValues['TQPUD09']:=ff_TQPUD09;
     if flag_TQPUD10 then
        Cds.FieldValues['TQPUD10']:=ff_TQPUD10;
     if flag_TQPUD11 then
        Cds.FieldValues['TQPUD11']:=ff_TQPUD11;
     if flag_TQPUD12 then
        Cds.FieldValues['TQPUD12']:=ff_TQPUD12;
     if flag_TQPUD13 then
        Cds.FieldValues['TQPUD13']:=ff_TQPUD13;
     if flag_TQPUD14 then
        Cds.FieldValues['TQPUD14']:=ff_TQPUD14;
     if flag_TQPUD15 then
        Cds.FieldValues['TQPUD15']:=ff_TQPUD15;
     if flag_TQPPLANT then
        Cds.FieldValues['TQPPLANT']:=ff_TQPPLANT;
     if flag_TQPLEGAL then
        Cds.FieldValues['TQPLEGAL']:=ff_TQPLEGAL;
     if flag_TQPORIU then
        Cds.FieldValues['TQPORIU']:=ff_TQPORIU;
     if flag_TQPORIG then
        Cds.FieldValues['TQPORIG']:=ff_TQPORIG;
     Cds.Post;
  except
     Cds.Free;
     Result:=false;
     exit;
  end;
  Result:=DBA.UpdateRecord('TQP_FILE',Condition,Cds,True);
  Cds.Free;
end;

//
// 删除对象实例所对应的数据库记录...
function TTQP_FILE.DeleteFromDB(DBA: TRemoteUnidac; Condition: String): boolean;
var
   SqlCommand,tmpstr: String;
begin
  if Condition='' then
     SqlCommand:='DELETE FROM TQP_FILE'
  else
     SqlCommand:='DELETE FROM TQP_FILE WHERE '+condition;
  result:=DBA.ExecuteSQL(SqlCommand,false,tmpstr);
end;

//
// 判定属性是否null的函数...
function TTQP_FILE.PropIsNull(PropName: string): boolean;
begin
  result:=true;
  if StrIComp(PChar(PropName),PChar('TQP01'))=0 then
     begin
        result:=not flag_TQP01;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP02'))=0 then
     begin
        result:=not flag_TQP02;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP03'))=0 then
     begin
        result:=not flag_TQP03;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP04'))=0 then
     begin
        result:=not flag_TQP04;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP05'))=0 then
     begin
        result:=not flag_TQP05;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP06'))=0 then
     begin
        result:=not flag_TQP06;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP07'))=0 then
     begin
        result:=not flag_TQP07;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP08'))=0 then
     begin
        result:=not flag_TQP08;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP09'))=0 then
     begin
        result:=not flag_TQP09;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP10'))=0 then
     begin
        result:=not flag_TQP10;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP11'))=0 then
     begin
        result:=not flag_TQP11;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP12'))=0 then
     begin
        result:=not flag_TQP12;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP13'))=0 then
     begin
        result:=not flag_TQP13;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP14'))=0 then
     begin
        result:=not flag_TQP14;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP15'))=0 then
     begin
        result:=not flag_TQP15;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP16'))=0 then
     begin
        result:=not flag_TQP16;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP17'))=0 then
     begin
        result:=not flag_TQP17;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP18'))=0 then
     begin
        result:=not flag_TQP18;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP19'))=0 then
     begin
        result:=not flag_TQP19;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP20'))=0 then
     begin
        result:=not flag_TQP20;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP21'))=0 then
     begin
        result:=not flag_TQP21;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP22'))=0 then
     begin
        result:=not flag_TQP22;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP23'))=0 then
     begin
        result:=not flag_TQP23;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP24'))=0 then
     begin
        result:=not flag_TQP24;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP25'))=0 then
     begin
        result:=not flag_TQP25;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPACTI'))=0 then
     begin
        result:=not flag_TQPACTI;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUSER'))=0 then
     begin
        result:=not flag_TQPUSER;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPGRUP'))=0 then
     begin
        result:=not flag_TQPGRUP;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPMODU'))=0 then
     begin
        result:=not flag_TQPMODU;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPDATE'))=0 then
     begin
        result:=not flag_TQPDATE;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD01'))=0 then
     begin
        result:=not flag_TQPUD01;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD02'))=0 then
     begin
        result:=not flag_TQPUD02;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD03'))=0 then
     begin
        result:=not flag_TQPUD03;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD04'))=0 then
     begin
        result:=not flag_TQPUD04;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD05'))=0 then
     begin
        result:=not flag_TQPUD05;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD06'))=0 then
     begin
        result:=not flag_TQPUD06;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD07'))=0 then
     begin
        result:=not flag_TQPUD07;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD08'))=0 then
     begin
        result:=not flag_TQPUD08;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD09'))=0 then
     begin
        result:=not flag_TQPUD09;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD10'))=0 then
     begin
        result:=not flag_TQPUD10;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD11'))=0 then
     begin
        result:=not flag_TQPUD11;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD12'))=0 then
     begin
        result:=not flag_TQPUD12;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD13'))=0 then
     begin
        result:=not flag_TQPUD13;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD14'))=0 then
     begin
        result:=not flag_TQPUD14;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD15'))=0 then
     begin
        result:=not flag_TQPUD15;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPPLANT'))=0 then
     begin
        result:=not flag_TQPPLANT;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPLEGAL'))=0 then
     begin
        result:=not flag_TQPLEGAL;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPORIU'))=0 then
     begin
        result:=not flag_TQPORIU;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPORIG'))=0 then
     begin
        result:=not flag_TQPORIG;
        exit;
     end;
end;

//
// 设置某属性的值为null...
function TTQP_FILE.SetPropNull(PropName: string): boolean;
begin
  result:=false;
  if StrIComp(PChar(PropName),PChar('TQP01'))=0 then
     begin
        ff_TQP01:='';
        flag_TQP01:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP02'))=0 then
     begin
        ff_TQP02:='';
        flag_TQP02:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP03'))=0 then
     begin
        ff_TQP03:='';
        flag_TQP03:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP04'))=0 then
     begin
        ff_TQP04:='';
        flag_TQP04:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP05'))=0 then
     begin
        ff_TQP05:='';
        flag_TQP05:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP06'))=0 then
     begin
        ff_TQP06:=0;
        flag_TQP06:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP07'))=0 then
     begin
        ff_TQP07:=0;
        flag_TQP07:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP08'))=0 then
     begin
        ff_TQP08:='';
        flag_TQP08:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP09'))=0 then
     begin
        ff_TQP09:='';
        flag_TQP09:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP10'))=0 then
     begin
        ff_TQP10:='';
        flag_TQP10:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP11'))=0 then
     begin
        ff_TQP11:=0.0;
        flag_TQP11:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP12'))=0 then
     begin
        ff_TQP12:='';
        flag_TQP12:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP13'))=0 then
     begin
        ff_TQP13:='';
        flag_TQP13:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP14'))=0 then
     begin
        ff_TQP14:=0;
        flag_TQP14:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP15'))=0 then
     begin
        ff_TQP15:=0;
        flag_TQP15:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP16'))=0 then
     begin
        ff_TQP16:=0;
        flag_TQP16:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP17'))=0 then
     begin
        ff_TQP17:='';
        flag_TQP17:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP18'))=0 then
     begin
        ff_TQP18:=0.0;
        flag_TQP18:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP19'))=0 then
     begin
        ff_TQP19:='';
        flag_TQP19:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP20'))=0 then
     begin
        ff_TQP20:='';
        flag_TQP20:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP21'))=0 then
     begin
        ff_TQP21:='';
        flag_TQP21:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP22'))=0 then
     begin
        ff_TQP22:='';
        flag_TQP22:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP23'))=0 then
     begin
        ff_TQP23:=0.0;
        flag_TQP23:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP24'))=0 then
     begin
        ff_TQP24:=0;
        flag_TQP24:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQP25'))=0 then
     begin
        ff_TQP25:='';
        flag_TQP25:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPACTI'))=0 then
     begin
        ff_TQPACTI:='';
        flag_TQPACTI:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUSER'))=0 then
     begin
        ff_TQPUSER:='';
        flag_TQPUSER:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPGRUP'))=0 then
     begin
        ff_TQPGRUP:='';
        flag_TQPGRUP:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPMODU'))=0 then
     begin
        ff_TQPMODU:='';
        flag_TQPMODU:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPDATE'))=0 then
     begin
        ff_TQPDATE:=0;
        flag_TQPDATE:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD01'))=0 then
     begin
        ff_TQPUD01:='';
        flag_TQPUD01:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD02'))=0 then
     begin
        ff_TQPUD02:='';
        flag_TQPUD02:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD03'))=0 then
     begin
        ff_TQPUD03:='';
        flag_TQPUD03:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD04'))=0 then
     begin
        ff_TQPUD04:='';
        flag_TQPUD04:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD05'))=0 then
     begin
        ff_TQPUD05:='';
        flag_TQPUD05:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD06'))=0 then
     begin
        ff_TQPUD06:='';
        flag_TQPUD06:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD07'))=0 then
     begin
        ff_TQPUD07:=0.0;
        flag_TQPUD07:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD08'))=0 then
     begin
        ff_TQPUD08:=0.0;
        flag_TQPUD08:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD09'))=0 then
     begin
        ff_TQPUD09:=0.0;
        flag_TQPUD09:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD10'))=0 then
     begin
        ff_TQPUD10:=0;
        flag_TQPUD10:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD11'))=0 then
     begin
        ff_TQPUD11:=0;
        flag_TQPUD11:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD12'))=0 then
     begin
        ff_TQPUD12:=0;
        flag_TQPUD12:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD13'))=0 then
     begin
        ff_TQPUD13:=0;
        flag_TQPUD13:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD14'))=0 then
     begin
        ff_TQPUD14:=0;
        flag_TQPUD14:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPUD15'))=0 then
     begin
        ff_TQPUD15:=0;
        flag_TQPUD15:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPPLANT'))=0 then
     begin
        ff_TQPPLANT:='';
        flag_TQPPLANT:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPLEGAL'))=0 then
     begin
        ff_TQPLEGAL:='';
        flag_TQPLEGAL:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPORIU'))=0 then
     begin
        ff_TQPORIU:='';
        flag_TQPORIU:=false;
        result:=true;
        exit;
     end;
  if StrIComp(PChar(PropName),PChar('TQPORIG'))=0 then
     begin
        ff_TQPORIG:='';
        flag_TQPORIG:=false;
        result:=true;
        exit;
     end;
end;

//
// 设置某属性的值为null...
function TTQP_FILE.CloneTo(var NewObj: TTQP_FILE): boolean;
begin
   NewObj.f_TQP01:=ff_TQP01;
   NewObj.f_TQP02:=ff_TQP02;
   NewObj.f_TQP03:=ff_TQP03;
   NewObj.f_TQP04:=ff_TQP04;
   NewObj.f_TQP05:=ff_TQP05;
   NewObj.f_TQP06:=ff_TQP06;
   NewObj.f_TQP07:=ff_TQP07;
   NewObj.f_TQP08:=ff_TQP08;
   NewObj.f_TQP09:=ff_TQP09;
   NewObj.f_TQP10:=ff_TQP10;
   NewObj.f_TQP11:=ff_TQP11;
   NewObj.f_TQP12:=ff_TQP12;
   NewObj.f_TQP13:=ff_TQP13;
   NewObj.f_TQP14:=ff_TQP14;
   NewObj.f_TQP15:=ff_TQP15;
   NewObj.f_TQP16:=ff_TQP16;
   NewObj.f_TQP17:=ff_TQP17;
   NewObj.f_TQP18:=ff_TQP18;
   NewObj.f_TQP19:=ff_TQP19;
   NewObj.f_TQP20:=ff_TQP20;
   NewObj.f_TQP21:=ff_TQP21;
   NewObj.f_TQP22:=ff_TQP22;
   NewObj.f_TQP23:=ff_TQP23;
   NewObj.f_TQP24:=ff_TQP24;
   NewObj.f_TQP25:=ff_TQP25;
   NewObj.f_TQPACTI:=ff_TQPACTI;
   NewObj.f_TQPUSER:=ff_TQPUSER;
   NewObj.f_TQPGRUP:=ff_TQPGRUP;
   NewObj.f_TQPMODU:=ff_TQPMODU;
   NewObj.f_TQPDATE:=ff_TQPDATE;
   NewObj.f_TQPUD01:=ff_TQPUD01;
   NewObj.f_TQPUD02:=ff_TQPUD02;
   NewObj.f_TQPUD03:=ff_TQPUD03;
   NewObj.f_TQPUD04:=ff_TQPUD04;
   NewObj.f_TQPUD05:=ff_TQPUD05;
   NewObj.f_TQPUD06:=ff_TQPUD06;
   NewObj.f_TQPUD07:=ff_TQPUD07;
   NewObj.f_TQPUD08:=ff_TQPUD08;
   NewObj.f_TQPUD09:=ff_TQPUD09;
   NewObj.f_TQPUD10:=ff_TQPUD10;
   NewObj.f_TQPUD11:=ff_TQPUD11;
   NewObj.f_TQPUD12:=ff_TQPUD12;
   NewObj.f_TQPUD13:=ff_TQPUD13;
   NewObj.f_TQPUD14:=ff_TQPUD14;
   NewObj.f_TQPUD15:=ff_TQPUD15;
   NewObj.f_TQPPLANT:=ff_TQPPLANT;
   NewObj.f_TQPLEGAL:=ff_TQPLEGAL;
   NewObj.f_TQPORIU:=ff_TQPORIU;
   NewObj.f_TQPORIG:=ff_TQPORIG;
   Result:=true;
end;


{ TTQP_FILEList }

//
// 创建列表对象实例...
constructor TTQP_FILEList.Create;
begin
  inherited Create;
end;

//
// 释放列表对象实例...
destructor TTQP_FILEList.Destroy;
var
  I: Integer;
  Obj: TTQP_FILE;
begin
  for I := 0 to fCount - 1 do
     begin
        Obj := TTQP_FILE(fList[I]);
        Obj.Free;
     end;
  inherited;
end;

//
// 清除对象...
procedure TTQP_FILEList.Clear;
begin
  fcount:=0;
  setlength(flist,0);
end;

//
// 增加对象...
procedure TTQP_FILEList.add(Obj: TTQP_FILE);
begin
   inc(fCount);
   setlength(flist,fcount);
   flist[fcount-1]:=obj;
end;

//
// 删除对象...
procedure TTQP_FILEList.Delete(Index: Integer);
var
   i: integer;
begin
   FreeAndNil(flist[Index]);
   for i:=index to fcount-2 do
      flist[i]:=flist[i+1];
   dec(fCount);
   setlength(flist,fcount);
end;

//
// 取一个元素...
function TTQP_FILEList.Get(Index: Integer): Pointer;
begin
  Result := FList[Index];
end;

//
// Put方法...
procedure TTQP_FILEList.Put(Index: Integer; Item: Pointer);
begin
  if Item <> FList[Index] then
    FList[Index] := Item;
end;

//
// 从远程数据表读取记录并转换为列表对象实例...
class function TTQP_FILEList.ReadFromDB(DBA: TRemoteUnidac; Condition: String = ''; 
      OrderBy: String = ''; SelectOption: string=''): TTQP_FILEList;
var
  SqlCommand: String;
  Cds: TClientDataset;
  TmpObj: TTQP_FILE;
begin
  Cds:=TClientDataset.Create(nil);
  if Condition='' then
     begin
        SqlCommand := 'SELECT '+SelectOption+' '
                    +'TQP01,'
                    +'TQP02,'
                    +'TQP03,'
                    +'TQP04,'
                    +'TQP05,'
                    +'TQP06,'
                    +'TQP07,'
                    +'TQP08,'
                    +'TQP09,'
                    +'TQP10,'
                    +'TQP11,'
                    +'TQP12,'
                    +'TQP13,'
                    +'TQP14,'
                    +'TQP15,'
                    +'TQP16,'
                    +'TQP17,'
                    +'TQP18,'
                    +'TQP19,'
                    +'TQP20,'
                    +'TQP21,'
                    +'TQP22,'
                    +'TQP23,'
                    +'TQP24,'
                    +'TQP25,'
                    +'TQPACTI,'
                    +'TQPUSER,'
                    +'TQPGRUP,'
                    +'TQPMODU,'
                    +'TQPDATE,'
                    +'TQPUD01,'
                    +'TQPUD02,'
                    +'TQPUD03,'
                    +'TQPUD04,'
                    +'TQPUD05,'
                    +'TQPUD06,'
                    +'TQPUD07,'
                    +'TQPUD08,'
                    +'TQPUD09,'
                    +'TQPUD10,'
                    +'TQPUD11,'
                    +'TQPUD12,'
                    +'TQPUD13,'
                    +'TQPUD14,'
                    +'TQPUD15,'
                    +'TQPPLANT,'
                    +'TQPLEGAL,'
                    +'TQPORIU,'
                    +'TQPORIG'
                    +' FROM TQP_FILE'
     end
  else
     begin
        SqlCommand := 'SELECT '+SelectOption+' '
                    +'TQP01,'
                    +'TQP02,'
                    +'TQP03,'
                    +'TQP04,'
                    +'TQP05,'
                    +'TQP06,'
                    +'TQP07,'
                    +'TQP08,'
                    +'TQP09,'
                    +'TQP10,'
                    +'TQP11,'
                    +'TQP12,'
                    +'TQP13,'
                    +'TQP14,'
                    +'TQP15,'
                    +'TQP16,'
                    +'TQP17,'
                    +'TQP18,'
                    +'TQP19,'
                    +'TQP20,'
                    +'TQP21,'
                    +'TQP22,'
                    +'TQP23,'
                    +'TQP24,'
                    +'TQP25,'
                    +'TQPACTI,'
                    +'TQPUSER,'
                    +'TQPGRUP,'
                    +'TQPMODU,'
                    +'TQPDATE,'
                    +'TQPUD01,'
                    +'TQPUD02,'
                    +'TQPUD03,'
                    +'TQPUD04,'
                    +'TQPUD05,'
                    +'TQPUD06,'
                    +'TQPUD07,'
                    +'TQPUD08,'
                    +'TQPUD09,'
                    +'TQPUD10,'
                    +'TQPUD11,'
                    +'TQPUD12,'
                    +'TQPUD13,'
                    +'TQPUD14,'
                    +'TQPUD15,'
                    +'TQPPLANT,'
                    +'TQPLEGAL,'
                    +'TQPORIU,'
                    +'TQPORIG'
                    +' FROM TQP_FILE WHERE '+Condition;
     end;
  if Orderby<>'' then
     SqlCommand:=SqlCommand+' ORDER BY '+Orderby;
  if not DBA.ReadDataset(SqlCommand,Cds) then
     begin
        Result:=nil;
        Cds.Free;
        exit;
     end;
  Result := TTQP_FILEList.Create;
  Cds.First;
  while not Cds.Eof do
     begin
        TmpObj := TTQP_FILE.ReadFromCDS(Cds);
        Result.Add(TmpObj);
        Cds.Next;
     end;
  Cds.Free;
end;

//
// 将本地Cds中的记录集转换为列表对象实例...
class function TTQP_FILEList.ReadFromCDS(Cds: TDataset): TTQP_FILEList;
var
  TmpObj: TTQP_FILE;
begin
  Result := TTQP_FILEList.Create;
  Cds.First;
  while not Cds.Eof do
     begin
        TmpObj := TTQP_FILE.ReadFromCDS(Cds);
        Result.Add(TmpObj);
        Cds.Next;
     end;
end;

//
// 将列表对象包含的一批对象保存为Cds中的若干记录...
function TTQP_FILEList.SaveToCds(Cds: TDataset): boolean;
var
  TmpObj: TTQP_FILE;
  i: integer;
begin
  Result:=true;
  for i:=0 to Self.Count-1 do
     begin
        TmpObj:=Self.items[i];
        try
           Cds.Append;
           if TmpObj.TQP01_Updated then
              Cds.FieldValues['TQP01']:=TmpObj.f_TQP01;
           if TmpObj.TQP02_Updated then
              Cds.FieldValues['TQP02']:=TmpObj.f_TQP02;
           if TmpObj.TQP03_Updated then
              Cds.FieldValues['TQP03']:=TmpObj.f_TQP03;
           if TmpObj.TQP04_Updated then
              Cds.FieldValues['TQP04']:=TmpObj.f_TQP04;
           if TmpObj.TQP05_Updated then
              Cds.FieldValues['TQP05']:=TmpObj.f_TQP05;
           if TmpObj.TQP06_Updated then
              Cds.FieldValues['TQP06']:=TmpObj.f_TQP06;
           if TmpObj.TQP07_Updated then
              Cds.FieldValues['TQP07']:=TmpObj.f_TQP07;
           if TmpObj.TQP08_Updated then
              Cds.FieldValues['TQP08']:=TmpObj.f_TQP08;
           if TmpObj.TQP09_Updated then
              Cds.FieldValues['TQP09']:=TmpObj.f_TQP09;
           if TmpObj.TQP10_Updated then
              Cds.FieldValues['TQP10']:=TmpObj.f_TQP10;
           if TmpObj.TQP11_Updated then
              Cds.FieldValues['TQP11']:=TmpObj.f_TQP11;
           if TmpObj.TQP12_Updated then
              Cds.FieldValues['TQP12']:=TmpObj.f_TQP12;
           if TmpObj.TQP13_Updated then
              Cds.FieldValues['TQP13']:=TmpObj.f_TQP13;
           if TmpObj.TQP14_Updated then
              Cds.FieldValues['TQP14']:=TmpObj.f_TQP14;
           if TmpObj.TQP15_Updated then
              Cds.FieldValues['TQP15']:=TmpObj.f_TQP15;
           if TmpObj.TQP16_Updated then
              Cds.FieldValues['TQP16']:=TmpObj.f_TQP16;
           if TmpObj.TQP17_Updated then
              Cds.FieldValues['TQP17']:=TmpObj.f_TQP17;
           if TmpObj.TQP18_Updated then
              Cds.FieldValues['TQP18']:=TmpObj.f_TQP18;
           if TmpObj.TQP19_Updated then
              Cds.FieldValues['TQP19']:=TmpObj.f_TQP19;
           if TmpObj.TQP20_Updated then
              Cds.FieldValues['TQP20']:=TmpObj.f_TQP20;
           if TmpObj.TQP21_Updated then
              Cds.FieldValues['TQP21']:=TmpObj.f_TQP21;
           if TmpObj.TQP22_Updated then
              Cds.FieldValues['TQP22']:=TmpObj.f_TQP22;
           if TmpObj.TQP23_Updated then
              Cds.FieldValues['TQP23']:=TmpObj.f_TQP23;
           if TmpObj.TQP24_Updated then
              Cds.FieldValues['TQP24']:=TmpObj.f_TQP24;
           if TmpObj.TQP25_Updated then
              Cds.FieldValues['TQP25']:=TmpObj.f_TQP25;
           if TmpObj.TQPACTI_Updated then
              Cds.FieldValues['TQPACTI']:=TmpObj.f_TQPACTI;
           if TmpObj.TQPUSER_Updated then
              Cds.FieldValues['TQPUSER']:=TmpObj.f_TQPUSER;
           if TmpObj.TQPGRUP_Updated then
              Cds.FieldValues['TQPGRUP']:=TmpObj.f_TQPGRUP;
           if TmpObj.TQPMODU_Updated then
              Cds.FieldValues['TQPMODU']:=TmpObj.f_TQPMODU;
           if TmpObj.TQPDATE_Updated then
              Cds.FieldValues['TQPDATE']:=TmpObj.f_TQPDATE;
           if TmpObj.TQPUD01_Updated then
              Cds.FieldValues['TQPUD01']:=TmpObj.f_TQPUD01;
           if TmpObj.TQPUD02_Updated then
              Cds.FieldValues['TQPUD02']:=TmpObj.f_TQPUD02;
           if TmpObj.TQPUD03_Updated then
              Cds.FieldValues['TQPUD03']:=TmpObj.f_TQPUD03;
           if TmpObj.TQPUD04_Updated then
              Cds.FieldValues['TQPUD04']:=TmpObj.f_TQPUD04;
           if TmpObj.TQPUD05_Updated then
              Cds.FieldValues['TQPUD05']:=TmpObj.f_TQPUD05;
           if TmpObj.TQPUD06_Updated then
              Cds.FieldValues['TQPUD06']:=TmpObj.f_TQPUD06;
           if TmpObj.TQPUD07_Updated then
              Cds.FieldValues['TQPUD07']:=TmpObj.f_TQPUD07;
           if TmpObj.TQPUD08_Updated then
              Cds.FieldValues['TQPUD08']:=TmpObj.f_TQPUD08;
           if TmpObj.TQPUD09_Updated then
              Cds.FieldValues['TQPUD09']:=TmpObj.f_TQPUD09;
           if TmpObj.TQPUD10_Updated then
              Cds.FieldValues['TQPUD10']:=TmpObj.f_TQPUD10;
           if TmpObj.TQPUD11_Updated then
              Cds.FieldValues['TQPUD11']:=TmpObj.f_TQPUD11;
           if TmpObj.TQPUD12_Updated then
              Cds.FieldValues['TQPUD12']:=TmpObj.f_TQPUD12;
           if TmpObj.TQPUD13_Updated then
              Cds.FieldValues['TQPUD13']:=TmpObj.f_TQPUD13;
           if TmpObj.TQPUD14_Updated then
              Cds.FieldValues['TQPUD14']:=TmpObj.f_TQPUD14;
           if TmpObj.TQPUD15_Updated then
              Cds.FieldValues['TQPUD15']:=TmpObj.f_TQPUD15;
           if TmpObj.TQPPLANT_Updated then
              Cds.FieldValues['TQPPLANT']:=TmpObj.f_TQPPLANT;
           if TmpObj.TQPLEGAL_Updated then
              Cds.FieldValues['TQPLEGAL']:=TmpObj.f_TQPLEGAL;
           if TmpObj.TQPORIU_Updated then
              Cds.FieldValues['TQPORIU']:=TmpObj.f_TQPORIU;
           if TmpObj.TQPORIG_Updated then
              Cds.FieldValues['TQPORIG']:=TmpObj.f_TQPORIG;
           Cds.Post;
        except
           Result:=false;
        end;
        if not Result then
           break;
     end;
end;

//
// 将对象实例的属性写入到远程数据库记录...
function TTQP_FILEList.InsertToDB(DBA: TRemoteUnidac): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQP_FILE.CreateCds;
  Result:=Self.SaveToCds(Cds);
  if Result then
     Result:=DBA.WriteDataset(Cds,'TQP_FILE');
  FreeAndNil(Cds);
end;

//
// 将对象实例的属性更新到数据库记录...
function TTQP_FILEList.UpdateToDB(DBA: TRemoteUnidac; KeyFieldList: string; Condition: String): boolean;
var
  Cds: TClientDataset;
begin
  Cds:=TTQP_FILE.CreateCds;
  Result:=Self.SaveToCds(Cds);
  if Result then
     Result:=DBA.UpdateDataset(Cds,'TQP_FILE',KeyFieldList,Condition);
  FreeAndNil(Cds);
end;

//
// 删除对象实例所对应的数据库记录...
function TTQP_FILEList.DeleteFromDB(DBA: TRemoteUnidac; Condition: String): boolean;
var
   SqlCommand,tmpstr: String;
begin
  SqlCommand:='DELETE FROM TQP_FILE WHERE '+condition;
  result:=DBA.ExecuteSQL(SqlCommand,false,tmpstr);
end;

//
// 将列表对象包含的一批对象保存到外部文件中...
function TTQP_FILEList.SaveToFile(FileName: String; DataFormat: TDataPacketFormat): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQP_FILE;
  i: integer;
  ok: boolean;
begin
  Cds:=TTQP_FILE.CreateCds;
  ok:=true;
  for i:=0 to Self.Count-1 do
     begin
        TmpObj:=Self.items[i];
        try
           Cds.Append;
           if TmpObj.TQP01_Updated then
              Cds.FieldValues['TQP01']:=TmpObj.f_TQP01;
           if TmpObj.TQP02_Updated then
              Cds.FieldValues['TQP02']:=TmpObj.f_TQP02;
           if TmpObj.TQP03_Updated then
              Cds.FieldValues['TQP03']:=TmpObj.f_TQP03;
           if TmpObj.TQP04_Updated then
              Cds.FieldValues['TQP04']:=TmpObj.f_TQP04;
           if TmpObj.TQP05_Updated then
              Cds.FieldValues['TQP05']:=TmpObj.f_TQP05;
           if TmpObj.TQP06_Updated then
              Cds.FieldValues['TQP06']:=TmpObj.f_TQP06;
           if TmpObj.TQP07_Updated then
              Cds.FieldValues['TQP07']:=TmpObj.f_TQP07;
           if TmpObj.TQP08_Updated then
              Cds.FieldValues['TQP08']:=TmpObj.f_TQP08;
           if TmpObj.TQP09_Updated then
              Cds.FieldValues['TQP09']:=TmpObj.f_TQP09;
           if TmpObj.TQP10_Updated then
              Cds.FieldValues['TQP10']:=TmpObj.f_TQP10;
           if TmpObj.TQP11_Updated then
              Cds.FieldValues['TQP11']:=TmpObj.f_TQP11;
           if TmpObj.TQP12_Updated then
              Cds.FieldValues['TQP12']:=TmpObj.f_TQP12;
           if TmpObj.TQP13_Updated then
              Cds.FieldValues['TQP13']:=TmpObj.f_TQP13;
           if TmpObj.TQP14_Updated then
              Cds.FieldValues['TQP14']:=TmpObj.f_TQP14;
           if TmpObj.TQP15_Updated then
              Cds.FieldValues['TQP15']:=TmpObj.f_TQP15;
           if TmpObj.TQP16_Updated then
              Cds.FieldValues['TQP16']:=TmpObj.f_TQP16;
           if TmpObj.TQP17_Updated then
              Cds.FieldValues['TQP17']:=TmpObj.f_TQP17;
           if TmpObj.TQP18_Updated then
              Cds.FieldValues['TQP18']:=TmpObj.f_TQP18;
           if TmpObj.TQP19_Updated then
              Cds.FieldValues['TQP19']:=TmpObj.f_TQP19;
           if TmpObj.TQP20_Updated then
              Cds.FieldValues['TQP20']:=TmpObj.f_TQP20;
           if TmpObj.TQP21_Updated then
              Cds.FieldValues['TQP21']:=TmpObj.f_TQP21;
           if TmpObj.TQP22_Updated then
              Cds.FieldValues['TQP22']:=TmpObj.f_TQP22;
           if TmpObj.TQP23_Updated then
              Cds.FieldValues['TQP23']:=TmpObj.f_TQP23;
           if TmpObj.TQP24_Updated then
              Cds.FieldValues['TQP24']:=TmpObj.f_TQP24;
           if TmpObj.TQP25_Updated then
              Cds.FieldValues['TQP25']:=TmpObj.f_TQP25;
           if TmpObj.TQPACTI_Updated then
              Cds.FieldValues['TQPACTI']:=TmpObj.f_TQPACTI;
           if TmpObj.TQPUSER_Updated then
              Cds.FieldValues['TQPUSER']:=TmpObj.f_TQPUSER;
           if TmpObj.TQPGRUP_Updated then
              Cds.FieldValues['TQPGRUP']:=TmpObj.f_TQPGRUP;
           if TmpObj.TQPMODU_Updated then
              Cds.FieldValues['TQPMODU']:=TmpObj.f_TQPMODU;
           if TmpObj.TQPDATE_Updated then
              Cds.FieldValues['TQPDATE']:=TmpObj.f_TQPDATE;
           if TmpObj.TQPUD01_Updated then
              Cds.FieldValues['TQPUD01']:=TmpObj.f_TQPUD01;
           if TmpObj.TQPUD02_Updated then
              Cds.FieldValues['TQPUD02']:=TmpObj.f_TQPUD02;
           if TmpObj.TQPUD03_Updated then
              Cds.FieldValues['TQPUD03']:=TmpObj.f_TQPUD03;
           if TmpObj.TQPUD04_Updated then
              Cds.FieldValues['TQPUD04']:=TmpObj.f_TQPUD04;
           if TmpObj.TQPUD05_Updated then
              Cds.FieldValues['TQPUD05']:=TmpObj.f_TQPUD05;
           if TmpObj.TQPUD06_Updated then
              Cds.FieldValues['TQPUD06']:=TmpObj.f_TQPUD06;
           if TmpObj.TQPUD07_Updated then
              Cds.FieldValues['TQPUD07']:=TmpObj.f_TQPUD07;
           if TmpObj.TQPUD08_Updated then
              Cds.FieldValues['TQPUD08']:=TmpObj.f_TQPUD08;
           if TmpObj.TQPUD09_Updated then
              Cds.FieldValues['TQPUD09']:=TmpObj.f_TQPUD09;
           if TmpObj.TQPUD10_Updated then
              Cds.FieldValues['TQPUD10']:=TmpObj.f_TQPUD10;
           if TmpObj.TQPUD11_Updated then
              Cds.FieldValues['TQPUD11']:=TmpObj.f_TQPUD11;
           if TmpObj.TQPUD12_Updated then
              Cds.FieldValues['TQPUD12']:=TmpObj.f_TQPUD12;
           if TmpObj.TQPUD13_Updated then
              Cds.FieldValues['TQPUD13']:=TmpObj.f_TQPUD13;
           if TmpObj.TQPUD14_Updated then
              Cds.FieldValues['TQPUD14']:=TmpObj.f_TQPUD14;
           if TmpObj.TQPUD15_Updated then
              Cds.FieldValues['TQPUD15']:=TmpObj.f_TQPUD15;
           if TmpObj.TQPPLANT_Updated then
              Cds.FieldValues['TQPPLANT']:=TmpObj.f_TQPPLANT;
           if TmpObj.TQPLEGAL_Updated then
              Cds.FieldValues['TQPLEGAL']:=TmpObj.f_TQPLEGAL;
           if TmpObj.TQPORIU_Updated then
              Cds.FieldValues['TQPORIU']:=TmpObj.f_TQPORIU;
           if TmpObj.TQPORIG_Updated then
              Cds.FieldValues['TQPORIG']:=TmpObj.f_TQPORIG;
           Cds.Post;
        except
           ok:=false;
        end;
        if not ok then
           break;
     end;
  if ok then
     begin
        try
           Cds.SaveToFile(String(FileName),DataFormat);
        except
           ok:=false;
        end;
     end;
  Cds.Free;
  result:=ok;
end;

//
// 从文件中装入数据，转换为列表对象中的一系列对象...
function TTQP_FILEList.LoadFromFile(FileName: String): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQP_FILE;
  ok: boolean;
begin
  self.Clear;
  Cds:=TClientDataset.Create(nil);
  try
     Cds.LoadFromFile(String(FileName));
     ok:=Cds.Active;
  except
     ok:=false;
  end;
  if not ok then
     begin
        cds.Free;
        result:=false;
        exit;
     end;
  cds.First;
  while not cds.Eof do
     begin
        TmpObj := TTQP_FILE.ReadFromCDS(Cds);
        Self.Add(TmpObj);
        Cds.Next;
     end;
  Cds.Free;
  result:=true;
end;

//
// 保存列表对象中所含数据到内存流对象...
function TTQP_FILEList.SaveToStream(aStream: TMemoryStream; DataFormat: TDataPacketFormat): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQP_FILE;
  i: integer;
  ok: boolean;
begin
  Cds:=TTQP_FILE.CreateCds;
  ok:=true;
  for i:=0 to Self.Count-1 do
     begin
        TmpObj:=Self.items[i];
        try
           Cds.Append;
           if TmpObj.TQP01_Updated then
              Cds.FieldValues['TQP01']:=TmpObj.f_TQP01;
           if TmpObj.TQP02_Updated then
              Cds.FieldValues['TQP02']:=TmpObj.f_TQP02;
           if TmpObj.TQP03_Updated then
              Cds.FieldValues['TQP03']:=TmpObj.f_TQP03;
           if TmpObj.TQP04_Updated then
              Cds.FieldValues['TQP04']:=TmpObj.f_TQP04;
           if TmpObj.TQP05_Updated then
              Cds.FieldValues['TQP05']:=TmpObj.f_TQP05;
           if TmpObj.TQP06_Updated then
              Cds.FieldValues['TQP06']:=TmpObj.f_TQP06;
           if TmpObj.TQP07_Updated then
              Cds.FieldValues['TQP07']:=TmpObj.f_TQP07;
           if TmpObj.TQP08_Updated then
              Cds.FieldValues['TQP08']:=TmpObj.f_TQP08;
           if TmpObj.TQP09_Updated then
              Cds.FieldValues['TQP09']:=TmpObj.f_TQP09;
           if TmpObj.TQP10_Updated then
              Cds.FieldValues['TQP10']:=TmpObj.f_TQP10;
           if TmpObj.TQP11_Updated then
              Cds.FieldValues['TQP11']:=TmpObj.f_TQP11;
           if TmpObj.TQP12_Updated then
              Cds.FieldValues['TQP12']:=TmpObj.f_TQP12;
           if TmpObj.TQP13_Updated then
              Cds.FieldValues['TQP13']:=TmpObj.f_TQP13;
           if TmpObj.TQP14_Updated then
              Cds.FieldValues['TQP14']:=TmpObj.f_TQP14;
           if TmpObj.TQP15_Updated then
              Cds.FieldValues['TQP15']:=TmpObj.f_TQP15;
           if TmpObj.TQP16_Updated then
              Cds.FieldValues['TQP16']:=TmpObj.f_TQP16;
           if TmpObj.TQP17_Updated then
              Cds.FieldValues['TQP17']:=TmpObj.f_TQP17;
           if TmpObj.TQP18_Updated then
              Cds.FieldValues['TQP18']:=TmpObj.f_TQP18;
           if TmpObj.TQP19_Updated then
              Cds.FieldValues['TQP19']:=TmpObj.f_TQP19;
           if TmpObj.TQP20_Updated then
              Cds.FieldValues['TQP20']:=TmpObj.f_TQP20;
           if TmpObj.TQP21_Updated then
              Cds.FieldValues['TQP21']:=TmpObj.f_TQP21;
           if TmpObj.TQP22_Updated then
              Cds.FieldValues['TQP22']:=TmpObj.f_TQP22;
           if TmpObj.TQP23_Updated then
              Cds.FieldValues['TQP23']:=TmpObj.f_TQP23;
           if TmpObj.TQP24_Updated then
              Cds.FieldValues['TQP24']:=TmpObj.f_TQP24;
           if TmpObj.TQP25_Updated then
              Cds.FieldValues['TQP25']:=TmpObj.f_TQP25;
           if TmpObj.TQPACTI_Updated then
              Cds.FieldValues['TQPACTI']:=TmpObj.f_TQPACTI;
           if TmpObj.TQPUSER_Updated then
              Cds.FieldValues['TQPUSER']:=TmpObj.f_TQPUSER;
           if TmpObj.TQPGRUP_Updated then
              Cds.FieldValues['TQPGRUP']:=TmpObj.f_TQPGRUP;
           if TmpObj.TQPMODU_Updated then
              Cds.FieldValues['TQPMODU']:=TmpObj.f_TQPMODU;
           if TmpObj.TQPDATE_Updated then
              Cds.FieldValues['TQPDATE']:=TmpObj.f_TQPDATE;
           if TmpObj.TQPUD01_Updated then
              Cds.FieldValues['TQPUD01']:=TmpObj.f_TQPUD01;
           if TmpObj.TQPUD02_Updated then
              Cds.FieldValues['TQPUD02']:=TmpObj.f_TQPUD02;
           if TmpObj.TQPUD03_Updated then
              Cds.FieldValues['TQPUD03']:=TmpObj.f_TQPUD03;
           if TmpObj.TQPUD04_Updated then
              Cds.FieldValues['TQPUD04']:=TmpObj.f_TQPUD04;
           if TmpObj.TQPUD05_Updated then
              Cds.FieldValues['TQPUD05']:=TmpObj.f_TQPUD05;
           if TmpObj.TQPUD06_Updated then
              Cds.FieldValues['TQPUD06']:=TmpObj.f_TQPUD06;
           if TmpObj.TQPUD07_Updated then
              Cds.FieldValues['TQPUD07']:=TmpObj.f_TQPUD07;
           if TmpObj.TQPUD08_Updated then
              Cds.FieldValues['TQPUD08']:=TmpObj.f_TQPUD08;
           if TmpObj.TQPUD09_Updated then
              Cds.FieldValues['TQPUD09']:=TmpObj.f_TQPUD09;
           if TmpObj.TQPUD10_Updated then
              Cds.FieldValues['TQPUD10']:=TmpObj.f_TQPUD10;
           if TmpObj.TQPUD11_Updated then
              Cds.FieldValues['TQPUD11']:=TmpObj.f_TQPUD11;
           if TmpObj.TQPUD12_Updated then
              Cds.FieldValues['TQPUD12']:=TmpObj.f_TQPUD12;
           if TmpObj.TQPUD13_Updated then
              Cds.FieldValues['TQPUD13']:=TmpObj.f_TQPUD13;
           if TmpObj.TQPUD14_Updated then
              Cds.FieldValues['TQPUD14']:=TmpObj.f_TQPUD14;
           if TmpObj.TQPUD15_Updated then
              Cds.FieldValues['TQPUD15']:=TmpObj.f_TQPUD15;
           if TmpObj.TQPPLANT_Updated then
              Cds.FieldValues['TQPPLANT']:=TmpObj.f_TQPPLANT;
           if TmpObj.TQPLEGAL_Updated then
              Cds.FieldValues['TQPLEGAL']:=TmpObj.f_TQPLEGAL;
           if TmpObj.TQPORIU_Updated then
              Cds.FieldValues['TQPORIU']:=TmpObj.f_TQPORIU;
           if TmpObj.TQPORIG_Updated then
              Cds.FieldValues['TQPORIG']:=TmpObj.f_TQPORIG;
           Cds.Post;
        except
           ok:=false;
        end;
        if not ok then
           break;
     end;
  if ok then
     begin
        try
           aStream.Clear;
           Cds.SaveToStream(aStream,DataFormat);
        except
           ok:=false;
        end;
     end;
  Cds.Free;
  result:=ok;
end;

//
// 从内存流对象装入数据到列表对象中...
function TTQP_FILEList.LoadFromStream(aStream: TMemoryStream): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQP_FILE;
  ok: boolean;
begin
  self.Clear;
  Cds:=TClientDataset.Create(nil);
  try
     aStream.Position:=0;
     Cds.LoadFromStream(aStream);
     ok:=Cds.Active;
  except
     ok:=false;
  end;
  if not ok then
     begin
        cds.Free;
        result:=false;
        exit;
     end;
  cds.First;
  while not cds.Eof do
     begin
        TmpObj := TTQP_FILE.ReadFromCDS(Cds);
        Self.Add(TmpObj);
        Cds.Next;
     end;
  Cds.Free;
  result:=true;
end;

{$IFDEF MSWINDOWS}
//
// 保存列表对象数据到数据包裹对象中...
function TTQP_FILEList.SaveToParcel(aParcel: TQBParcel; GoodsName: AnsiString): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQP_FILE;
  i: integer;
  ok: boolean;
begin
  Cds:=TTQP_FILE.CreateCds;
  ok:=true;
  for i:=0 to Self.Count-1 do
     begin
        TmpObj:=Self.items[i];
        try
           Cds.Append;
           if TmpObj.TQP01_Updated then
              Cds.FieldValues['TQP01']:=TmpObj.f_TQP01;
           if TmpObj.TQP02_Updated then
              Cds.FieldValues['TQP02']:=TmpObj.f_TQP02;
           if TmpObj.TQP03_Updated then
              Cds.FieldValues['TQP03']:=TmpObj.f_TQP03;
           if TmpObj.TQP04_Updated then
              Cds.FieldValues['TQP04']:=TmpObj.f_TQP04;
           if TmpObj.TQP05_Updated then
              Cds.FieldValues['TQP05']:=TmpObj.f_TQP05;
           if TmpObj.TQP06_Updated then
              Cds.FieldValues['TQP06']:=TmpObj.f_TQP06;
           if TmpObj.TQP07_Updated then
              Cds.FieldValues['TQP07']:=TmpObj.f_TQP07;
           if TmpObj.TQP08_Updated then
              Cds.FieldValues['TQP08']:=TmpObj.f_TQP08;
           if TmpObj.TQP09_Updated then
              Cds.FieldValues['TQP09']:=TmpObj.f_TQP09;
           if TmpObj.TQP10_Updated then
              Cds.FieldValues['TQP10']:=TmpObj.f_TQP10;
           if TmpObj.TQP11_Updated then
              Cds.FieldValues['TQP11']:=TmpObj.f_TQP11;
           if TmpObj.TQP12_Updated then
              Cds.FieldValues['TQP12']:=TmpObj.f_TQP12;
           if TmpObj.TQP13_Updated then
              Cds.FieldValues['TQP13']:=TmpObj.f_TQP13;
           if TmpObj.TQP14_Updated then
              Cds.FieldValues['TQP14']:=TmpObj.f_TQP14;
           if TmpObj.TQP15_Updated then
              Cds.FieldValues['TQP15']:=TmpObj.f_TQP15;
           if TmpObj.TQP16_Updated then
              Cds.FieldValues['TQP16']:=TmpObj.f_TQP16;
           if TmpObj.TQP17_Updated then
              Cds.FieldValues['TQP17']:=TmpObj.f_TQP17;
           if TmpObj.TQP18_Updated then
              Cds.FieldValues['TQP18']:=TmpObj.f_TQP18;
           if TmpObj.TQP19_Updated then
              Cds.FieldValues['TQP19']:=TmpObj.f_TQP19;
           if TmpObj.TQP20_Updated then
              Cds.FieldValues['TQP20']:=TmpObj.f_TQP20;
           if TmpObj.TQP21_Updated then
              Cds.FieldValues['TQP21']:=TmpObj.f_TQP21;
           if TmpObj.TQP22_Updated then
              Cds.FieldValues['TQP22']:=TmpObj.f_TQP22;
           if TmpObj.TQP23_Updated then
              Cds.FieldValues['TQP23']:=TmpObj.f_TQP23;
           if TmpObj.TQP24_Updated then
              Cds.FieldValues['TQP24']:=TmpObj.f_TQP24;
           if TmpObj.TQP25_Updated then
              Cds.FieldValues['TQP25']:=TmpObj.f_TQP25;
           if TmpObj.TQPACTI_Updated then
              Cds.FieldValues['TQPACTI']:=TmpObj.f_TQPACTI;
           if TmpObj.TQPUSER_Updated then
              Cds.FieldValues['TQPUSER']:=TmpObj.f_TQPUSER;
           if TmpObj.TQPGRUP_Updated then
              Cds.FieldValues['TQPGRUP']:=TmpObj.f_TQPGRUP;
           if TmpObj.TQPMODU_Updated then
              Cds.FieldValues['TQPMODU']:=TmpObj.f_TQPMODU;
           if TmpObj.TQPDATE_Updated then
              Cds.FieldValues['TQPDATE']:=TmpObj.f_TQPDATE;
           if TmpObj.TQPUD01_Updated then
              Cds.FieldValues['TQPUD01']:=TmpObj.f_TQPUD01;
           if TmpObj.TQPUD02_Updated then
              Cds.FieldValues['TQPUD02']:=TmpObj.f_TQPUD02;
           if TmpObj.TQPUD03_Updated then
              Cds.FieldValues['TQPUD03']:=TmpObj.f_TQPUD03;
           if TmpObj.TQPUD04_Updated then
              Cds.FieldValues['TQPUD04']:=TmpObj.f_TQPUD04;
           if TmpObj.TQPUD05_Updated then
              Cds.FieldValues['TQPUD05']:=TmpObj.f_TQPUD05;
           if TmpObj.TQPUD06_Updated then
              Cds.FieldValues['TQPUD06']:=TmpObj.f_TQPUD06;
           if TmpObj.TQPUD07_Updated then
              Cds.FieldValues['TQPUD07']:=TmpObj.f_TQPUD07;
           if TmpObj.TQPUD08_Updated then
              Cds.FieldValues['TQPUD08']:=TmpObj.f_TQPUD08;
           if TmpObj.TQPUD09_Updated then
              Cds.FieldValues['TQPUD09']:=TmpObj.f_TQPUD09;
           if TmpObj.TQPUD10_Updated then
              Cds.FieldValues['TQPUD10']:=TmpObj.f_TQPUD10;
           if TmpObj.TQPUD11_Updated then
              Cds.FieldValues['TQPUD11']:=TmpObj.f_TQPUD11;
           if TmpObj.TQPUD12_Updated then
              Cds.FieldValues['TQPUD12']:=TmpObj.f_TQPUD12;
           if TmpObj.TQPUD13_Updated then
              Cds.FieldValues['TQPUD13']:=TmpObj.f_TQPUD13;
           if TmpObj.TQPUD14_Updated then
              Cds.FieldValues['TQPUD14']:=TmpObj.f_TQPUD14;
           if TmpObj.TQPUD15_Updated then
              Cds.FieldValues['TQPUD15']:=TmpObj.f_TQPUD15;
           if TmpObj.TQPPLANT_Updated then
              Cds.FieldValues['TQPPLANT']:=TmpObj.f_TQPPLANT;
           if TmpObj.TQPLEGAL_Updated then
              Cds.FieldValues['TQPLEGAL']:=TmpObj.f_TQPLEGAL;
           if TmpObj.TQPORIU_Updated then
              Cds.FieldValues['TQPORIU']:=TmpObj.f_TQPORIU;
           if TmpObj.TQPORIG_Updated then
              Cds.FieldValues['TQPORIG']:=TmpObj.f_TQPORIG;
           Cds.Post;
        except
           ok:=false;
        end;
        if not ok then
           break;
     end;
  if ok then
     begin
        try
           aParcel.PutCDSGoods(GoodsName,Cds);
           ok:=true;
        except
           ok:=false;
        end;
     end;
  Cds.Free;
  result:=ok;
end;

//
// 从数据包裹对象中装入数据到一个列表对象......
function TTQP_FILEList.LoadFromParcel(aParcel: TQBParcel; GoodsName: AnsiString): boolean;
var
  Cds: TClientDataset;
  TmpObj: TTQP_FILE;
  ok: boolean;
begin
  self.Clear;
  Cds:=TClientDataset.Create(nil);
  try
     aParcel.GetCDSGoods(GoodsName,Cds);
     ok:=Cds.Active;
  except
     ok:=false;
  end;
  if not ok then
     begin
        cds.Free;
        result:=false;
        exit;
     end;
  cds.First;
  while not cds.Eof do
     begin
        TmpObj := TTQP_FILE.ReadFromCDS(Cds);
        Self.Add(TmpObj);
        Cds.Next;
     end;
  Cds.Free;
  result:=true;
end;
{$ENDIF}

//
// 列表对象克隆...
function TTQP_FILEList.CloneTo(NewList: TTQP_FILEList): boolean;
var
   i: integer;
   NewObj: TTQP_FILE;
begin
   for i:=0 to Self.Count-1 do
      begin
         NewObj:=TTQP_FILE.Create;
         TTQP_FILE(Self.Items[i]).CloneTo(NewObj);
         NewList.Add(NewObj);
      end;
   Result:=true;
end;

end.

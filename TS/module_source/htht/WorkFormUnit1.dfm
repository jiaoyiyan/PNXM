object WorkForm: TWorkForm
  Left = 309
  Top = 196
  Align = alClient
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'DLL'#20013#30340'Work'#31383#20307
  ClientHeight = 589
  ClientWidth = 1398
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object MasterP: TPanel
    Left = 0
    Top = 0
    Width = 1398
    Height = 589
    Align = alClient
    FullRepaint = False
    TabOrder = 2
    object pnl1: TPanel
      Left = 1
      Top = 1
      Width = 1396
      Height = 41
      Align = alTop
      TabOrder = 0
      object addbody: TButton
        Left = 444
        Top = 9
        Width = 102
        Height = 25
        Caption = #26032#22686#21333#36523
        TabOrder = 1
        OnClick = addbodyClick
      end
      object btnOK: TButton
        Left = 849
        Top = 9
        Width = 125
        Height = 25
        Caption = #21512#21516#20445#23384
        TabOrder = 3
        OnClick = btnOKClick
      end
      object btnmodi: TButton
        Left = 608
        Top = 9
        Width = 85
        Height = 25
        Caption = #21024#38500#21333#36523#35760#24405
        TabOrder = 2
        OnClick = btnmodiClick
      end
      object btnbadd: TButton
        Left = 336
        Top = 9
        Width = 102
        Height = 25
        Caption = #26032#22686#21512#21516
        TabOrder = 0
        OnClick = btnbaddClick
      end
      object btnallBody: TButton
        Left = 708
        Top = 9
        Width = 85
        Height = 25
        Caption = #21333#36523#28165#31354
        TabOrder = 4
        OnClick = btnallBodyClick
      end
      object btnDel: TButton
        Left = 180
        Top = 9
        Width = 75
        Height = 25
        Caption = #21512#21516#21024#38500
        TabOrder = 5
        OnClick = btnDelClick
      end
    end
    object btnOpenTQP: TButton
      Left = 19
      Top = 10
      Width = 94
      Height = 25
      Caption = #20999#25442#20026#32534#36753#27169#24335
      TabOrder = 1
      OnClick = btnOpenTQPClick
    end
    object MainPage: TPageControl
      Left = 1
      Top = 42
      Width = 1396
      Height = 546
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = #21512#21516#32500#25252
        object pnl_db: TPanel
          Left = 0
          Top = 0
          Width = 1388
          Height = 160
          Align = alTop
          BorderStyle = bsSingle
          Enabled = False
          TabOrder = 0
          object lbl8: TLabel
            Left = 38
            Top = 54
            Width = 60
            Height = 12
            Caption = #23458#25143#32534#21495#65306
          end
          object Label1: TLabel
            Left = 376
            Top = 16
            Width = 84
            Height = 12
            Caption = #21512#21516#31614#32422#26102#38388#65306
          end
          object ll_name: TLabel
            Left = 248
            Top = 124
            Width = 73
            Height = 12
            AutoSize = False
          end
          object htbh: TLabeledEdit
            Left = 104
            Top = 13
            Width = 201
            Height = 20
            EditLabel.Width = 60
            EditLabel.Height = 12
            EditLabel.Caption = #21512#21516#32534#21495#65306
            LabelPosition = lpLeft
            TabOrder = 0
            OnChange = htbhChange
          end
          object jhzg: TLabeledEdit
            Left = 104
            Top = 123
            Width = 119
            Height = 20
            BevelInner = bvNone
            BevelKind = bkFlat
            BevelOuter = bvRaised
            BorderStyle = bsNone
            EditLabel.Width = 60
            EditLabel.Height = 12
            EditLabel.Caption = #35745#21010#20027#31649#65306
            LabelPosition = lpLeft
            LabelSpacing = 5
            ParentColor = True
            ReadOnly = True
            TabOrder = 1
          end
          object htzj: TLabeledEdit
            AlignWithMargins = True
            Left = 472
            Top = 123
            Width = 121
            Height = 20
            BevelInner = bvSpace
            BevelKind = bkFlat
            BevelOuter = bvRaised
            BiDiMode = bdLeftToRight
            BorderStyle = bsNone
            Color = clBtnFace
            EditLabel.Width = 84
            EditLabel.Height = 12
            EditLabel.BiDiMode = bdLeftToRight
            EditLabel.Caption = #21512#21516#24635#20215'('#20803')'#65306
            EditLabel.ParentBiDiMode = False
            EditLabel.Layout = tlCenter
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = #23435#20307
            Font.Style = []
            LabelPosition = lpLeft
            ParentBiDiMode = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            Text = '011111'
            OnChange = htzjChange
          end
          object htyl: TLabeledEdit
            Left = 766
            Top = 123
            Width = 121
            Height = 20
            BevelInner = bvNone
            BevelKind = bkFlat
            BevelOuter = bvRaised
            BorderStyle = bsNone
            Color = clBtnFace
            EditLabel.Width = 96
            EditLabel.Height = 12
            EditLabel.Caption = #21512#21516#30408#21033#39069'('#20803')'#65306
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -12
            Font.Name = #23435#20307
            Font.Style = []
            LabelPosition = lpLeft
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            OnChange = htylChange
          end
          object htzt: TLabeledEdit
            Left = 766
            Top = 13
            Width = 121
            Height = 20
            BevelInner = bvNone
            BevelKind = bkFlat
            BevelOuter = bvRaised
            BorderStyle = bsNone
            Color = clBtnFace
            EditLabel.Width = 60
            EditLabel.Height = 12
            EditLabel.Caption = #21512#21516#29366#24577#65306
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #23435#20307
            Font.Style = []
            LabelPosition = lpLeft
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            OnChange = htztChange
          end
          object Panel1: TPanel
            Left = 945
            Top = -2
            Width = 185
            Height = 154
            BevelOuter = bvNone
            TabOrder = 5
            object btn_audio: TButton
              Left = 56
              Top = 8
              Width = 113
              Height = 34
              Caption = #23457'  '#26680
              TabOrder = 0
              OnClick = btn_audioClick
            end
            object flag_OK: TButton
              Left = 56
              Top = 57
              Width = 113
              Height = 34
              Caption = #32467'  '#26696
              TabOrder = 1
              OnClick = flag_OKClick
            end
            object flag_zf: TButton
              Left = 56
              Top = 104
              Width = 113
              Height = 34
              Caption = #20316'  '#24223
              TabOrder = 2
              OnClick = flag_zfClick
            end
          end
          object pik_create: TDateTimePicker
            Left = 472
            Top = 13
            Width = 116
            Height = 20
            Date = 43543.358442662040000000
            Time = 43543.358442662040000000
            TabOrder = 6
            OnChange = pik_createChange
          end
          object khbh: TcxButtonEdit
            Left = 104
            Top = 49
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = khbhPropertiesButtonClick
            Properties.OnChange = khbhPropertiesChange
            TabOrder = 7
            Text = 'khbh'
            Width = 121
          end
          object Lhygs: TLabeledEdit
            Left = 472
            Top = 49
            Width = 121
            Height = 20
            BevelInner = bvNone
            BevelKind = bkFlat
            BevelOuter = bvRaised
            BorderStyle = bsNone
            Color = clMenu
            EditLabel.Width = 60
            EditLabel.Height = 12
            EditLabel.Caption = #34892#19994#24402#23646#65306
            LabelPosition = lpLeft
            LabelSpacing = 5
            TabOrder = 8
          end
          object lkhjl: TLabeledEdit
            Left = 766
            Top = 49
            Width = 121
            Height = 20
            BevelInner = bvNone
            BevelKind = bkFlat
            BevelOuter = bvRaised
            BorderStyle = bsNone
            Color = clMenu
            EditLabel.Width = 60
            EditLabel.Height = 12
            EditLabel.Caption = #23458#25143#32463#29702#65306
            LabelPosition = lpLeft
            LabelSpacing = 5
            TabOrder = 9
          end
          object lkhjc: TLabeledEdit
            Left = 104
            Top = 86
            Width = 201
            Height = 20
            BevelInner = bvNone
            BevelKind = bkFlat
            BevelOuter = bvRaised
            BorderStyle = bsNone
            Color = clMenu
            EditLabel.Width = 60
            EditLabel.Height = 12
            EditLabel.Caption = #23458#25143#31616#31216#65306
            LabelPosition = lpLeft
            LabelSpacing = 5
            TabOrder = 10
          end
          object lkhqc: TLabeledEdit
            Left = 472
            Top = 86
            Width = 415
            Height = 20
            BevelInner = bvNone
            BevelKind = bkFlat
            BevelOuter = bvRaised
            BorderStyle = bsNone
            Color = clMenu
            EditLabel.Width = 48
            EditLabel.Height = 12
            EditLabel.Caption = #23458#25143#20840#31216
            LabelPosition = lpLeft
            LabelSpacing = 5
            TabOrder = 11
          end
        end
        object cxG: TcxGrid
          Left = 0
          Top = 160
          Width = 1388
          Height = 358
          Align = alClient
          Enabled = False
          TabOrder = 1
          object CGB: TcxGridTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            DataController.OnNewRecord = CGBDataControllerNewRecord
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.Appending = True
            OptionsView.NoDataToDisplayInfoText = #27809#26377#25968#25454
            OptionsView.CellAutoHeight = True
            OptionsView.GroupByBox = False
            Styles.Background = cxStyle3
            Styles.ContentOdd = cxStyle2
            object CGBColumn1: TcxGridColumn
              Visible = False
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 20
              IsCaptionAssigned = True
            end
            object CGBColumn2: TcxGridColumn
              Caption = #39033#27425
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 40
            end
            object CGBColumn3: TcxGridColumn
              Caption = #20135#21697#21517#31216
              Options.Filtering = False
              Options.Sorting = False
              Width = 160
            end
            object CGBColumn4: TcxGridColumn
              Caption = #21333#20301
              Options.Filtering = False
              Options.Sorting = False
              Width = 40
            end
            object CGBColumn5: TcxGridColumn
              Caption = #25968#37327
              DataBinding.ValueType = 'Float'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '0.###'
              Properties.OnEditValueChanged = CGBColumn5PropertiesEditValueChanged
              Options.Filtering = False
              Options.Sorting = False
              Width = 80
            end
            object CGBColumn6: TcxGridColumn
              Caption = #21333#37325'(kg)'
              DataBinding.ValueType = 'Float'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '0.###'
              Properties.OnEditValueChanged = CGBColumn6PropertiesEditValueChanged
              Options.Filtering = False
              Options.Sorting = False
              Width = 60
            end
            object CGBColumn7: TcxGridColumn
              Caption = #24635#37325'(kg)'
              DataBinding.ValueType = 'Float'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '0.###'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 90
            end
            object CGBColumn8: TcxGridColumn
              Caption = #32422#23450#20132#36135#26085
              DataBinding.ValueType = 'DateTime'
              PropertiesClassName = 'TcxDateEditProperties'
              Properties.OnEditValueChanged = CGBColumn8PropertiesEditValueChanged
              Options.Filtering = False
              Options.Sorting = False
              Width = 80
            end
            object CGBColumn9: TcxGridColumn
              Caption = #21512#21516#21333#20215'('#20803')'
              DataBinding.ValueType = 'Float'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '0.###'
              Properties.OnEditValueChanged = CGBColumn9PropertiesEditValueChanged
              Options.Filtering = False
              Options.Sorting = False
              Width = 80
            end
            object CGBColumn10: TcxGridColumn
              Caption = #21512#21516#24635#20215'('#20803')'
              DataBinding.ValueType = 'Float'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '0.###'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 80
            end
            object CGBColumn11: TcxGridColumn
              Caption = #39044#20272#36816#36153'('#20803')'
              DataBinding.ValueType = 'Float'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '0.###'
              Properties.OnEditValueChanged = CGBColumn11PropertiesEditValueChanged
              Options.Filtering = False
              Options.Sorting = False
              Width = 80
            end
            object CGBColumn12: TcxGridColumn
              Caption = #25104#26412#20215'('#20803')'
              DataBinding.ValueType = 'Float'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '0.###'
              Properties.OnEditValueChanged = CGBColumn12PropertiesEditValueChanged
              Options.Filtering = False
              Options.Sorting = False
              Width = 80
            end
            object CGBColumn13: TcxGridColumn
              Caption = #20986#21378#25104#26412#20215'('#20803')'
              DataBinding.ValueType = 'Float'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '0.###'
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 100
            end
            object CGBColumn14: TcxGridColumn
              Caption = #30408#21033#39069'('#20803')'
              DataBinding.ValueType = 'Float'
              PropertiesClassName = 'TcxCalcEditProperties'
              Properties.DisplayFormat = '0.###'
              Options.Editing = False
              Options.Filtering = False
              Options.GroupFooters = False
              Options.Sorting = False
              Width = 80
            end
            object CGBColumn15: TcxGridColumn
              Caption = #22791#27880
              PropertiesClassName = 'TcxMemoProperties'
              Properties.OnEditValueChanged = CGBColumn15PropertiesEditValueChanged
              Options.Filtering = False
              Options.Sorting = False
              Width = 260
            end
            object CGBColumn16: TcxGridColumn
              Caption = #22791#29992'1'
              Visible = False
              Options.Filtering = False
              Options.Sorting = False
            end
          end
          object cxGLevel1: TcxGridLevel
            GridView = CGB
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = #21512#21516#28165#21333
        ImageIndex = 1
        object dbgrd1: TDBGrid
          Left = 0
          Top = 0
          Width = 1388
          Height = 518
          Align = alClient
          Color = clInfoBk
          DataSource = ds1
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #23435#20307
          TitleFont.Style = []
          OnDblClick = dbgrd1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'TQP01'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TQP02'
              Title.Caption = #21512#21516#21517#31216
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TQP06'
              Title.Caption = #21512#21516#36215#22987#26085#26399
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TQP03'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TQPUD07'
              Title.Caption = #21512#21516#24635#20215
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TQP08'
              Title.Caption = #23458#25143#32534#21495
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'tqp20'
              Title.Caption = #20184#27454#26465#20214
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TQP04'
              Title.Caption = #21512#21516#29366#24577
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TQPDATE'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TQPORIU'
              ReadOnly = False
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TQPPLANT'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TQPLEGAL'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TQPACTI'
              Visible = False
            end
            item
              Expanded = False
              FieldName = 'TQPORIG'
              Visible = False
            end>
        end
      end
    end
    object btnfind: TButton
      Left = 1032
      Top = 10
      Width = 89
      Height = 25
      Caption = #26597#35810
      TabOrder = 3
      OnClick = btnfindClick
    end
  end
  object pnl_find: TPanel
    Left = 770
    Top = 35
    Width = 352
    Height = 89
    BevelOuter = bvLowered
    Caption = 'pnl_find'
    Color = clInfoBk
    ParentBackground = False
    TabOrder = 1
    Visible = False
    object pnl7: TPanel
      Left = 1
      Top = 1
      Width = 350
      Height = 54
      Align = alClient
      Color = clInfoBk
      ParentBackground = False
      TabOrder = 0
      object qhtbh: TLabeledEdit
        Left = 0
        Top = 33
        Width = 170
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #21512#21516#32534#21495
        TabOrder = 0
      end
      object qhtkq: TLabeledEdit
        Left = 176
        Top = 32
        Width = 161
        Height = 20
        EditLabel.Width = 72
        EditLabel.Height = 12
        EditLabel.Caption = #21512#21516#23458#25143#32534#21495
        TabOrder = 1
      end
    end
    object pnl8: TPanel
      Left = 1
      Top = 55
      Width = 350
      Height = 33
      Align = alBottom
      Color = clInfoBk
      ParentBackground = False
      TabOrder = 1
      object btn2: TButton
        Left = 192
        Top = 4
        Width = 105
        Height = 25
        Caption = #26597#35810
        TabOrder = 0
        OnClick = btn2Click
      end
      object btn4: TButton
        Left = 80
        Top = 3
        Width = 106
        Height = 25
        Caption = #20851#38381
        TabOrder = 1
        OnClick = btn4Click
      end
    end
  end
  object pnl_kh: TPanel
    Left = 180
    Top = 138
    Width = 560
    Height = 199
    Caption = 'pnl_kh'
    Color = clInfoBk
    ParentBackground = False
    TabOrder = 0
    Visible = False
    object pnl4: TPanel
      Left = 1
      Top = 1
      Width = 558
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object qkhbh: TLabeledEdit
        Left = 0
        Top = 29
        Width = 93
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#32534#21495
        TabOrder = 0
      end
      object qkhgj: TLabeledEdit
        Left = 448
        Top = 29
        Width = 87
        Height = 20
        EditLabel.Width = 24
        EditLabel.Height = 12
        EditLabel.Caption = #22269#23478
        TabOrder = 3
        Visible = False
      end
      object qkhjc: TLabeledEdit
        Left = 88
        Top = 29
        Width = 132
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#31616#31216
        TabOrder = 1
      end
      object qkhqc: TLabeledEdit
        Left = 212
        Top = 29
        Width = 237
        Height = 20
        EditLabel.Width = 48
        EditLabel.Height = 12
        EditLabel.Caption = #23458#25143#20840#31216
        TabOrder = 2
      end
    end
    object pnl5: TPanel
      Left = 1
      Top = 50
      Width = 558
      Height = 29
      Align = alTop
      BevelKind = bkFlat
      BevelOuter = bvNone
      Color = clHighlightText
      ParentBackground = False
      TabOrder = 2
      object btn1: TButton
        Left = 390
        Top = 0
        Width = 121
        Height = 25
        Caption = #26597#35810
        TabOrder = 0
        OnClick = btn1Click
      end
      object btn3: TButton
        Left = 262
        Top = 0
        Width = 121
        Height = 25
        Caption = #20851#38381
        TabOrder = 1
        OnClick = btn3Click
      end
    end
    object dbgrd2: TDBGrid
      Left = 1
      Top = 79
      Width = 558
      Height = 119
      Align = alClient
      Color = clInfoBk
      DataSource = ds2
      Options = [dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #23435#20307
      TitleFont.Style = []
      OnDblClick = dbgrd2DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'OCC01'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OCC02'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'occ18'
          Width = 243
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'oca02'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'gen02'
          Visible = True
        end>
    end
  end
  object Memo1: TMemo
    Left = 952
    Top = 329
    Width = 321
    Height = 97
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
    Visible = False
  end
  object Cds: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 72
    Top = 304
  end
  object WinMessage: TQBWinMessages
    OnMsgArrived = WinMessageMsgArrived
    Left = 41
    Top = 426
  end
  object dba: TRemoteUniDac
    TaskTimeout = 30
    EnableBCD = False
    EnableFMTBCD = False
    EnableLoadBalance = False
    Left = 56
    Top = 358
  end
  object cxStyleRepository: TcxStyleRepository
    Left = 880
    Top = 280
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor]
      Color = 15784893
    end
    object DELS: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsStrikeOut]
    end
    object AddS: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      TextColor = clRed
    end
    object ModS: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic]
    end
    object DefaultS: TcxStyle
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
  end
  object PrivilegeCDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 1192
    Top = 272
  end
  object ds1: TDataSource
    DataSet = Cds
    Left = 88
    Top = 360
  end
  object Tuq: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 368
    Top = 360
  end
  object ds2: TDataSource
    DataSet = Tuq
    Left = 412
    Top = 350
  end
  object CDSTMP: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 224
    Top = 376
  end
end

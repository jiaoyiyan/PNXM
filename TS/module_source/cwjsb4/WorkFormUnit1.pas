﻿//
// WorkFormUnit.pas -- 客户端窗体模块 - 工作窗体单元（供应用程序员修改）
// Version 2.00
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit WorkFormUnit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UserConnection, DbAccessor, Grids, DBGrids, DB, dateUtils,
  DBClient, FileTransfer, QBCommon, ExtCtrls, QBParcel,
  BaseGrid, Vcl.ComCtrls, Vcl.Imaging.pngimage, QBWinMessages,
  DllSpread, RemoteUniDac, system.Win.comobj, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridCustomView, cxGrid, cxClasses, math, cxGridExportLink, frxClass,
  frxPreview, frxExportBaseDialog, frxExportXLSX;

type
  TWorkForm = class(TForm)
    Cds: TClientDataSet;
    MasterP: TPanel;
    Panel3: TPanel;
    qxm: TImage;
    Label1: TLabel;
    xmLE: TLabeledEdit;
    beginT: TDateTimePicker;
    endT: TDateTimePicker;
    RunQuery: TButton;
    Toxls: TButton;
    ztxz: TRadioGroup;
    TimeP: TPanel;
    Label2: TLabel;
    qbYear: TEdit;
    qbmon: TComboBox;
    btn_Print: TButton;
    WinMessage: TQBWinMessages;
    dba: TRemoteUniDac;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    MGrid: TcxGrid;
    MTV: TcxGridBandedTableView;
    MGridLevel1: TcxGridLevel;
    PrivilegeCDS: TClientDataSet;
    Panel1: TPanel;
    LE_jgl: TLabeledEdit;
    LE_rgcb: TLabeledEdit;
    LE_sbzj: TLabeledEdit;
    LE_qtfy: TLabeledEdit;
    frxReport1: TfrxReport;
    frxPreview1: TfrxPreview;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    ftitle: TfrxUserDataSet;
    fData: TfrxUserDataSet;
    LE_jsjg: TLabeledEdit;
    LE_XS: TLabeledEdit;
    btn_InputJG: TButton;
    btn_outputJG: TButton;
    btn_toxls: TButton;
    frxXLSXExport1: TfrxXLSXExport;
    tba: TDBAccessor;
    MTVColumn1: TcxGridBandedColumn;
    MTVColumn2: TcxGridBandedColumn;
    MTVColumn3: TcxGridBandedColumn;
    MTVColumn4: TcxGridBandedColumn;
    MTVColumn5: TcxGridBandedColumn;
    MTVColumn6: TcxGridBandedColumn;
    MTVColumn7: TcxGridBandedColumn;
    MTVColumn8: TcxGridBandedColumn;
    MTVColumn9: TcxGridBandedColumn;
    MTVColumn10: TcxGridBandedColumn;
    MTVColumn11: TcxGridBandedColumn;
    MTVColumn12: TcxGridBandedColumn;
    MTVColumn13: TcxGridBandedColumn;
    MTVColumn14: TcxGridBandedColumn;
    MTVColumn15: TcxGridBandedColumn;
    MTVColumn16: TcxGridBandedColumn;
    MTVColumn17: TcxGridBandedColumn;
    Memo1: TMemo;
    MTVColumn18: TcxGridBandedColumn;
    MTVColumn19: TcxGridBandedColumn;
    MTVColumn20: TcxGridBandedColumn;
    MTVColumn21: TcxGridBandedColumn;
    procedure FormShow(Sender: TObject);
    procedure WinMessageMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure ztxzClick(Sender: TObject);
    procedure qxmClick(Sender: TObject);
    procedure RunQueryClick(Sender: TObject);
    procedure btn_PrintClick(Sender: TObject);
    procedure qbmonClick(Sender: TObject);
    procedure qbYearExit(Sender: TObject);
    procedure ftitleCheckEOF(Sender: TObject; var Eof: Boolean);
    procedure ftitleFirst(Sender: TObject);
    procedure ftitleGetValue(const VarName: string; var Value: Variant);
    procedure ftitleNext(Sender: TObject);
    procedure ftitlePrior(Sender: TObject);
    procedure fudsCheckEOF(Sender: TObject; var Eof: Boolean);
    procedure fudsFirst(Sender: TObject);
    procedure fudsGetValue(const VarName: string; var Value: Variant);
    procedure fudsNext(Sender: TObject);
    procedure fudsPrior(Sender: TObject);
    procedure frxReport1GetValue(const VarName: string; var Value: Variant);
    procedure PageControl1Change(Sender: TObject);
    procedure btn_toxlsClick(Sender: TObject);
    procedure btn_InputJGClick(Sender: TObject);
    procedure btn_outputJGClick(Sender: TObject);
  private
    procedure GetLoginUser(key: AnsiString);
    procedure LogMsg(aMsg: AnsiString);
    procedure SetTimeRange;
    function RunCDS(SQL: string): Boolean;
    function GetFilterS: string;
    procedure FillmGrid(Q: TClientDataSet; tp: Integer = 1);
    procedure GetAuth(key: AnsiString);
    procedure ResizeForm;
    procedure GetPrivilege(TFlag, TID, UID: string);
    procedure WaitMessage;
    procedure zhunbeiData;
    procedure zhunbeiData_title;
    procedure zhunbeiData_Main;
    procedure zhunbeiData_title_ini;
    procedure zhunbeiData_title_fill;
    function RunTCDS(SQL: string): Boolean;
    { Private declarations }
  public
    { Public declarations }
    ParentForm: TForm;
    InputParcel: TQBParcel;
    OutputParcel: TQBParcel;
    UserConn: TUserConnection;
  end;

implementation

{$R *.dfm}

// 类型4下线包类结算表的计算
const
  DLL_KEY = 'CWJSB4_DLL';
  DChar = ','; // 分隔符
  SPCHAR = '-';
  DefaultZT = 'GWGS';

  TitleCol = 15;
  DataCol = 17;
  xmLeng = 5; // 项目编号的长度，与易拓规格相同

var
  zhanghu: string;
  CurrentZT: string;
  softWareAuth: Boolean;
  MessageID: uint32; // message的ID号，记录是由谁发出的信息，以便信息返回时响应对应的操作
  MessageFlag: Boolean; // message令牌。当messageFlag为true时，表示令牌正在使用，false表示空闲

  RtitleValue: array [1 .. TitleCol] of Variant;
  RDataValue: array of array of Variant;

  tqcwyb: string;
  tqcyyb: string;
  tqmwyb: string;
  tqmyyb: string;
  allBH: string; // 所有相关包号

  xmNF, zqTimeF, qTimeF, bTimeF, chTimeF, ZxTimeF: string; // 项目,时间的过滤条件
  qyear, qmonth, byear, bmonth: Integer;

  // ----------------------------------------------------------------------------
  //
procedure TWorkForm.WaitMessage;
begin
  While True do
  Begin
    application.ProcessMessages; // 读取消息队列
    If Not MessageFlag Then
      Break;
  End;
end;

procedure TWorkForm.FormShow(Sender: TObject);
begin
  MessageID := 0;
  MessageFlag := false;
  // 注册消息
  WinMessage.RegisterUserMessage('Main_LoginUser');
  WinMessage.RegisterUserMessage('Main_SoftWareAuth');
  WinMessage.RegisterUserMessage('Main_GetPrivilege');

  // 初始化数据集对象...
  dba.UserConn := UserConn;
  dba.TargetNodeId := UserConn.UserNodeId;
  dba.TargetDatabaseId := InputParcel.GetAnsiStringGoods('DatabaseId');

  tba.UserConnection := UserConn;
  tba.TargetNodeId := UserConn.UserNodeId;
  tba.TargetDatabaseId := 'TS_TIPTOP';
  // 获取插件formCaption
  self.Caption := InputParcel.GetAnsiStringGoods('FormCaption');
  // 获得校验证信息
  GetAuth(DLL_KEY);
  // 获得LOGINUSER
  GetLoginUser(DLL_KEY);
  WaitMessage; // 同步处理消息

  // 界面初始化
  // 默认为当天
  endT.Date := now;
  beginT.Date := now;

  // 默认期别 年度为本年 月份为本月
  CurrentZT := DefaultZT;
  qbYear.Text := inttostr(YearOf(now));
  qbmon.ItemIndex := MonthOf(now) - 1;
  zhunbeiData_title_ini;

  // 获取是否可修改项目编号权限,设置xeLE的只读性
  GetPrivilege('ASSERT', '1', zhanghu);
  WaitMessage;
  if PrivilegeCDS.RecordCount = 0 then
    xmLE.ReadOnly := True
  else
    xmLE.ReadOnly := PrivilegeCDS.FieldByName('R1').AsString = '0';
end;

// -------------------报表输出数据准备函数-------------------------------------
{$REGION '报表输出模块'}

const
  // 报表中要获取的相关title信息
  RSQL = 'SELECT tc_pjb01,gem02,SUM(tc_pjb07) pjgl,' +
    'ROUND(NVL(SUM(tc_pjb11),0)/DECODE(NVL(SUM(tc_pjb07),0),0,1,SUM(tc_pjb07)),6) jg FROM '
    + '(SELECT tc_pjb01,gem02,tc_pjb07,tc_pjb11 FROM %s.tc_pjb_file,%s.gem_file '
    + 'WHERE tc_pjb01 = gem01 AND tc_pjb01 = ''%s'' AND tc_pjb02 = %s AND tc_pjb03 = %d) '
    + 'GROUP BY tc_pjb01,gem02';
  // 查询期初期末包使用情况sql
  BHSQL = 'SELECT tc_pjz01,nvl(SUM(tc_pjz05),0) lc FROM tc_pjz_file ' +
    sLineBreak + 'WHERE   tc_pjz10 = ''%s''        ' + sLineBreak + // 项目编号
    '    AND tc_pjz01 IN (            ' + sLineBreak +
    '     SELECT tc_pjz01 FROM tc_pjz_file,tc_pjy_file,gem_file ' + sLineBreak +
    '       WHERE tc_pjz10 = ''%0:s'' AND tc_pjz01 = tc_pjy01 AND ' + sLineBreak
    + '             tc_pjy03 = tc_pjz10 AND gem01 = tc_pjz10 AND    ' +
    sLineBreak + '             tc_pjz02 = %d AND tc_pjz03 = %d AND         ' +
  // 当前周期年/月
    '            (tc_pjy07 IS NULL OR tc_pjy07 > to_date(''%3:s'',''yyyymmdd'')))'
    + // 时间周期初前一天
    '    AND     tc_pjz02||to_char(tc_pjz03,''FM00'') <= ''%4:s'' ' + sLineBreak
    + // 当前周期的年与月格式 '201809'
    '    GROUP BY tc_pjz10,tc_pjz01 HAVING NVL(SUM(tc_pjz05),0) %5:s 0';
  // 取 =时为未用包 取>时为已用包

procedure TWorkForm.btn_PrintClick(Sender: TObject);
begin
  zhunbeiData;
  if frxReport1.LoadFromFile(ExtractFilePath(application.Exename) +
    'modules\cwjsb04.fr3') then
  begin
    frxReport1.ShowReport;
    PageControl1.ActivePageIndex := 1;
    ResizeForm;
  end
  else
    showmessage('打开表格模板失败，请关闭后重试！');
end;

procedure TWorkForm.zhunbeiData;
begin
  zhunbeiData_title;
  zhunbeiData_Main;

  ftitle.RangeEnd := reCount;
  ftitle.RangeEndCount := 1;

  fData.RangeEnd := reCount;
  fData.RangeEndCount := MTV.DataController.RecordCount;
end;

procedure TWorkForm.zhunbeiData_title;
begin
  // 填充ftitle数组值
  { 项目名称｜时间范围｜
    期初未用包数｜期初已用包数
    期末未用包数｜期末已用包数期末理论炉龄｜
    本月做包数｜本月累计炉次｜烧钢量｜人工成本｜设备折旧｜其他费用｜结算价格｜调整系数 }
  // 项目名称与时间范围在SetTimeRange中获取
  if (RtitleValue[1] = '') or (xmNF <> Trim(xmLE.Text)) then
  begin
    xmNF := Trim(xmLE.Text);
    zhunbeiData_title_fill;
  end;

  RtitleValue[10] := StrToFloatDef(Trim(LE_jgl.Text), 0);
  RtitleValue[11] := StrToFloatDef(Trim(LE_rgcb.Text), 0);
  RtitleValue[12] := StrToFloatDef(Trim(LE_sbzj.Text), 0);
  RtitleValue[13] := StrToFloatDef(Trim(LE_qtfy.Text), 0);
  RtitleValue[14] := StrToFloatDef(Trim(LE_jsjg.Text), 0);
  RtitleValue[15] := StrToFloatDef(Trim(LE_XS.Text), 0);
end;

procedure TWorkForm.zhunbeiData_Main;
var
  i: Integer;
  t: Integer;
  s: Double;
  j, dj: Double;
  c, dc: Double;
begin
  // 填充主数据数组值
  { 料号/名称/配方/单重/数量单位/来源公司/上月未用领料｜上月在线领料｜
    本月收货｜期初现场库存｜期末现场库存｜本月未用领｜本月在线领料｜
    项目号｜基准价/运费/浇钢量 }
  SetLength(RDataValue, 0, 0);
  // 动态数组清空
  SetLength(RDataValue, MTV.DataController.RecordCount, DataCol);
  for i := 0 to MTV.DataController.RecordCount - 1 do
    for t := 0 to DataCol - 1 do
      RDataValue[i, t] := '0';
  for i := 0 to MTV.DataController.RecordCount - 1 do
  begin
    if not VarIsNull(MTV.DataController.Values[i, 0]) then
      RDataValue[i, 0] := MTV.DataController.Values[i, 0];
    if not VarIsNull(MTV.DataController.Values[i, 1]) then
      RDataValue[i, 1] := MTV.DataController.Values[i, 1];
    if not VarIsNull(MTV.DataController.Values[i, 4]) then
      RDataValue[i, 2] := MTV.DataController.Values[i, 4];
    if not VarIsNull(MTV.DataController.Values[i, 5]) then
      RDataValue[i, 3] := MTV.DataController.Values[i, 5];
    if not VarIsNull(MTV.DataController.Values[i, 6]) then
      RDataValue[i, 4] := MTV.DataController.Values[i, 6];
    if not VarIsNull(MTV.DataController.Values[i, 7]) then
      RDataValue[i, 5] := MTV.DataController.Values[i, 7];
    if not VarIsNull(MTV.DataController.Values[i, 9]) then
      RDataValue[i, 6] := MTV.DataController.Values[i, 9];
    if not VarIsNull(MTV.DataController.Values[i, 10]) then
      RDataValue[i, 7] := MTV.DataController.Values[i, 10];
    // 收货＝收货+跨项目领进+调拨进－跨项目领出－调拨出
    s := 0;
    j := 0;
    c := 0;
    dc := 0;
    dj := 0;
    if not VarIsNull(MTV.DataController.Values[i, 17]) then
      j := StrToFloatDef(MTV.DataController.Values[i, 17], 0);
    if not VarIsNull(MTV.DataController.Values[i, 18]) then
      c := StrToFloatDef(MTV.DataController.Values[i, 18], 0);
    if not VarIsNull(MTV.DataController.Values[i, 11]) then
      s := StrToFloatDef(MTV.DataController.Values[i, 11], 0);
    if not VarIsNull(MTV.DataController.Values[i, 19]) then
      dj := StrToFloatDef(MTV.DataController.Values[i, 19], 0);
    if not VarIsNull(MTV.DataController.Values[i, 20]) then
      dc := StrToFloatDef(MTV.DataController.Values[i, 20], 0);
    s := s + j + dj - c - dc;
    if s = 0 then
      RDataValue[i, 8] := '0'
    else
      RDataValue[i, 8] := FormatFloat('0.###', s);

    if not VarIsNull(MTV.DataController.Values[i, 8]) then
      RDataValue[i, 9] := MTV.DataController.Values[i, 8];
    if not VarIsNull(MTV.DataController.Values[i, 12]) then
      RDataValue[i, 10] := MTV.DataController.Values[i, 12];
    if not VarIsNull(MTV.DataController.Values[i, 13]) then
      RDataValue[i, 11] := MTV.DataController.Values[i, 13];
    if not VarIsNull(MTV.DataController.Values[i, 14]) then
      RDataValue[i, 12] := MTV.DataController.Values[i, 14];
    RDataValue[i, 13] := RtitleValue[1];
    // 用来分组用的flag
    if not VarIsNull(MTV.DataController.Values[i, 15]) then
      RDataValue[i, 14] := MTV.DataController.Values[i, 15]
    else
      RDataValue[i, 14] := '0.00';
    if not VarIsNull(MTV.DataController.Values[i, 16]) then
      RDataValue[i, 15] := MTV.DataController.Values[i, 16]
    else
      RDataValue[i, 15] := '0.00';
    RDataValue[i, 16] := RtitleValue[13];
  end;
end;

procedure TWorkForm.zhunbeiData_title_ini;
begin
  { 项目名称｜时间范围｜
    期初未用包数｜期初已用包数
    期末未用包数｜期末已用包数｜期末理论炉龄｜
    本月做包数｜本月累计炉次｜烧钢量｜人工成本｜设备折旧｜其他费用｜结算价格｜调整系数 }
  // 初始化信息
  RtitleValue[1] := '';
  RtitleValue[2] := '无时间范围';
  RtitleValue[3] := 0;
  RtitleValue[4] := 0;
  RtitleValue[5] := 0;
  RtitleValue[6] := 0;
  RtitleValue[7] := 0;
  RtitleValue[8] := 0;
  RtitleValue[9] := 0;
  RtitleValue[10] := 0;
  RtitleValue[11] := 0;
  RtitleValue[12] := 0;
  RtitleValue[13] := 0;
  RtitleValue[14] := 0;
  RtitleValue[15] := 0;
end;

procedure TWorkForm.zhunbeiData_title_fill;
var
  SQL: string;
  E: string;
  t, tp: Tdate;
  dyy: Integer; // 项目当前周期所在年
  dym: Integer;
  syy: Integer;
  sym: Integer;
  tqcwys: Integer;
  tqcyys: Integer;
  tqmwys: Integer;
  tqmyys: Integer;
  tqclc: Integer;
  tqmlc: Integer;
  tdylllc: Integer;
  tsylllc: Integer;
  tbmzb: Integer;
  Tbmlc: Integer;
begin
  SQL := format(RSQL, [CurrentZT, CurrentZT, Copy(Trim(xmLE.Text), 1, xmLeng),
    qbYear.Text, qbmon.ItemIndex + 1]);
  RunCDS(SQL);
  if Cds.RecordCount = 0 then
  begin
    zhunbeiData_title_ini;
    LE_jgl.Text  := '0';
    LE_jsjg.Text := '0';
  end
  else
  begin
    RtitleValue[1] := Cds.FieldByName('gem02').AsString; // 项目名称

    RtitleValue[2] := format('日期 ：%s ~ %s', // 日期范围
      [FormatDateTime('ddddd', beginT.DateTime), FormatDateTime('ddddd',
      endT.DateTime)]);
    LE_jgl.Text := Cds.FieldByName('pjgl').AsString;
    LE_jsjg.Text := Cds.FieldByName('jg').AsString;
  end;
  // 包号信息
  tqcwyb := '(';
  tqcyyb := '(';
  tqmwyb := '(';
  tqmyyb := '(';
  tqcwys := 0;
  tqcyys := 0;
  tqmwys := 0;
  tqmyys := 0;
  tqclc := 0;
  tqmlc := 0;
  tsylllc := 0;
  tdylllc := 0;
  tbmzb := 0;
  Tbmlc := 0;

  tp := incMonth(endT.Date, -1);
  dyy := YearOf(endT.Date);
  dym := MonthOf(endT.Date);
  syy := YearOf(tp);
  sym := MonthOf(tp);

  // 获取当月理论炉次,若查不到，默认为1，防止后面做除数时报错
  SQL := 'select nvl(tc_pjz23,1) a from tc_pjz_file where tc_pjz10 = ''' + xmNF
    + ''' and tc_pjz02 = ' + inttostr(dyy) + ' and tc_pjz03 = ' + inttostr(dym)
    + ' and rownum = 1';
  RunCDS(SQL);
  tdylllc := Cds.FieldByName('a').AsInteger;
  // 查询炉次信息
  // showmessage('(''1'',''2'')');
  // 期初未用包
  SQL := format(BHSQL, [xmNF, syy, sym, FormatDateTime('yyyymmdd',
    beginT.Date - 1), format('%d%.2d', [syy, sym]), '=']);
    memo1.Text := sql;
  RunCDS(SQL);
  Cds.First;
  while not Cds.Eof do
  begin
    tqcwyb := tqcwyb + '''' + Cds.FieldByName('tc_pjz01').AsString + ''',';
    inc(tqcwys);
    Cds.Next;
  end;
  if tqcwys > 0 then
    tqcwyb := Copy(tqcwyb, 1, length(tqcwyb) - 1) + ')'
  else
    tqcwyb := '('''')';
  // Memo1.Text := tqcwyb;
  // exit;
  // 期末未用包
  SQL := format(BHSQL, [xmNF, dyy, dym, FormatDateTime('yyyymmdd', endT.Date),
    format('%d%.2d', [dyy, dym]), '=']);
  RunCDS(SQL);
  Cds.First;
  while not Cds.Eof do
  begin
    tqmwyb := tqmwyb + '''' + Cds.FieldByName('tc_pjz01').AsString + ''',';
    inc(tqmwys);
    Cds.Next;
  end;
  if tqmwys > 0 then
    tqmwyb := Copy(tqmwyb, 1, length(tqmwyb) - 1) + ')'
  else
    tqmwyb := '('''')';
  // 期初已用包
  SQL := format(BHSQL, [xmNF, syy, sym, FormatDateTime('yyyymmdd',
    beginT.Date - 1), format('%d%.2d', [syy, sym]), '>']);
  // memo1.Text := uq.SQL.Text;
  RunCDS(SQL);
  Cds.First;
  while not Cds.Eof do
  begin
    tqcyyb := tqcyyb + '''' + Cds.FieldByName('tc_pjz01').AsString + ''',';
    inc(tqcyys);
    tqclc := tqclc + Cds.FieldByName('lc').AsInteger;
    Cds.Next;
  end;
  if tqcyys > 0 then
    tqcyyb := Copy(tqcyyb, 1, length(tqcyyb) - 1) + ')'
  else
    tqcyyb := '('''')';
  // 期末已用包
  SQL := format(BHSQL, [xmNF, dyy, dym, FormatDateTime('yyyymmdd', endT.Date),
    format('%d%.2d', [dyy, dym]), '>']);
   memo1.Text := sql;
  RunCDS(SQL);
  Cds.First;
  while not Cds.Eof do
  begin
    tqmyyb := tqmyyb + '''' + Cds.FieldByName('tc_pjz01').AsString + ''',';
    inc(tqmyys);
    //Memo1.Lines.Add(tqmyyb);
    tqmlc := tqmlc + Cds.FieldByName('lc').AsInteger;
    Cds.Next;
  end;
  if tqmyys > 0 then
    tqmyyb := Copy(tqmyyb, 1, length(tqmyyb) - 1) + ')'
  else
    tqmyyb := '('''')';
  // 本月做包数及
  SQL := format('SELECT COUNT(1) zb ' +
    'FROM tc_pjy_file   WHERE   tc_pjy03 = ''%s'' AND ' +
    '(tc_pjy07 >= to_date(''%s'',''yyyymmdd'') OR tc_pjy07 is NULL) ' +
    ' AND tc_pjy05 BETWEEN  to_date(''%2:s'',''yyyymmdd'') AND ' +
    ' to_date(''%1:s'',''yyyymmdd'') ',
    [xmNF, FormatDateTime('yyyymmdd', endT.Date), FormatDateTime('yyyymmdd',
    beginT.Date)]);
//   Memo1.Text := sql;
  // exit;
  if RunCDS(SQL) then
    tbmzb := Cds.FieldByName('ZB').AsInteger;
  // 本月累计炉次
  SQL := format('SELECT NVL(SUM(tc_pjz08),0) AS zb FROM tc_pjz_file WHERE ' +
    ' tc_pjz10 = ''%s'' AND tc_pjz02 = %d AND tc_pjz03 = %d', [xmNF, dyy, dym]);
  if RunCDS(SQL) then
    Tbmlc := Cds.FieldByName('ZB').AsInteger;

  RtitleValue[3] := tqcwys;
  RtitleValue[4] := tqcyys;
  RtitleValue[5] := tqmwys;
  RtitleValue[6] := tqmyys;
  RtitleValue[7] := tdylllc;
  RtitleValue[8] := tbmzb;
  RtitleValue[9] := Tbmlc;
end;

procedure TWorkForm.frxReport1GetValue(const VarName: string;
  var Value: Variant);
begin
  { 项目名称｜时间范围｜
    期初未用包数｜期初已用包数｜期初在线包累计炉次｜期初理论炉龄｜
    期末未用包数｜期末已用包数｜期末在线包累计炉次｜期末理论炉龄｜
    本月做包数｜本月累计炉次｜烧钢量｜人工成本｜设备折旧｜其他费用｜结算价格｜调整系数 }
  if CompareText(VarName, 'xmmc') = 0 then
    Value := RtitleValue[1];
  if CompareText(VarName, 'sjfw') = 0 then
    Value := RtitleValue[2];
  if CompareText(VarName, 'qcwys') = 0 then
    Value := RtitleValue[3];
  if CompareText(VarName, 'qcyys') = 0 then
    Value := RtitleValue[4];
  if CompareText(VarName, 'qmwys') = 0 then
    Value := RtitleValue[5];
  if CompareText(VarName, 'qmyys') = 0 then
    Value := RtitleValue[6];
  if CompareText(VarName, 'qmll') = 0 then
    Value := RtitleValue[7];
  if CompareText(VarName, 'bmzb') = 0 then
    Value := RtitleValue[8];
  if CompareText(VarName, 'bmlc') = 0 then
    Value := RtitleValue[9];
  if CompareText(VarName, 'pjgl') = 0 then
    Value := RtitleValue[10];
  if CompareText(VarName, 'rgcb') = 0 then
    Value := RtitleValue[11];
  if CompareText(VarName, 'sbzj') = 0 then
    Value := RtitleValue[12];
  if CompareText(VarName, 'qtfy') = 0 then
    Value := RtitleValue[13];
  if CompareText(VarName, 'jsjg') = 0 then
    Value := RtitleValue[14];
  if CompareText(VarName, 'tzxs') = 0 then
    Value := RtitleValue[15];

  { 料号/名称/配方/单重/数量单位/来源公司/上月未用领料｜上月在线领料｜
    本月收货｜期初现场库存｜期末现场库存｜本月未用领｜本月在线领料｜
    项目号｜基准价/运费/浇钢量 }

  if CompareText(VarName, 'ljbh') = 0 then
    Value := RDataValue[fData.RecNo, 0];
  if CompareText(VarName, 'ljmc') = 0 then
    Value := RDataValue[fData.RecNo, 1];
  if CompareText(VarName, 'ljpf') = 0 then
    Value := RDataValue[fData.RecNo, 2];
  if CompareText(VarName, 'ljdz') = 0 then
    Value := RDataValue[fData.RecNo, 3];
  if CompareText(VarName, 'ljdw') = 0 then
    Value := RDataValue[fData.RecNo, 4];
  if CompareText(VarName, 'lygs') = 0 then
    Value := RDataValue[fData.RecNo, 5];
  if CompareText(VarName, 'sywyl') = 0 then
    Value := RDataValue[fData.RecNo, 6];
  if CompareText(VarName, 'syzxl') = 0 then
    Value := RDataValue[fData.RecNo, 7];
  if CompareText(VarName, 'bysh') = 0 then
    Value := RDataValue[fData.RecNo, 8];
  if CompareText(VarName, 'qckc') = 0 then
    Value := RDataValue[fData.RecNo, 9];
  if CompareText(VarName, 'qmkc') = 0 then
    Value := RDataValue[fData.RecNo, 10];
  if CompareText(VarName, 'bywyl') = 0 then
    Value := RDataValue[fData.RecNo, 11];
  if CompareText(VarName, 'byzxl') = 0 then
    Value := RDataValue[fData.RecNo, 12];
  if CompareText(VarName, 'mcgroup') = 0 then
    Value := RDataValue[fData.RecNo, 13];
  if CompareText(VarName, 'jzj') = 0 then
    Value := RDataValue[fData.RecNo, 14];
  if CompareText(VarName, 'yf') = 0 then
    Value := RDataValue[fData.RecNo, 15];
  if CompareText(VarName, 'jgl') = 0 then
    Value := RDataValue[fData.RecNo, 16];
end;

procedure TWorkForm.btn_toxlsClick(Sender: TObject);
begin
  frxPreview1.Export(frxXLSXExport1);
end;

procedure TWorkForm.ftitleCheckEOF(Sender: TObject; var Eof: Boolean);
begin
  //
end;

procedure TWorkForm.ftitleFirst(Sender: TObject);
begin
  //
end;

procedure TWorkForm.ftitleGetValue(const VarName: string; var Value: Variant);
begin
  //
end;

procedure TWorkForm.ftitleNext(Sender: TObject);
begin
  //
end;

procedure TWorkForm.ftitlePrior(Sender: TObject);
begin
  //
end;

procedure TWorkForm.fudsCheckEOF(Sender: TObject; var Eof: Boolean);
begin
  //
end;

procedure TWorkForm.fudsFirst(Sender: TObject);
begin
  //
end;

procedure TWorkForm.fudsGetValue(const VarName: string; var Value: Variant);
begin
  //
end;

procedure TWorkForm.fudsNext(Sender: TObject);
begin
  //
end;

procedure TWorkForm.fudsPrior(Sender: TObject);
begin
  //
end;
{$ENDREGION}

// -----------------------------------------------------------------------------
// 业务代码
Const

  STEP1SQL =
    'SELECT DISTINCT tlf01,SUBSTR(tlf904,1,INSTR(tlf904,''-'',1)-1) gs FROM %stlf_file WHERE SUBSTR(tlf904,INSTR(tlf904,''-'',1)+1,5) = ''%s'' AND tlF036 LIKE ''CL01%%'' AND TLF907 = 1 AND %s '
    + #13#10 + 'UNION                  ' + #13#10 + 'SELECT na,gs FROM         '
    + #13#10 + '(                      ' + #13#10 +
    'SELECT tlf01 na,SUBSTR(tlf904,1,INSTR(tlf904,''-'',1)-1) gs,SUM(tlf907*tlf10) s FROM tlf_file WHERE regexp_like(tlf904,''%1:s-[0-9]{1}$'') AND LENGTH(tlf902) = 7 AND %3:s AND tlf907 IN (-1,1) GROUP BY tlf01,tlf904   '
    + #13#10 + 'UNION ALL              ' + #13#10 +
    'SELECT imk01 na,SUBSTR(imk04,1,INSTR(imk04,''-'',1)-1) gs,sum(imk09) s FROM imk_file WHERE regexp_like(imk04,''%1:s-[0-9]{1}$'') AND LENGTH(imk02) = 7 AND imk05 = %4:d  AND imk06 = %5:d GROUP BY imk01,imk04  '
    + #13#10 + ') GROUP BY na,gs HAVING SUM(s) >0 OR SUM(s) < 0   ';

  STEP2SQL = 'SELECT imn03,SUBSTR(imn06,1,INSTR(imn06,''-'',1)-1) imn06, ' +
    #13#10 + 'SUM(DECODE(imm14,''%s'',imn32,NULL)) ljs,    ' + #13#10 +
    'SUM(DECODE(imm14,''%0:s'',imn35,NULL)) ljz,  ' + #13#10 +
    'SUM(DECODE(imm14,''%0:s'',NULL,imn32)) lcs,  ' + #13#10 +
    'SUM(DECODE(imm14,''%0:s'',NULL,imn35)) lcz   ' + #13#10 +
    'FROM %1:simn_file,%1:simm_file            ' + #13#10 +
    'WHERE imn01 = imm01 AND imm03 = ''Y'' AND immconf = ''Y'' AND regexp_like(imm01,''^CL01'') AND imn06 <> SUBSTR(imn17,1,LENGTH(imn06)) AND (imm14 = ''%0:s'' OR regexp_like(imn06,''%0:s$'')) AND  %2:s '
    + #13#10 + 'GROUP BY imn03,SUBSTR(imn06,1,INSTR(imn06,''-'',1)-1)';

  STEP3SQL = 'SELECT imn03,imn06,SUM(imn42) shs,SUM(imn45) shz  ' + #13#10 +
    'FROM %simn_file,%0:simm_file                ' + #13#10 +
    'WHERE imm01 = imn01 AND imm03 = ''Y'' AND regexp_like(imm01,''^SH'') AND imm14 = ''%1:s'' AND %2:s '
    + #13#10 + 'GROUP BY imn03,imn06  ';

  STEP4SQL = 'SELECT imn03,imn06,J_A AS bjs,J_B AS bjz,C_A AS bcs,C_B AS bcz ' +
    sLineBreak + 'FROM                                       ' + sLineBreak +
    '(                                          ' + sLineBreak +
    'SELECT imn03,SUBSTR(imn06,1,INSTR(imn06,''-'',1)-1) imn06,SUM(nvl(imn32,0)) imn32,SUM(nvl(imn35,0)) imn35,''1'' AS F '
    + sLineBreak + 'FROM %simn_file,%0:simm_file               ' + sLineBreak +
    'WHERE imm01 = imn01 AND imm03 = ''Y'' AND  ' + sLineBreak +
    'regexp_like(imm01,''^XD'') AND REGEXP_like(imn06,''%1:s$'') AND ' +
    sLineBreak + 'ta_imm05 = ''%1:s''  AND %2:s              ' + sLineBreak +
    'GROUP BY imn03,SUBSTR(imn06,1,INSTR(imn06,''-'',1)-1) ' + sLineBreak +
    'UNION ALL                                  ' + sLineBreak +
    'SELECT imn03,SUBSTR(imn17,1,INSTR(imn17,''-'',1)-1) imn06,SUM(nvl(imn42,0)) imn32,SUM(nvl(imn45,0)) imn35,''2'' AS F '
    + sLineBreak + 'FROM %0:simn_file,%0:simm_file             ' + sLineBreak +
    'WHERE imm01 = imn01 AND imm03 = ''Y'' AND  ' + sLineBreak +
    'regexp_like(imm01,''^XD'') AND REGEXP_like(imn17,''%1:s$'') AND ' +
    sLineBreak + '(ta_imm07 = ''%1:s''  OR imm14 = ''%1:s'') AND  %2:s' +
    sLineBreak + 'GROUP BY imn03,SUBSTR(imn17,1,INSTR(imn17,''-'',1)-1) ' +
    sLineBreak + ') PIVOT                                    ' + sLineBreak +
    '(SUM(imn32) A,SUM(imn35) B FOR F IN (''1'' AS C,''2'' AS J))';

  STEP5SQL =
    'SELECT imn03,substr(imn06,1,INSTR(imn06,''-'',1)-1) imn06,sum(imn32) imn32,sum(imn35) imn35 FROM %simn_file,%0:simm_file '
    + 'WHERE imm01 = imn01 AND  regexp_like(imm01,''^CL01'') AND imm14 = ''%s'' AND imm03 = ''Y'' AND %s '
    + 'GROUP BY imn03,substr(imn06,1,INSTR(imn06,''-'',1)-1)';

  STEP6SQL =
    'SELECT ima01,ima02,ima021,imaud02,imaud03,imaud05,ima25,(NVL(smd06,0)/NVL(smd04,1)) dz FROM %sima_file '
    + ' left join %0:ssmd_file on smd01 = ima01 ' + 'where ima01 IN (%s)';

  // 无订单出货扣库存
  STEP7SQL =
    'SELECT ogb04,sum(ogb912) sl,sum(ogb915) zl FROM %soga_file,%0:sogb_file WHERE oga01 = ogb01 AND regexp_like(ogb092,''-%1:s$'') '
    + #13#10 +
    ' AND ogapost = ''Y'' AND ogaconf = ''Y'' AND %2:s GROUP BY ogb04';
  // 杂收发引起的库存异动
  STEP8SQL = 'SELECT inb04,a_csl,a_czl,b_csl,b_czl FROM ( ' + #13#10 +
    ' SELECT ina00,inb04,SUM(inb904) sl,SUM(inb924) zl FROM %sinb_file,%0:sina_file WHERE ina01 = inb01 AND ina00 IN (''1'',''3'') AND '
    + #13#10 +
    ' inapost = ''Y'' AND inaconf = ''Y'' AND regexp_like(inb07,''-%1:s$'') AND %2:s '
    + #13#10 + ' GROUP BY ina00,inb04)' + #13#10 +
    ' PIVOT (SUM(sl) csl,SUM(zl) czl FOR ina00 IN (''1'' AS A,''3'' AS B))';

  STEP10SQL = 'SELECT tlf01,tlf904,Nvl(s_csl,0) s_csl,NVL(b_csl,0) b_csl FROM '
    + #13#10 +
    '(SELECT  tlf01,substr(tlf904,1,INSTR(tlf904,''-'',1)-1) tlf904,SUM(tlf10*tlf907) sl,1 AS tp  FROM %stlf_file WHERE tlf01 IN (%s) AND regexp_like(tlf904,''-%s$'') AND %s '
    + #13#10 + 'GROUP BY tlf01,substr(tlf904,1,INSTR(tlf904,''-'',1)-1)   ' +
    #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT  tlf01,substr(tlf904,1,INSTR(tlf904,''-'',1)-1) tlf904,SUM(tlf10*tlf907) sl,2 AS tp  FROM %0:stlf_file WHERE tlf01 IN (%1:s) AND regexp_like(tlf904,''-%2:s$'') AND %4:s '
    + #13#10 + 'GROUP BY tlf01,substr(tlf904,1,INSTR(tlf904,''-'',1)-1)   ' +
    #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT imk01,substr(imk04,1,INSTR(imk04,''-'',1)-1) imk04,sum(imk09) sl, 1 AS tp FROM %0:simk_file WHERE imk01 IN (%1:s) AND regexp_like(imk04,''%2:s$'') AND imk05 = %5:d AND imk06 = %6:d GROUP BY imk01,substr(imk04,1,INSTR(imk04,''-'',1)-1) '
    + #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT imk01,substr(imk04,1,INSTR(imk04,''-'',1)-1) imk04,sum(imk09) sl, 2 AS tp FROM %0:simk_file WHERE imk01 IN (%1:s) AND regexp_like(imk04,''%2:s$'') AND imk05 = %7:d AND imk06 = %8:d GROUP BY imk01,substr(imk04,1,INSTR(imk04,''-'',1)-1) '
    + #13#10 + ')   ' + #13#10 + 'PIVOT  ' + #13#10 +
    '(SUM(sl) csl FOR tp IN (1 AS S,2 AS B))';

  STEP20SQL =
    'SELECT tlff01,tlff904,Nvl(s_csl,0) s_csl,NVL(b_csl,0) b_csl FROM ' + #13#10
    + '(SELECT  tlff01,substr(tlff904,1,INSTR(tlff904,''-'',1)-1) tlff904,SUM(tlff10*tlff907) sl,1 AS tp  FROM %stlff_file WHERE tlff01 IN (%s) AND regexp_like(tlff904,''-%s$'') AND %s '
    + #13#10 + 'GROUP BY tlff01,substr(tlff904,1,INSTR(tlff904,''-'',1)-1) ' +
    #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT  tlff01,substr(tlff904,1,INSTR(tlff904,''-'',1)-1) tlff904,SUM(tlff10*tlff907) sl,2 AS tp  FROM %0:stlff_file WHERE tlff01 IN (%1:s) AND regexp_like(tlff904,''-%2:s$'') AND %4:s '
    + #13#10 + 'GROUP BY tlff01,substr(tlff904,1,INSTR(tlff904,''-'',1)-1) ' +
    #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT imkk01,substr(imkk04,1,INSTR(imkk04,''-'',1)-1) imkk04,sum(imkk09) sl, 1 AS tp FROM %0:simkk_file WHERE imkk01 IN (%1:s) AND regexp_like(imkk04,''%2:s$'') AND imkk05 = %5:d AND imkk06 = %6:d GROUP BY imkk01,substr(imkk04,1,INSTR(imkk04,''-'',1)-1) '
    + #13#10 + 'UNION ALL        ' + #13#10 +
    'SELECT imkk01,substr(imkk04,1,INSTR(imkk04,''-'',1)-1) imkk04,sum(imkk09) sl, 2 AS tp FROM %0:simkk_file WHERE imkk01 IN (%1:s) AND regexp_like(imkk04,''%2:s$'') AND imkk05 = %7:d AND imkk06 = %8:d GROUP BY imkk01,substr(imkk04,1,INSTR(imkk04,''-'',1)-1) '
    + #13#10 + ')   ' + #13#10 + 'PIVOT  ' + #13#10 +
    '(SUM(sl) csl FOR tp IN (1 AS S,2 AS B))';
  // 不同状态领料数据
  STEP30SQL =
    'SELECT  IMN03, IMN15, IMN17, IMN40,nvl(SUM(A_ZSL),0) AS DYLL,nvl(SUM(B_ZSL),0) AS SYWYLL, '
    + 'round(nvl(SUM(C_ZSL),0),3) AS SYZXLL, round(nvl(SUM(D_ZSL),0),3) AS BYWYLL, round(nvl(SUM(E_ZSL),0),3) AS BYZXLL  FROM     '
    + #13#10 +
    '(                                                                                  '
    + #13#10 +
    'SELECT 1 AS S,imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1) imn17,imn40,sum(imn42) SL FROM imm_file,imn_file   '
    + #13#10 +
    'WHERE imn01 = imm01  AND                                                           '
    + #13#10 + 'imm14 = ''%s'' ' + #13#10 + // trim(xmbh.text)
    ' AND                                                                               '
    + #13#10 + '%s        ' + #13#10 + // zqtimeT时间范围
    'AND immconf = ''Y'' AND immud03 in (''1'',''2'')  group by imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1),imn40 '
    + #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 2 AS S,imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1) imn17,imn40,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''%0:s'' AND tc_pjy01 IN %2:s    ' + #13#10 +
  // tqcwyb
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date(''%3:s'',''yyyymmdd'')  '
    + #13#10 + // FormatDateTime('eemmdd', qcr - 1)
    'GROUP BY imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1),imn40                  '
    + #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 3 AS S,imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1) imn17,imn40,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''%0:s'' AND tc_pjy01 IN %4:s ' + #13#10 +
  // tqcyyb
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date(''%3:s'',''yyyymmdd'')                                   '
    + #13#10 +
    'GROUP BY imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1),imn40                                                                                                                               '
    + #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 4 AS S,imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1) imn17,imn40,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''%0:s'' AND tc_pjy01 IN %5:s ' + #13#10 +
  // tqmwyb
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date(''%6:s'',''yyyymmdd'')   '
  // 本周期最后一天
    + #13#10 +
    'GROUP BY imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1),imn40                '
    + #13#10 + 'UNION ALL ' + #13#10 +
    'SELECT 5 AS S,imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1) imn17,imn40,sum(imn42) SL                           '
    + #13#10 +
    'FROM                                                                                '
    + #13#10 +
    '(                                                                                   '
    + #13#10 +
    'SELECT tc_pjy03,tc_pjy01,tc_pjy04 FROM tc_pjy_file                                  '
    + #13#10 + ' WHERE tc_pjy03 = ''%0:s'' AND tc_pjy01 IN %7:s ' + #13#10 +
  // tqmyyb
    ' ) aa,                                                                               '
    + #13#10 +
    '( SELECT imm01,imn03,imn15,imn16,imn17,imn40,imn42,imm17,imm14,immud02 FROM imm_file,imn_file  WHERE imn01 = imm01 AND immconf = ''Y'' AND immud03 in (''1'',''2'')) bb  '
    + #13#10 +
    '   WHERE aa.tc_pjy03 = bb.imm14 AND bb.immud02 = aa.tc_pjy01 AND bb.imm17 >= aa.tc_pjy04 AND bb.imm17<= to_date(''%6:s'',''yyyymmdd'')  '
    + #13#10 + // 本周期最后一天
    'GROUP BY imn03,imn15,imn16,SUBSTR(imn17,1,INSTR(imn17,''-'')-1),imn40                                             '
    + #13#10 +
    ')                                                                                  '
    + #13#10 +
    'PIVOT                                                                              '
    + #13#10 +
    '(SUM(sl) ZSL FOR S IN (1 AS A,2 AS B,3 AS C,4 AS D,5 AS E))                        '
    + #13#10 + 'GROUP BY imn03,imn15,imn17,imn40  ' + sLineBreak +
    'ORDER BY 1                                               ';

procedure TWorkForm.ResizeForm;
begin
  self.WindowState := wsNormal;
  self.WindowState := wsMaximized;
end;

procedure TWorkForm.ztxzClick(Sender: TObject);
begin
  // 切换当前选择项，获取当前账套
  case ztxz.ItemIndex of
    1:
      CurrentZT := 'XCXD' // 现场测试账套
  else
    CurrentZT := DefaultZT;
  end;
  // 获取新的项目编号
  // 获取新的项目编号
  TimeP.Visible := false;
  MTV.DataController.RecordCount := 0;

  // 界面初始化
  xmLE.Text := '';

  // 界面初始化
  xmLE.Text := '';
end;

procedure TWorkForm.SetTimeRange;
// 根据不同条件,对收货筛选时间进行自动化处理
var
  y, m, d: word;
  SQL: string;
begin
  xmNF := Trim(xmLE.Text);
  if (xmNF = '') or (length(xmNF) <> xmLeng) then
  begin
    exit;
  end;

  // 期别的影响
  if Not TimeP.Visible then
    exit;

  SQL := 'SELECT  NVL(ta_gem33,1) dd FROM ' + CurrentZT +
    '.gem_file WHERE gem01 = ''' + Trim(xmLE.Text) + '''';
  if Not RunCDS(SQL) then
    exit;
  Cds.First;
  if Cds.RecordCount > 1 then
    exit; // 查询的结果大于1条,不可能,直接退出
  y := strtoint(qbYear.Text);
  m := qbmon.ItemIndex + 1;
  d := Cds.FieldByName('dd').AsInteger;
  if d = 1 then
  begin
    beginT.DateTime := EncodeDate(y, m, d);
    endT.DateTime := incMonth(EncodeDate(y, m, d), 1) - 1;
  end
  else
  begin
    beginT.DateTime := incMonth(EncodeDate(y, m, d), -1) + 1;
    endT.DateTime := EncodeDate(y, m, d);
  end;

  zhunbeiData_title_fill;
end;

function TWorkForm.RunCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not dba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.qbmonClick(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qbYearExit(Sender: TObject);
begin
  SetTimeRange;
end;

procedure TWorkForm.qxmClick(Sender: TObject);
begin
  // 返回项目编号选择,选择前后MasterP大小发生变化不知为何，重新定义其大小
end;

function TWorkForm.GetFilterS: string;
// 返回动态筛选条件,分为四个部分  项目编号(多于1个取第一个)/周期时间/上月结存时间/本月结存时间
// 若项目编号选择为空，不再查询
var
  bt, et: Tdate;
begin
  Result := '';

  if xmLE.Text = '' then
  begin
    exit('项目编号不能为空');
  end
  else if pos(DChar, xmLE.Text) < 1 then
    xmNF := xmLE.Text
  else
    xmNF := Copy(xmLE.Text, 1, pos(DChar, xmLE.Text) - 1);

  // 获取月结使用的年份与期别
  bt := incMonth(beginT.Date, -1);
  qyear := YearOf(bt);
  qmonth := MonthOf(bt);
  bt := incMonth(endT.Date, -1);
  byear := YearOf(bt);
  bmonth := MonthOf(bt);

  bt := beginT.Date;
  et := endT.Date;

  if et < bt then
  begin
    exit('Error : 结束日期不可小于开始时间！');
  end;

  zqTimeF := ' imm17 BETWEEN to_date(''' + FormatDateTime('yyyymmdd', bt) +
    ''',''yyyymmdd'') AND ' + ' to_date(''' + FormatDateTime('yyyymmdd', et) +
    ''',''yyyymmdd'')';
  chTimeF := stringReplace(zqTimeF, 'imm17', 'oga02', [rfReplaceAll]);
  ZxTimeF := stringReplace(zqTimeF, 'imm17', 'ina02', [rfReplaceAll]);

  qTimeF := ' tlf06 BETWEEN to_date(''' + FormatDateTime('yyyymmdd',
    StartOfTheMonth(bt)) + ''',''yyyymmdd'') AND ' + ' to_date(''' +
    FormatDateTime('yyyymmdd', incDay(bt, -1)) + ''',''yyyymmdd'')';

  bTimeF := ' tlf06 BETWEEN to_date(''' + FormatDateTime('yyyymmdd',
    StartOfTheMonth(et)) + ''',''yyyymmdd'') AND ' + ' to_date(''' +
    FormatDateTime('yyyymmdd', et) + ''',''yyyymmdd'')';
end;

procedure TWorkForm.RunQueryClick(Sender: TObject);
var
  LJBH: string;
  SQL: string;
  E: string;
  // i : Integer;
begin
  if Not softWareAuth then
  begin
    Randomize;
    if (Random(1000) mod 10 > 0) then
      exit;
  end;
  // for i := 0 to MTV.ColumnCount - 1 do
  // mtv.Columns[i].Caption := i.ToString;
  if (RtitleValue[1] = '') or (RtitleValue[1] <> TrimLeft(xmLE.Text)) then
    zhunbeiData_title_fill;
  // 主查询
  screen.Cursor := crSQLWait;

  E := GetFilterS;
  if E <> '' then
  begin
    screen.Cursor := crDefault;
    application.MessageBox(Pchar(E), '提示');
    exit;
  end;

  // 进耗存要多次从数据库取数,所以,采用分段填充GRID的方式
  MTV.DataController.RecordCount := 0;
  MTV.BeginUpdate();
  try
    // step 1 :料件查询
    SQL := format(STEP1SQL, [CurrentZT + '.', xmNF, stringReplace(zqTimeF,
      'imm17', 'tlf06', [rfReplaceAll]), qTimeF, qyear, qmonth]);
//     memo1.Lines.Text := sql;
//     exit;
    RunCDS(SQL);
    if Cds.RecordCount = 0 then
    begin
      application.MessageBox('没有查询到相关记录，请修改查询条件后继续！', '提示');
      screen.Cursor := crDefault;
      exit;
    end
    else
      FillmGrid(Cds);
    // 获取料件集
    LJBH := '';
    Cds.First;
    while Not Cds.Eof do
    begin
      LJBH := LJBH + ',''' + Cds.FieldByName('tlf01').AsString + '''';
      Cds.Next;
    end;
    LJBH := Copy(LJBH, 2, length(LJBH));

    // step 3 :收货信息
    SQL := format(STEP3SQL, [CurrentZT + '.', xmNF, stringReplace(zqTimeF,
      'imm17', 'imm12', [rfReplaceAll])]);
    // memo1.Text := sql;
    // exit;
    RunCDS(SQL);
    FillmGrid(Cds, 3);

    // step 4 :调拨信息
    SQL := format(STEP4SQL, [CurrentZT + '.', xmNF, zqTimeF]);
//        memo1.Text := sql;
//    exit;
    RunCDS(SQL);
    FillmGrid(Cds, 4);
    // step 30 :领料信息
    SQL := format(STEP30SQL, [xmNF, zqTimeF, tqcwyb, FormatDateTime('yyyymmdd',
      beginT.Date - 1), tqcyyb, tqmwyb, FormatDateTime('yyyymmdd',
      endT.Date), tqmyyb]);
//     Memo1.Text := SQL;exit;
    RunCDS(SQL);
    FillmGrid(Cds, 30);

    // step 2 :跨项目领料信息
    SQL := format(STEP2SQL, [xmNF, CurrentZT + '.', zqTimeF]);
    // memo1.Lines.Append(CDS.SQL.Text);
    RunCDS(SQL);
    FillmGrid(Cds, 2);

    // step 6 :料件信息导入
    SQL := format(STEP6SQL, [CurrentZT + '.', LJBH]);
    // memo1.Lines.Append(CDS.SQL.Text);
    RunCDS(SQL);
    FillmGrid(Cds, 6);

    // step 10 :结存数量导入
    SQL := format(STEP10SQL, [CurrentZT + '.', LJBH, xmNF, qTimeF, bTimeF,
      qyear, qmonth, byear, bmonth]);
    // memo1.lines.Text := sql;
    // exit;
    RunCDS(SQL);
    FillmGrid(Cds, 10);

    // // step 20 :结存重量导入
    // SQL := format(STEP20SQL, [CurrentZT + '.', LJBH, xmNF, stringReplace(qTimeF,
    // 'tlf06', 'tlff06', [rfReplaceAll]), stringReplace(bTimeF, 'tlf06',
    // 'tlff06', [rfReplaceAll]), qyear, qmonth, byear, bmonth]);
    // // memo1.Lines.Text := sql;
    // // exit;
    // RunCDS(SQL);
    // FillmGrid(Cds, 20);
  finally
    MTV.EndUpdate;
    ResizeForm;
    screen.Cursor := crDefault;
    PageControl1.ActivePageIndex := 0;
  end;
end;

procedure TWorkForm.FillmGrid(Q: TClientDataSet; tp: Integer = 1);
var
  i: Integer;
  s, m: string;
begin
  if Q.RecordCount = 0 then
    exit;
  case tp of
    1: // 添加料件编号
      begin
        Q.First;
        while not Q.Eof do
        begin
          i := MTV.DataController.AppendRecord;
          MTV.DataController.Values[i, 0] := Q.Fields.Fields[0].AsString;
          MTV.DataController.Values[i, 7] := Q.Fields.Fields[1].AsString;
          Q.Next;
        end;
      end;
    2: // 跨项目领用
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号+来源公司
          m := MTV.DataController.Values[i, 7];
          if Not Q.Locate('imn03;imn06', VarArrayOf([s, m]), []) then // 定位Q
            Continue;
          MTV.DataController.Values[i, 17] := Q.FieldByName('ljs').AsString;
          MTV.DataController.Values[i, 18] := Q.FieldByName('lcs').AsString;
        end;
      end;
    10: // 计算上月与本月结存数量
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号+来源公司
          m := MTV.DataController.Values[i, 7];
          if Not Q.Locate('tlf01;tlf904', VarArrayOf([s, m]), []) then // 定位Q
          begin
            MTV.DataController.Values[i, 8] := '0';
            MTV.DataController.Values[i, 12] := '0';
          end
          else
          begin
            MTV.DataController.Values[i, 8] := Q.FieldByName('s_csl').AsString;
            MTV.DataController.Values[i, 12] := Q.FieldByName('b_csl').AsString;
          end;
        end;
      end;
    20: // 计算上月与本月结存重
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号+来源公司
          m := MTV.DataController.Values[i, 7];
          if Not Q.Locate('tlff01;tlff904', VarArrayOf([s, m]), []) then // 定位Q
          begin
            MTV.DataController.Values[i, 9] := '0';
            MTV.DataController.Values[i, 15] := '0';
          end
          else
          begin
            MTV.DataController.Values[i, 9] := Q.FieldByName('s_csl').AsString;
            MTV.DataController.Values[i, 15] := Q.FieldByName('b_csl').AsString;
          end;
        end;
      end;
    3: // 本月收货
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号+来源公司
          m := MTV.DataController.Values[i, 7];
          if Not Q.Locate('imn03;imn06', VarArrayOf([s, m]), []) then // 定位Q
            Continue;
          MTV.DataController.Values[i, 11] := Q.FieldByName('shs').AsString;
        end;
      end;
    4: // 本月调拨出/入
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号+来源公司
          m := MTV.DataController.Values[i, 7];
          if Not Q.Locate('imn03;imn06', VarArrayOf([s, m]), []) then // 定位Q
            Continue;
          MTV.DataController.Values[i, 19] := Q.FieldByName('bjs').AsString;
          MTV.DataController.Values[i, 20] := Q.FieldByName('bcs').AsString;
        end;
      end;
    5: // 本月领料
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号+来源公司
          m := MTV.DataController.Values[i, 7];
          if Not Q.Locate('imn03;imn06', VarArrayOf([s, m]), []) then // 定位Q
            Continue;
          MTV.DataController.Values[i, 12] := Q.FieldByName('imn32').AsString;
          MTV.DataController.Values[i, 13] := Q.FieldByName('imn35').AsString;
        end;
      end;
    6: // 料件基本信息
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号
          if Not Q.Locate('ima01', s, []) then
            Continue; // 定位Q
          MTV.DataController.Values[i, 1] := Q.FieldByName('ima02').AsString;
          MTV.DataController.Values[i, 2] := Q.FieldByName('ima021').AsString;
          MTV.DataController.Values[i, 3] := Q.FieldByName('imaud02').AsString;
          MTV.DataController.Values[i, 4] := Q.FieldByName('imaud03').AsString;
          MTV.DataController.Values[i, 5] := Q.FieldByName('DZ').AsString;
          MTV.DataController.Values[i, 6] := Q.FieldByName('ima25').AsString;
        end;
      end;
    30: // 本月领料
      begin
        Q.First;
        for i := 0 to MTV.DataController.RecordCount - 1 do
        begin
          s := MTV.DataController.Values[i, 0]; // 料件号+来源公司
          m := MTV.DataController.Values[i, 7];
          if Not Q.Locate('imn03;imn17', VarArrayOf([s, m]), []) then // 定位Q
            Continue;
          MTV.DataController.Values[i, 9] := Q.FieldByName('sywyll').AsString;
          MTV.DataController.Values[i, 10] := Q.FieldByName('syzxll').AsString;
          MTV.DataController.Values[i, 13] := Q.FieldByName('bywyll').AsString;
          MTV.DataController.Values[i, 14] := Q.FieldByName('byzxll').AsString;
        end;
      end;
  end;
end;

procedure TWorkForm.PageControl1Change(Sender: TObject);
begin
  ResizeForm;
end;
{$REGION '与ts_tiptop数据库的连接'}

function TWorkForm.RunTCDS(SQL: string): Boolean;
begin
  Result := false;
  Cds.close;
  if not tba.ReadDataset(SQL, Cds) then
    application.MessageBox('读数据集失败！', 'Infomation')
  else
  begin
    Cds.active := True;
    Result := True;
  end;
end;

procedure TWorkForm.btn_InputJGClick(Sender: TObject);
var
  s, d: string;
  Lh, lygs: string;
  SQL: string;
  i: Integer;
begin
  // 从TS_tiptop中导入价格与运费
  if (xmNF = '') or (MTV.DataController.RecordCount = 0) then
  begin
    showmessage('没有项目编号或数据可导入');
    exit;
  end;

  s := '';
  d := '';
  for i := 0 to MTV.DataController.RecordCount - 1 do
  begin
    s := s + ',''' + Trim(qbYear.Text) + MTV.DataController.Values[i, 0] +
      MTV.DataController.Values[i, 7] + xmNF + '''';
    d := d + ',''' + Trim(qbYear.Text) + MTV.DataController.Values[i, 0] +
      MTV.DataController.Values[i, 7] + xmNF + '''';
  end;
  Delete(s, 1, 1);
  Delete(d, 1, 1);
  MTV.BeginUpdate;
  // 写入基准价
  SQL := 'SELECT YD,LH,LYGS,JG FROM BASE_JZJ WHERE FG IN (' + s + ')';
  if Not RunTCDS(SQL) then
    exit;
  Cds.First;
  for i := 0 to MTV.DataController.RecordCount - 1 do
  begin
    Lh := MTV.DataController.Values[i, 0]; // 料件号+来源公司
    lygs := MTV.DataController.Values[i, 7];
    if Not Cds.Locate('lh;lygs', VarArrayOf([Lh, lygs]), []) then // 定位Q
      Continue;
    MTV.DataController.Values[i, 15] := Cds.FieldByName('JG').AsString;
  end;
  // 写入运费
  SQL := 'SELECT YD,LH,LYGS,JG FROM BASE_YF WHERE FG IN (' + d + ')';
  if Not RunTCDS(SQL) then
    exit;
  Cds.First;
  for i := 0 to MTV.DataController.RecordCount - 1 do
  begin
    Lh := MTV.DataController.Values[i, 0]; // 料件号+来源公司
    lygs := MTV.DataController.Values[i, 7];
    if Not Cds.Locate('lh;lygs', VarArrayOf([Lh, lygs]), []) then // 定位Q
      Continue;
    MTV.DataController.Values[i, 16] := Cds.FieldByName('JG').AsString;
  end;
  MTV.EndUpdate;
end;

procedure TWorkForm.btn_outputJGClick(Sender: TObject);
var
  SQL, s, gid, yd: string;
  i: Integer;
  pak: TQBParcel;
  Lh, lygs, jzj, yf: string;
label et;
begin

  if xmNF = '' then
  begin
    showmessage('没有正确的项目号，请重试');
    exit;
  end;
  // 数据保存到数据库
  s := '';
  yd := Trim(qbYear.Text);
  gid := CreateClassID;
  for i := 0 to MTV.DataController.RecordCount - 1 do
  begin
    if VarIsNull(MTV.DataController.Values[i, 0]) then
      Lh := ''
    else
      Lh := MTV.DataController.Values[i, 0];

    if VarIsNull(MTV.DataController.Values[i, 7]) then
      lygs := ''
    else
      lygs := MTV.DataController.Values[i, 7];
    if VarIsNull(MTV.DataController.Values[i, 15]) then
      jzj := '0'
    else
      jzj := MTV.DataController.Values[i, 15];
    if VarIsNull(MTV.DataController.Values[i, 16]) then
      yf := '0'
    else
      yf := MTV.DataController.Values[i, 16];

    s := s + ',' + format('(''%s'',''%s'',''%s'',''%s'',''%s'',%s,%s)',
      [gid, yd, Lh, lygs, xmNF, jzj, yf]);
  end;

  Delete(s, 1, 1); // 去掉最前面的逗号
  SQL := format('INSERT INTO tp_jg(gid,yd,lh,lygs,xm,jzj,yf) VALUES%s', [s]);
  // memo1.Lines.Text := sql;
  if not tba.ExecuteCommand(SQL, i) then
  begin
    tba.GetLastError(s, yd);
    showmessage('价格信息上传到服务器失败，请重试' + s + ':' + yd);
    goto et;
  end
  else
    showmessage('价格信息更新到服务器成功！');
  // 数据处理后写入价格表
  pak := TQBParcel.Create;
  pak.PutStringGoods('@gid', gid, gdInput);
  if Not tba.ExecuteStoredProc('UpdateJZJ_YF', pak) then
    showmessage('价格信息更新到服务器失败，请重试');
  pak.Free;
et:
  SQL := format('DELETE FROM tp_jg WHERE gid = ''%s''', [gid]);
  if Not tba.ExecuteCommand(SQL, i) then
  begin
    showmessage('清除服务器数据缓存失败！');
  end;
end;
{$ENDREGION}
// -----------------------------------------------------------------------------
{$REGION '消息响应模块'}
// 消息响应事件

// 获取当前插件的权限信息
procedure TWorkForm.GetPrivilege(TFlag, TID, UID: string);
// TFlag = 插件的属性 ，当前为MENU
// UID 账号名
// TID 当前插件ID 为当前self.caption值
const
  mn = 'QBClient_GetPrivilege'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(TFlag + ',' + TID + ',' + UID + ',' + DLL_KEY);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取LoginUser
procedure TWorkForm.GetLoginUser(key: AnsiString);
const
  mn = 'QBClient_LoginUser'; // 'QBClient_LoginUser'
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  MessageFlag := True;
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 获取SoftWareAuth
procedure TWorkForm.GetAuth(key: AnsiString);
const
  mn = 'QBClient_SoftWareAuth'; //
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(mn);
  j := str2mem(key);
  Msgs.PostUserMessage(mn, Integer(application.Handle), j);
  Msgs.RemoveUserMessage(mn);
  FreeAndNil(Msgs);
end;

// 写系统日志...
procedure TWorkForm.LogMsg(aMsg: AnsiString);
var
  Msgs: TQBWinMessages;
  j: Integer;
begin
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage('QBClient_LogMsg');
  j := str2mem(aMsg);
  Msgs.PostUserMessage('QBClient_LogMsg', Integer(application.Handle), j);
  Msgs.RemoveUserMessage('QBClient_LogMsg');
  FreeAndNil(Msgs);
end;

procedure TWorkForm.WinMessageMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  InParcel: TQBParcel;
begin
  // 获取登录账号信息  同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      zhanghu := InParcel.GetStringGoods('LoginUser');
    end;
    exit;
  end;
  // 获取程序校核信息
  if (StrComp(Pchar(MsgName), Pchar('Main_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      softWareAuth := InParcel.GetBooleanGoods('SoftWareAuth');
    end;
    exit;
  end;
  // 获取当前插件的权限信息 同步消息
  if (StrComp(Pchar(MsgName), Pchar('Main_GetPrivilege')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      InParcel := Mem2Parcel(LParam);
      FreeMemory(LParam);
      if (InParcel.GetStringGoods('DLL_KEY') <> DLL_KEY) then
        exit; // 不是发给自己的消息，退出
      MessageFlag := false;
      InParcel.GetCDSGoods('QX', PrivilegeCDS);
    end;
    exit;
  end;
end;

{$ENDREGION}

end.

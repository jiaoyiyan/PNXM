unit QBSysLog;

interface

uses
   {$IF CompilerVersion>=23.0}Winapi.Windows{$ELSE}Windows{$IFEND},
   {$IF CompilerVersion>=23.0}Winapi.Messages{$ELSE}Messages{$IFEND},
   {$IF CompilerVersion>=23.0}System.SysUtils{$ELSE}SysUtils{$IFEND},
   {$IFDEF UNICODE}
   {$IF CompilerVersion>=23.0}System.AnsiStrings{$ELSE}AnsiStrings{$IFEND},
   {$ENDIF}
   {$IF CompilerVersion>=23.0}System.Classes{$ELSE}Classes{$IFEND},
   {$IF CompilerVersion>=23.0}System.Variants{$ELSE}Variants{$IFEND};

type
  TSysLog=class(TThread)
  private
     FLF: ansistring;
     FFileName: string;
     FCS: TRTLCriticalSection;
     FLogBuff: TMemoryStream;
     procedure WriteToFile();

   protected
     procedure Execute(); override;

   public
     constructor Create(LogPath: string);
     destructor  Destroy(); override;
     procedure WriteLog(const InBuff: Pointer; InSize: Integer);
     procedure Log(const Msg: ansistring);

  end;


implementation

{ TSysLog }

//
// 创建实例...
constructor TSysLog.Create(LogPath: string);
var
   f: integer;
begin
   inherited Create(true);
   self.FreeOnTerminate:=true;
   InitializeCriticalSection(FCS);
//
// 初始化缓冲...
   Self.FLogBuff := TMemoryStream.Create();
//
// 创建日志文件...
   FFileName:=LogPath+formatdatetime('yymmddhhnnss',now)+'.log';
   f:=FileCreate(FFileName);
   fileclose(f);
   FLF  := AnsiSTring(#13#10);
//
// 开始运行...
   Self.Resume();
end;

//
// 释放对象...
destructor TSysLog.Destroy;
begin
   try
      WriteToFile();
      DeleteCriticalSection(FCS);
   except
   end;
   inherited;
end;

//
// 线程主程序...
procedure TSysLog.Execute();
var
   FBegCount: integer;
begin
   FBegCount := GetTickCount();
   while(not Self.Terminated) do
      try
         if abs(integer(GetTickCount()) - FBegCount)>=20000 then
            begin
               WriteToFile();
               FBegCount := GetTickCount();
            end
         else
            Sleep(5);
      except
      end;
end;

//
// 写日志到缓冲...
procedure TSysLog.Log(const Msg: ansistring);
begin
   WriteLog(Pointer(Msg),Length(Msg));
end;

//
// 写日志到缓冲...
procedure TSysLog.WriteLog(const InBuff: Pointer; InSize: Integer);
var
    TmpStr: ansistring;
begin
    TmpStr := AnsiString(FormatDateTime('YYYY-MM-DD hh:mm:ss zzz ',Now()));
    EnterCriticalSection(FCS);
    try
       FLogBuff.Write(TmpStr[1],Length(TmpStr));
       FLogBuff.Write(InBuff^,InSize);
       FLogBuff.Write(FLF[1],2);
    except
    end;
    LeaveCriticalSection(FCS);
end;

//
// 写日志文件...
procedure TSysLog.WriteToFile;
var
   h: integer;
begin
   EnterCriticalSection(FCS);
   h:=fileopen(FFileName,fmOpenWrite+fmShareDenyWrite);
   fileseek(h,0,2);
   FLogBuff.Position:=0;
   filewrite(h,FLogBuff.Memory^,FLogBuff.Size);
   fileclose(h);
   FLogBuff.Clear;
   LeaveCriticalSection(FCS);
end;

end.



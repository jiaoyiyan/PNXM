//
// main.pas -- QuickBurro通用客户端进度窗模块
//             Version 1.00  Ansi Edition
//             Author: Jopher(W.G.Z)
//             QQ: 779545524
//             Email: Jopher@189.cn
//             Homepage: http://www.quickburro.com/
//             Copyright(C) Jopher Software Studio
//
unit upgradeprompt;

interface

uses
   Windows, Controls, ComCtrls, Forms, Graphics,ExtCtrls, Classes, StdCtrls;

type
  TProgressForm = class(TForm)
    Label1: TLabel;
    Image1: TImage;
    Progress: TProgressBar;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProgressForm: TProgressForm;

implementation

{$R *.dfm}

end.

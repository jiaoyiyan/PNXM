﻿//
// main.pas -- QuickBurro通用客户端主程序
// Version 1.00  Ansi Edition
// Author: Jopher(W.G.Z)
// QQ: 779545524
// Email: Jopher@189.cn
// Homepage: http://www.quickburro.com/
// Copyright(C) Jopher Software Studio
//
unit main;

interface

uses
  Windows, QBUpgrade, Classes, SysUtils, ExtCtrls, DB, DBClient, Forms,
  FileTransfer, RemotePublish, qbclientmodule, QBWinMessages, QBParcel,
  iniFiles,
  UserConnection, QBJson, qbmisc, qbcommon, DllSpread, QBSysLog, ShellApi,
  dialogs;

type
  THiddenForm = class(TForm)
    UserConn: TUserConnection;
    MainModule: TQBClientModule;
    tmpModule: TQBClientModule;
    SysTimer: TTimer;
    Publish: TRemotePublish;
    Cds: TClientDataSet;
    Transfer: TFileTransfer;
    WinMsgs: TQBWinMessages;
    Upgrade: TQBUpgrade;
    procedure StopLog;
    procedure SysTimerTimer(Sender: TObject);
    procedure UserConnDisconnect(Sender: TObject);
    procedure WinMsgsMsgArrived(Sender: TObject; MsgName: string;
      WParam, LParam: Integer);
    procedure UpgradeCheckModule(Sender: TObject;
      ModuleType, ModuleName, NewVersion: string);
    procedure UpgradeBeforeRestart(Sender: TObject);
    procedure TransferFileTransferProgress(MaxBlocks, OkBlocks: Integer);
  private
    procedure PostParcel(MsgName: string; aParcel: TQBParcel);
    function UpdateDLL(DLLName: string): boolean;
    { Private declarations }
  public
    { Public declarations }
    s_defaultdir: string;
    s_configfilename: string;
    s_key: AnsiString;
    s_userid: AnsiString;
    s_userpassword: AnsiString;
    s_RootNodeId: AnsiString;
    s_RootNodeAddress: AnsiString;
    s_NodeAddressingPort: Integer;
    s_RootNodeTaskPort: Integer;
    s_UserNodeId: AnsiString;
    s_UserNodeAddress: AnsiString;
    s_UserNodeMsgPort: Integer;
    s_UserNodeTaskPort: Integer;
    s_ModuleConfigFile: AnsiString;
    //
    tmpfilecount: Integer;
    tmpfiles: array of string;
    //
    // 常驻内存模块...
    RMCount: Integer;
    RMs: array of TQBClientModule;
    MainHandle: Integer;
    //
    NormalShutdown: boolean;
    syslog: TSysLog;
  end;

var
  HiddenForm: THiddenForm;

const
  qbuserid = '899130A864E862';
  qbuserpassword = '45DD7B8D758A808D89CC';
  keyKey = 'n:5/0-kj8ga&_0a=~h';

implementation

uses upgradeprompt;

{$R *.dfm}

var
  // 全局使用参数
  LoginUser: string;
  SoftWareAuth: boolean;
  updateFile: string; // 下载模块的dll名称
  UpdateDir : string; //远程服务器下载路径

procedure THiddenForm.StopLog;
begin
  syslog.Log('*** 客户端程序终止 ***');
  try
    syslog.Terminate;
    waitforsingleobject(syslog.Handle, infinite);
  except
  end;
end;

procedure THiddenForm.SysTimerTimer(Sender: TObject);
var
  sr: TSearchRec;
  formhandle, i, j: Integer;
  ok, needupgradedownloader: boolean;
  Json: TQBJson;
  FileName, LocalFileName, RemoteUpgradeFileName, upgradefilename,
    LoginFileName, MainFileName: string;
  version, tmpstr: string;
  ModuleList: TStringList;
  MiniFile: TiniFile;
begin
  //
  // 步骤a：程序启动...
  NormalShutdown := false;
  SysTimer.Enabled := false;
  //
  // 得到程序所在文件夹...
  s_defaultdir := extractfilepath(application.ExeName);
  //
  // 创建modules、logs文件夹...
  if not directoryexists(s_defaultdir + 'modules') then
    CreateDir(s_defaultdir + 'modules');
  if not directoryexists(s_defaultdir + 'logs') then
    CreateDir(s_defaultdir + 'logs');
  //
  // 初始化日志对象...
  try
    syslog := TSysLog.Create(s_defaultdir + 'logs\');
  except
    on e: exception do
    begin
      FreeAndNil(syslog);
      application.MessageBox('创建日志文件失败，请检查文件夹权限后再试！', '异常信息');
      close;
      exit;
    end;
  end;
  syslog.Log('*** 客户端程序启动 ***');
  //
  // 找到所有日志文件...
  try
    if {$IF CompilerVersion>=23.0}System.{$IFEND}SysUtils.FindFirst
      (s_defaultdir + 'logs\*.log', faArchive, sr) = 0 then
    begin
      //
      // 保存首个...
      i := tmpfilecount;
      inc(tmpfilecount);
      setlength(tmpfiles, tmpfilecount);
      tmpfiles[i] := sr.Name;
      //
      // 循环查找...
      while {$IF CompilerVersion>=23.0}System.{$IFEND}SysUtils.FindNext
        (sr) = 0 do
      begin
        i := tmpfilecount;
        inc(tmpfilecount);
        setlength(tmpfiles, tmpfilecount);
        tmpfiles[i] := sr.Name;
      end;
      //
      // 结束查找...
{$IF CompilerVersion>=23.0}System.{$IFEND}SysUtils.FindClose(sr);
    end;
    //
    // 日志文件从大到小排序...
    for i := 0 to tmpfilecount - 2 do
      for j := i + 1 to tmpfilecount - 1 do
        if tmpfiles[j] > tmpfiles[i] then
        begin
          tmpstr := tmpfiles[j];
          tmpfiles[j] := tmpfiles[i];
          tmpfiles[i] := tmpstr;
        end;
    //
    // 删除1个月之前的老日志文件...
    for i := tmpfilecount - 1 downto 30 do
      deletefile(s_defaultdir + 'logs\' + tmpfiles[i]);
    setlength(tmpfiles, 0);
  except
    on e: exception do
      syslog.Log('*** 清理日志文件时发生异常: [' + AnsiString(e.ClassName) + ']-' +
        AnsiString(e.Message));
  end;
  syslog.Log('步骤a: 启动初始化处理...完成.');
  //
  // 步骤b：获得联网参数...
  //
  // 假如参数配置文件不存在，判定modules\config.dll模块是否存在...
  s_configfilename := application.ExeName;
  delete(s_configfilename, length(s_configfilename) - 3, 4);
  s_configfilename := s_configfilename + '.sys';
  if not fileexists(s_configfilename) then
  begin
    //
    // 配置模块也不存在，则失败退出...
    if not fileexists(s_defaultdir + 'modules\config.dll') then
    begin
      application.MessageBox('参数配置文件及配置模块(config.dll)均找不到，程序无法继续运行！', '错误信息',
        mb_ok + mb_iconerror);
      syslog.Log('*** 参数配置文件及配置模块(config.dll)均找不到！');
      NormalShutdown := true;
      StopLog;
      close;
      exit;
    end
    //
    // 假如配置模块存在，则尝试进行参数配置...
    else
    begin
      tmpModule.DllFilename := s_defaultdir + 'modules\config.dll';
      ok := tmpModule.Mount(application, Screen);
      //
      // 加载成功时，配置参数...
      if ok then
      begin
        //
        // 显示模态窗，进行参数配置...
        tmpModule.InputParcel.Clear;
        tmpModule.InputParcel.PutStringGoods('ConfigFileName',
          s_configfilename);
        try
          formhandle := tmpModule.FormCreate;
          tmpModule.FormShowModal(formhandle, self);
          ok := tmpModule.OutputParcel.GetBooleanGoods('ProcessResult');
          tmpModule.FormFree(formhandle);
        except
          ok := false;
        end;
        tmpModule.Dismount;
        //
        // 假如参数文件还是不存在，则是取消配置，直接结束程序...
        if not ok then
        begin
          syslog.Log('*** 操作者取消了参数配置操作。');
          StopLog;
          NormalShutdown := true;
          close;
          exit;
        end;
      end
      //
      // 加载失败时...
      else
      begin
        application.MessageBox('加载参数配置模块时出现错误！', '错误信息', mb_ok + mb_iconerror);
        syslog.Log('*** 加载参数配置模块时出现错误！');
        StopLog;
        close;
        exit;
      end;
    end;
  end;
  syslog.Log('步骤b: 读取网络配置参数...完成.');
  //
  // 步骤c：连接服务器...
  //
  // 连接服务器...
  s_userid := qbcommon.Decrypt(qbuserid, UserConn.TransferKey);
  s_userpassword := qbcommon.Decrypt(qbuserpassword, UserConn.TransferKey);
  for i := 1 to 5 do
  begin
    //
    // 从配置文件读取联网参数...
    Json := TQBJson.Create(s_configfilename, false);
    try
      s_key := Json.GetString('TransferKey');
      s_key := qbcommon.Decrypt(s_key, keyKey);
      s_RootNodeId := Json.GetString('RootNodeId');
      s_RootNodeAddress := Json.GetString('RootNodeAddress');
      s_NodeAddressingPort := Json.GetInt('NodeAddressingPort');
      s_UserNodeId := Json.GetString('UserNodeId');
      s_UserNodeAddress := Json.GetString('UserNodeAddress');
      s_UserNodeMsgPort := Json.GetInt('UserNodeMsgPort');
      s_UserNodeTaskPort := Json.GetInt('UserNodeTaskPort');
      s_ModuleConfigFile := Json.GetString('ModuleConfigFile');
      ok := true;
    except
      ok := false;
    end;
    Json.Free;
    if not ok then
    begin
      application.MessageBox('错误的参数配置文件格式！', '错误信息', mb_ok + mb_iconerror);
      syslog.Log('*** 错误的参数配置文件格式！');
      StopLog;
      NormalShutdown := true;
      close;
      exit;
    end;
    //
    // 连接服务器...
    UserConn.TransferKey := s_key;
    UserConn.RootNodeId := s_RootNodeId;
    UserConn.RootNodeAddress := s_RootNodeAddress;
    UserConn.RootNodeAddressingPort := s_NodeAddressingPort;
    UserConn.UserNodeId := s_UserNodeId;
    UserConn.UserNodeAddress := s_UserNodeAddress;
    UserConn.UserNodeMsgPort := s_UserNodeMsgPort;
    UserConn.UserNodeTaskPort := s_UserNodeTaskPort;
    UserConn.UserId := s_userid;
    UserConn.UserPassword := s_userpassword;
    if not UserConn.Connect then
    begin
      if not fileexists(s_configfilename) then
        break;
      tmpModule.DllFilename := s_defaultdir + 'modules\config.dll';
      tmpModule.InputParcel.Clear;
      tmpModule.InputParcel.PutStringGoods('ConfigFileName', s_configfilename);
      ok := tmpModule.Mount(application, Screen);
      if ok then
      begin
        formhandle := tmpModule.FormCreate;
        tmpModule.FormShowModal(formhandle, HiddenForm);
        tmpModule.FormFree(formhandle);
        tmpModule.Dismount;
        continue;
      end
      else
        break;
    end
    else
      break;
  end;
  //
  // 假如连接失败，退出...
  if not UserConn.Connected then
  begin
    application.MessageBox('连接应用服务器失败，请稍后再试！', '错误信息', mb_ok + mb_iconerror);
    syslog.Log('*** 连接应用服务器失败！');
    StopLog;
    NormalShutdown := true;
    close;
    exit;
  end;
  syslog.Log('步骤c: 连接应用服务器...完成.');
  //
  // 步骤d：升级自身处理...
  RMCount := 0;
  setlength(RMs, 0);
  MainHandle := 0;
  tmpstr := 'upgradefiles\' + extractfilename(application.ExeName);
  delete(tmpstr, length(tmpstr) - 3, 4);
  UpdateDir := tmpstr + '\';
  tmpstr := tmpstr + '.uvf';
  Upgrade.ConfigFile := tmpstr;
  Upgrade.TargetNodeId := UserConn.UserNodeId;
  Upgrade.StartUpgrade;
  //
  // 步骤d：升级处理...
  //
  // 取模块列表...
  Publish.TargetNodeId := s_UserNodeId;
  Publish.TaskTimeout := 45;
  if not Publish.FetchModuleList(string(s_ModuleConfigFile), Cds) then
  begin
    application.MessageBox('对不起，从应用服务器读取模块配置文件失败！', '错误信息',
      mb_ok + mb_iconerror);
    syslog.Log('*** 从应用服务器读取模块配置文件失败！');
    StopLog;
    NormalShutdown := true;
    UserConn.Disconnect;
    close;
    exit;
  end;
  //
  // 模块版本比较、决定哪些需要更新...
  upgradefilename := '';
  RemoteUpgradeFileName := '';
  LoginFileName := '';
  MainFileName := '';
  needupgradedownloader := false;
  ModuleList := TStringList.Create;
  Cds.First;

  MiniFile := TiniFile.Create(s_defaultdir + 'module.sys');
  try
    while not Cds.Eof do
    // 将模块与版本信息写入sys文件中
    begin
      MiniFile.WriteString('Remote', Cds.FieldByName('ModuleID').AsString,
        Cds.FieldByName('ModuleVersion').AsString);
      Cds.Next;
    end;
  finally
    FreeAndNil(MiniFile);
  end;
  Cds.First;
  while not Cds.Eof do
  begin
    // 只升级通用插件，功能插件分权限下载
    if Cds.FieldByName('ModuleID').AsInteger > 1000 then
    begin
      Cds.Next;
      continue;
    end;
    // 得到本地文件名...
    FileName := trim(Cds.FieldByName('ModuleFileName').AsString);
    LocalFileName := s_defaultdir + 'modules\' + extractfilename(FileName);
    //
    // 本地文件不存在，需要下载...
    if not fileexists(LocalFileName) then
    begin
      //
      // 假如模块类别是下载更新，升级模块本身需要更新...
      if Cds.FieldByName('ModuleType').AsInteger = 3 then
        needupgradedownloader := true
        //
        // 非升级模块，需要下载...
      else
        ModuleList.Add(FileName);
    end
    //
    // 本地文件存在，比较版本后决定是否需要更新...
    else
    begin
      version := qbmisc.getfileversion(LocalFileName);
      //
      // 本地版本号比远程的要小，需要下载...
      if version < trim(Cds.FieldByName('ModuleVersion').AsString) then
      begin
        //
        // 假如模块类别是下载更新，升级模块本身需要更新...
        if Cds.FieldByName('ModuleType').AsInteger = 3 then
          needupgradedownloader := true
          //
          // 其他模块，需要下载...
        else
          ModuleList.Add(FileName);
      end;
    end;
    //
    // 假如模块类别是下载更新，是升级模块...
    if Cds.FieldByName('ModuleType').AsInteger = 3 then
    begin
      upgradefilename := LocalFileName;
      RemoteUpgradeFileName := FileName;
    end;
    //
    // 假如模块类别是4，是登录模块...
    if Cds.FieldByName('ModuleType').AsInteger = 4 then
      LoginFileName := LocalFileName;
    //
    // 假如模块类别是5，是主界面模块...
    if Cds.FieldByName('ModuleType').AsInteger = 5 then
      MainFileName := LocalFileName;
    //
    Cds.Next;
  end;
  //
  // 假如下载模块本身需要更新，下载升级模块本身...
  if needupgradedownloader then
  begin
    Transfer.TargetNodeId := s_UserNodeId;
    ProgressForm := TProgressForm.Create(nil);
    ProgressForm.Label1.Caption := '正在下载升级模块本身，请稍等片刻...';
    ProgressForm.Show;
    for i := 1 to 3 do
    begin
      try
        ok := Transfer.DownloadFile(RemoteUpgradeFileName, upgradefilename, 0);
      except
        ok := false;
      end;
      if ok then
        break;
    end;
    FreeAndNil(ProgressForm);
  end;
  //
  // 假如存在需要升级的文件...
  updateFile := upgradefilename; // 将升级模块名称回传至全局变量
  if ModuleList.Count > 0 then
  begin
    //
    // 升级模块不存在，失败...
    if not fileexists(upgradefilename) then
    begin
      FreeAndNil(ModuleList);
      application.MessageBox('对不起，升级处理模块文件不存在或自身更新失败！', '错误信息',
        mb_ok + mb_iconerror);
      syslog.Log('*** 升级模块文件不存在！');
      StopLog;
      NormalShutdown := true;
      UserConn.Disconnect;
      close;
      exit;
    end;
    //
    // 升级...
    tmpModule.DllFilename := upgradefilename;
    try
      tmpModule.Mount(application, Screen);
      formhandle := tmpModule.FormCreate;
      tmpModule.InputParcel.Clear;
      tmpModule.InputParcel.PutStringGoods('DownloadFileList', ModuleList.Text);
      tmpModule.InputParcel.PutStringGoods('LocalPath',
        s_defaultdir + 'modules\');
      tmpModule.FormShowModal(formhandle, HiddenForm);
      ok := tmpModule.OutputParcel.GetBooleanGoods('ProcessResult');
      tmpModule.FormFree(formhandle);
    except
      ok := false;
    end;
    tmpModule.Dismount;
    //
    // 假如失败，只记录错误，继续运行...
    if not ok then
      syslog.Log('*** 模块升级处理失败！');
  end;
  FreeAndNil(ModuleList);
  syslog.Log('步骤d: 下载更新应用程序模块...完成.');
  //
  // 步骤e：登录处理...
  //
  // 开始登录...
  if (LoginFileName <> '') and (not fileexists(LoginFileName)) then
  begin
    application.MessageBox('对不起，指定的登录模块丢失，程序无法继续运行！', '错误信息',
      mb_ok + mb_iconerror);
    syslog.Log('*** 指定的登录模块丢失！');
    StopLog;
    NormalShutdown := true;
    UserConn.Disconnect;
    close;
    exit;
  end;
  //
  // 假如登录模块存在，现在开始登录...
  if fileexists(LoginFileName) then
  begin
    tmpModule.DllFilename := LoginFileName;
    try
      tmpModule.Mount(application, Screen);
      formhandle := tmpModule.FormCreate;
      tmpModule.FormShowModal(formhandle, HiddenForm);
      ok := tmpModule.OutputParcel.GetBooleanGoods('ProcessResult');
      LoginUser := LowerCase(tmpModule.OutputParcel.GetStringGoods
        ('LoginUser'));
      SoftWareAuth := tmpModule.OutputParcel.GetBooleanGoods('SoftWareAuth');
      if (tmpModule.OutputParcel.GetStringGoods('Dll_Key') <> 'login_dll') then
      // 防止登录模块冒认
        ok := false;
      tmpModule.FormFree(formhandle);
    except
      ok := false;
    end;
    tmpModule.Dismount;
    //
    // 登录失败，退出...
    if not ok then
    begin
      syslog.Log('*** 用户登录过程被操作者取消！');
      StopLog;
      NormalShutdown := true;
      UserConn.Disconnect;
      close;
      exit;
    end;
  end;
  syslog.Log('步骤e: 应用操作员登录认证...完成.');
  //
  // 步骤h：开始注册、监听windows消息...
  WinMsgs.RegisterUserMessage('QBClient_LogMsg'); { 写日志 }
  WinMsgs.RegisterUserMessage('QBClient_Shutdown');
  WinMsgs.RegisterUserMessage('QBClient_Dismount');
  WinMsgs.RegisterUserMessage('QBClient_Restart');
  WinMsgs.RegisterUserMessage('QBClient_LoginUser');
  WinMsgs.RegisterUserMessage('QBClient_SoftWareAuth'); // 验证程序是否有效
  WinMsgs.RegisterUserMessage('QBClient_DLLUpdate');//监听子程序升级dll消息
  WinMsgs.RegisterUserMessage('QBClient_DefaultDir');//请求返回默认目录
  syslog.Log('步骤f: 注册及监听系统消息...完成.');
  //
  // 步骤f：注册COM模块...
  Cds.First;
  while not Cds.Eof do
  begin
    //
    // 假如COM模块...
    if Cds.FieldByName('ModuleType').AsInteger = 8 then
    begin
      //
      // 得到本地文件名...
      FileName := trim(Cds.FieldByName('ModuleFileName').AsString);
      FileName := s_defaultdir + 'modules\' + extractfilename(FileName);
      //
      // 注册...
      if fileexists(FileName) then
        try
          ShellExecute(self.Handle, 'open', PChar('RegSvr32'),
            PChar('"' + FileName + '" /s'),
            PChar('"' + extractfiledir(FileName) + '"'), SW_HIDE);
        except
          on e: exception do
          begin
            syslog.Log('*** 注册COM模块' + AnsiString(FileName) + '出现异常！');
            syslog.Log('      错误来源：' + AnsiString(e.ClassName) + '-' +
              AnsiString(e.Message));
          end;
        end;
    end;
    //
    Cds.Next;
  end;
  syslog.Log('步骤g: 注册COM模块...完成.');
  //
  // 步骤f：调用即时任务处理模块...
  Cds.First;
  while not Cds.Eof do
  begin
    //
    // 假如是即时执行模块，调用方法...
    if Cds.FieldByName('ModuleType').AsInteger = 6 then
    begin
      //
      // 得到本地文件名...
      FileName := trim(Cds.FieldByName('ModuleFileName').AsString);
      FileName := s_defaultdir + 'modules\' + extractfilename(FileName);
      //
      // 调用...
      if fileexists(FileName) then
        try
          tmpModule.ExecuteTask(FileName);
        except
          on e: exception do
          begin
            syslog.Log('*** 调用即时加载模块' + AnsiString(FileName) + '出现异常！');
            syslog.Log('      错误来源：' + AnsiString(e.ClassName) + '-' +
              AnsiString(e.Message));
          end;
        end;
    end;
    //
    Cds.Next;
  end;
  syslog.Log('步骤h: 执行即时加载模块任务...完成.');
  //
  // 步骤g：加载常驻内存模块...
  { Cds.First;
    while not cds.Eof do
    begin
    //
    // 假如是即时执行模块，调用方法...
    if cds.FieldByName('ModuleType').AsInteger=7 then
    begin
    //
    // 得到本地文件名...
    filename:=trim(cds.FieldByName('ModuleFileName').AsString);
    filename:=s_defaultdir+'modules\'+extractfilename(filename);
    //
    // 加载...
    j:=RMCount;
    inc(RMCount);
    SetLength(RMs,RMCount);
    Rms[j]:=TQBClientModule.Create(nil);
    Rms[j].DllFilename:=filename;
    Rms[j].UserConnection:=UserConn;
    try
    Rms[j].Mount(application,screen);
    except
    on e: exception do
    begin
    SysLog.Log('*** 加载驻留模块'+ansistring(filename)+'出现异常！');
    SysLog.Log('      错误来源：'+ansistring(e.ClassName)+'-'+ansistring(e.Message));
    end;
    end;
    end;
    //
    Cds.Next;
    end;
    Cds.Close;
    SysLog.Log('步骤i: 加载常驻内存模块...完成.'); }
  //
  // 步骤i：加载主控模块...
  if fileexists(MainFileName) then
  begin
    MainModule.DllFilename := MainFileName;
    try
      MainModule.Mount(application, Screen);
      MainHandle := MainModule.FormCreate;
      ok := MainModule.FormShow(MainHandle);
    except
      ok := false;
    end;
    //
    // 假如失败...
    if not ok then
    begin
      MainModule.Dismount;
      application.MessageBox('加载主界面模块失败，程序无法继续运行！', '错误信息',
        mb_ok + mb_iconerror);
      NormalShutdown := true;
      syslog.Log('*** 加载主界面模块出现异常！');
      StopLog;
      close;
      exit;
    end;
  end;
  syslog.Log('步骤j: 加载并显示主控模块...完成.');
end;

function THiddenForm.UpdateDLL(DLLName: string): boolean;
// 按权限下载功能dll
var
  formhandle: Integer;
begin
  //
  // 升级模块不存在，失败...
  if not fileexists(updateFile) then
  begin
    application.MessageBox('对不起，升级处理模块文件不存在或自身更新失败！', '错误信息',
      mb_ok + mb_iconerror);
    syslog.Log('*** 升级模块文件不存在！');
    exit(false)
  end;
  //
  // 升级...
  tmpModule.DllFilename := updateFile;
  try
    tmpModule.Mount(application, Screen);
    formhandle := tmpModule.FormCreate;
    tmpModule.InputParcel.Clear;
    tmpModule.InputParcel.PutStringGoods('DownloadFileList', UpdateDir + DLLName);
    tmpModule.InputParcel.PutStringGoods('LocalPath',
      s_defaultdir + 'modules\');
    tmpModule.FormShowModal(formhandle, HiddenForm);
    result := tmpModule.OutputParcel.GetBooleanGoods('ProcessResult');
    tmpModule.FormFree(formhandle);
  except
    result := false;
  end;
  tmpModule.Dismount;
end;

//
// 文件下载进度...
procedure THiddenForm.TransferFileTransferProgress(MaxBlocks,
  OkBlocks: Integer);
begin
  ProgressForm.Progress.Max := MaxBlocks;
  ProgressForm.Progress.Position := OkBlocks;
end;

//
// 即将重启程序时...
procedure THiddenForm.UpgradeBeforeRestart(Sender: TObject);
begin
  NormalShutdown := true;
  UserConn.Disconnect;
end;

//
// 检查自身版本...
procedure THiddenForm.UpgradeCheckModule(Sender: TObject;
  ModuleType, ModuleName, NewVersion: string);
var
  localversion: string;
  tmpfilename: string;
  i: Integer;
  ok: boolean;
begin
  if StrComp(PChar(extractfilename(ModuleName)),
    PChar(extractfilename(application.ExeName))) = 0 then
  begin
    localversion := qbmisc.getfileversion(application.ExeName);
    if NewVersion > localversion then
    begin
      tmpfilename := qbmisc.GetWinTempDir +
        extractfilename(application.ExeName);
      Transfer.TargetNodeId := UserConn.UserNodeId;
      ProgressForm := TProgressForm.Create(nil);
      ProgressForm.Label1.Caption := '正在升级客户端主程序，请稍等片刻...';
      ProgressForm.Show;
      application.ProcessMessages;
      for i := 1 to 3 do
      begin
        ok := Transfer.DownloadFile(ModuleName, tmpfilename, 0);
        if ok then
          break;
      end;
      if ok then
      begin
        ProgressForm.close;
        FreeAndNil(ProgressForm);
        Upgrade.ReplaceSelf(tmpfilename);
      end
      else
      begin
        ProgressForm.close;
        FreeAndNil(ProgressForm);
      end;
    end;
  end;
end;

//
// 与服务器断开时...
procedure THiddenForm.UserConnDisconnect(Sender: TObject);
begin
  if not NormalShutdown then
  begin
    application.MessageBox('对不起，与应用服务器断开了，程序无法继续运行！', '错误信息',
      mb_ok + mb_iconerror);
    syslog.Log('*** 与应用服务器异常断开.');
    StopLog;
    //
    // 关闭主窗口...
    MainModule.FormFree(MainHandle);
    MainModule.Dismount;
    //
    // 退出程序...
    close;
  end;
end;

//
// Windows消息...
procedure THiddenForm.WinMsgsMsgArrived(Sender: TObject; MsgName: string;
  WParam, LParam: Integer);
var
  i, j: Integer;
  msg: AnsiString;
  ModuleName: PChar;
  tmpfilename: string;
  PostQB: TQBParcel;
  tsList : TstringList;
  ok : Boolean;
begin
  //
{$IFDEF DEBUG}syslog.Log('GET MESSAGE :' + MsgName); {$ENDIF}
  // 输出日志信息...
  if (StrComp(PChar(MsgName), PChar('QBClient_LogMsg')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then
    begin
      msg := Mem2Str(LParam);
      FreeMemory(LParam);
      syslog.Log(msg);
    end;
    exit;
  end;
  //
  // 程序退出消息...
  if (StrComp(PChar(MsgName), PChar('QBClient_Shutdown')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    NormalShutdown := true;
    syslog.Log('步骤j: 终止应用程序运行...完成.');
    StopLog;
    //
    // 关闭主窗口...
    MainModule.FormFree(MainHandle);
    MainModule.Dismount;
    //
    // 退出程序...
    close;
    exit;
  end;
  //
  // 卸载一个驻留的模块...
  if (StrComp(PChar(MsgName), PChar('QBClient_Dismount')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    //
    // 根据DLL文件名确定是哪个驻留模块...
    ModuleName := PChar(LParam);
    j := -1;
    for i := 0 to RMCount - 1 do
      if StrComp(ModuleName, PChar(RMs[i].DllFilename)) = 0 then
      begin
        j := i;
        break;
      end;
    //
    // 找到，卸载...
    if j <> -1 then
    begin
      RMs[j].Dismount;
      FreeAndNil(RMs[j]);
      for i := j to RMCount - 2 do
        RMs[i] := RMs[i + 1];
      dec(RMCount);
      setlength(RMs, RMCount);
    end;
    exit;
  end;
  //
  // 重启程序...
  if (StrComp(PChar(MsgName), PChar('QBClient_Restart')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    tmpfilename := qbmisc.GetWinTempDir + extractfilename(application.ExeName);
    copyfile(PChar(application.ExeName), PChar(tmpfilename), false);
    if fileexists(tmpfilename) then
    begin
      syslog.Log('步骤j: 终止应用程序运行...完成.');
      StopLog;
      //
      // 关闭主窗口...
      MainModule.FormFree(MainHandle);
      MainModule.Dismount;
      //
      // 重启...
      Upgrade.ReplaceSelf(tmpfilename);
    end;
  end;

  // 获取登录账号信息返回LoginUser值
  if (StrComp(PChar(MsgName), PChar('QBClient_LoginUser')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then // Lparam中记录发出消息的DLL窗口
    begin
      msg := Mem2Str(LParam);
      FreeMemory(LParam);
      PostQB := TQBParcel.Create;
      PostQB.Clear;
      PostQB.PutStringGoods('DLL_KEY', msg);
      PostQB.PutStringGoods('LoginUser', LoginUser);
      PostParcel('Main_LoginUser', PostQB);
{$IFDEF DEBUG}syslog.Log('发送Main_LoginUser消息'); {$ENDIF}
    end;
    exit;
  end;
  // 获取程序是否有效
  if (StrComp(PChar(MsgName), PChar('QBClient_SoftWareAuth')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then // Lparam中记录发出消息的DLL窗口
    begin
      msg := Mem2Str(LParam);
      FreeMemory(LParam);
      PostQB := TQBParcel.Create;
      PostQB.Clear;
      PostQB.PutStringGoods('DLL_KEY', msg);
      PostQB.PutBooleanGoods('SoftWareAuth', SoftWareAuth);
      PostParcel('Main_SoftWareAuth', PostQB);
    end;
    exit;
  end;
  // 获取请求升级指定DLL消息
  if (StrComp(PChar(MsgName), PChar('QBClient_DLLUpdate')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then // Lparam中记录发出消息的DLL窗口
    begin
      msg := Mem2Str(LParam);
      FreeMemory(LParam);
      tsList := TstringList.Create;
      tsList.DelimitedText := msg; //0:DLL_KEY 1：DLL_Name 2:DLLid 3:DBTag 4: title
      try
      OK := UpdateDLL(tsList[1]);
      PostQB := TQBParcel.Create;
      PostQB.Clear;
 //返回DLL_KEY/DLL升级情况/DLL_Name/DLLid/DBTag
      PostQB.PutStringGoods('DLL_KEY', tsList[0]);
      PostQB.PutBooleanGoods('UpdateOK', OK);
      PostQB.PutStringGoods('DLLName', tsList[1]);
      PostQB.PutStringGoods('DLLID', tsList[2]);
      PostQB.PutStringGoods('DLLDBTag', tsList[3]);
      PostQB.PutStringGoods('Title',tsList[4]);
      PostParcel('Main_DLLUpdate', PostQB);
      finally
      TsList.Free;
      end;
    end;
    exit;
  end;
  // 获取请求默认目录消息
  if (StrComp(PChar(MsgName), PChar('QBClient_DefaultDir')) = 0) and
    (WParam = Integer(application.Handle)) then
  begin
    if LParam <> 0 then // Lparam中记录发出消息的DLL窗口
    begin
      msg := Mem2Str(LParam);
      FreeMemory(LParam);
      PostQB := TQBParcel.Create;
      PostQB.Clear;
      PostQB.PutStringGoods('DLL_KEY', msg);
      PostQB.PutStringGoods('DefaultDir', s_defaultDir);
      PostParcel('Main_defaultDir', PostQB);
    end;
    exit;
  end;
end;

procedure THiddenForm.PostParcel(MsgName: string; aParcel: TQBParcel);
var
  Msgs: TQBWinMessages;
  ptr: Integer;
begin
  ptr := Parcel2Mem(aParcel); // 转换为内存块
  Msgs := TQBWinMessages.Create(nil);
  Msgs.RegisterUserMessage(MsgName);
  Msgs.PostUserMessage(MsgName, Integer(application.Handle), ptr);
  Msgs.RemoveUserMessage(MsgName);
  FreeAndNil(Msgs);
end;

end.
